﻿''' <summary>
''' Defines an event arguments class for <see cref="Port">port messages</see>.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class MessageEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal details As String)
        Me.new(Diagnostics.TraceEventType.Information, details)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        MyBase.new()
        Me._severity = severity
        If String.IsNullOrWhiteSpace(details) Then
            details = ""
        End If
        Me._details = details
    End Sub

#End Region

    Private _severity As Diagnostics.TraceEventType
    ''' <summary>
    ''' Gets the message severity.
    ''' </summary>
    Public ReadOnly Property Severity As Diagnostics.TraceEventType
        Get
            Return Me._severity
        End Get
    End Property

    Private _details As String
    ''' <summary>
    ''' Gets the message details
    ''' </summary>
    Public ReadOnly Property Details As String
        Get
            Return Me._details
        End Get
    End Property

End Class
