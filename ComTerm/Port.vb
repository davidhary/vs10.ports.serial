﻿Imports System.IO
Imports System.IO.Ports
Imports System.Threading
''' <summary>
''' Defines a wrapper around the Visual Studio <see cref="SerialPort">serial port</see>
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="08/31/12" by="David" revision="0.1.4626.x">
''' Created based on Extended Serial Port Windows Forms Sample
''' http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37
''' </history>
Public Class Port

    Implements IPort

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' initialize a new instance
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        Me.New(New System.IO.Ports.SerialPort())
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Port" /> class.
    ''' Overloaded version opens the port immediate
    ''' </summary>
    ''' <param name="portParameters">The COM port parameters.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New(ByVal portParameters() As String)
        Me.New(New System.IO.Ports.SerialPort(), portParameters)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Port" /> class.
    ''' </summary>
    ''' <param name="serialPort">The <see cref="SerialPort">serial port.</see></param>
    Public Sub New(ByVal serialPort As System.IO.Ports.SerialPort)

        MyBase.New()

        Me._SyncContext = SynchronizationContext.Current

        ' create a new instance of the serial port.
        Me._serialPort = serialPort

        Me._receiveDelay = 1

    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Port" /> class.
    ''' Overloaded version opens the port immediately.
    ''' </summary>
    ''' <param name="serialPort">The serial port.</param>
    ''' <param name="portParameters">The COM port parameters.</param>
    Public Sub New(ByVal serialPort As System.IO.Ports.SerialPort, ByVal portParameters() As String)

        Me.New(serialPort)
        connect(portParameters)

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                    If Me.ConnectionChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ConnectionChangedEvent.GetInvocationList
                            RemoveHandler Me.ConnectionChanged, CType(d, Global.System.EventHandler(Of ConnectionEventArgs))
                        Next
                    End If

                    If Me.DataReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DataReceivedEvent.GetInvocationList
                            RemoveHandler Me.DataReceived, CType(d, Global.System.EventHandler(Of PortEventArgs))
                        Next
                    End If

                    If Me.DataSentEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DataSentEvent.GetInvocationList
                            RemoveHandler Me.DataSent, CType(d, Global.System.EventHandler(Of PortEventArgs))
                        Next
                    End If

                    If Me.MessageAvailableEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                            RemoveHandler Me.MessageAvailable, CType(d, Global.System.EventHandler(Of MessageEventArgs))
                        Next
                    End If

                    If Me._serialPort IsNot Nothing Then
                        ' terminate the reference to the serial port
                        Me._serialPort.Dispose()
                        Me._serialPort = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " CONNECTION INFO "

    Private _receiveDelay As Integer = 1
    ''' <summary>
    ''' Gets or sets the time in ms the Data Received handler waits.
    ''' </summary>
    Public Property ReceiveDelay() As Integer Implements IPort.ReceiveDelay
        Get
            Return Me._receiveDelay
        End Get
        Set(ByVal value As Integer)
            Me._receiveDelay = value
        End Set
    End Property

    Private _threshold As Integer
    ''' <summary>
    ''' Gets or sets the value defining the number of counts to wait before the data received event fires
    ''' </summary>
    Public Property Threshold() As Integer Implements IPort.Threshold
        Get
            Return Me._threshold
        End Get
        Set(ByVal value As Integer)
            Me._threshold = value
            Me._serialPort.ReceivedBytesThreshold = value
        End Set
    End Property

    ''' <summary>
    ''' Default port parameters.
    ''' </summary>
    ''' <returns>An array of parameters ordered per the <see cref="PortParameterIndex">parameter index</see>
    ''' Includes, in addition to the standard parameters, the Threshold count and Delay time in milliseconds</returns>
    Public Shared Function DefaultPortParameters() As String()
        Return New String() {"COM1", "9600", "8", "None", "One", "1", "1"}
    End Function

#End Region

#Region " CONNECTION STATUS "

    Private _isConnected As Boolean
    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected As Boolean Implements IPort.IsConnected
        Get
            Return Me._isConnected
        End Get
    End Property

    Private _IsLastSendOkay As Boolean
    ''' <summary>
    ''' Gets a value indicating whether this instance is last send okay.
    ''' </summary>
    Public ReadOnly Property IsLastSendOkay As Boolean Implements IPort.IsLastSendOkay
        Get
            Return Me._IsLastSendOkay
        End Get
    End Property

#End Region

#Region " CONNECTION MANAGEMENT "

    Private WithEvents _serialPort As New SerialPort

    ''' <summary>
    ''' Connects the port. 
    ''' </summary>
    ''' <param name="portParameters">The port parameters ordered by the <see cref="PortParameterIndex">index</see>
    '''  {"COM1", "9600", "8", "None", "One", "1", "1"}</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub Connect(ByVal portParameters() As String) Implements IPort.Connect

        If portParameters Is Nothing Then
            Throw New ArgumentNullException("portParameters")
        End If
        Try
            'parameters device
            With Me._serialPort
                .PortName = portParameters(PortParameterIndex.PortNumber)
                .BaudRate = CInt(portParameters(PortParameterIndex.BaudRate))
                ' demo working with enumerations. get enum value from string
                .Parity = DirectCast([Enum].Parse(GetType(Parity), portParameters(PortParameterIndex.Parity)), Parity)
                .DataBits = CInt(portParameters(PortParameterIndex.DataBits))
                .StopBits = DirectCast([Enum].Parse(GetType(StopBits), portParameters(PortParameterIndex.StopBits)), StopBits)
                .Handshake = Handshake.None
                .RtsEnable = False
                ' set the number of bytes in read buffer, when event will be fired
                .ReceivedBytesThreshold = CInt(portParameters(PortParameterIndex.Threshold))
                Me._threshold = .ReceivedBytesThreshold
                ' 5000ms will never be reached because we read only bytes present in Read Buffer
                .ReadTimeout = 5000
                ' Default value. make changes here
                .ReadBufferSize = 4096
                ' Default value
                .WriteBufferSize = 2048
                ' Make changes here
                .Encoding = System.Text.Encoding.ASCII
                ' Delay so that event handle can fetch more bytes at once
                Me._receiveDelay = CInt(portParameters(PortParameterIndex.DelayTime))
            End With

            ' open and check device if is available?
            Me._serialPort.Open()

        Catch ex As IOException
            Dim msg As String = BuildMessage(ex, "@ Connect()")
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onMessageAvailable), New MessageEventArgs(TraceEventType.Error, msg))

        Catch ex As Exception
            Dim msg As String = BuildMessage(ex, "@ Connect()")
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onMessageAvailable), New MessageEventArgs(TraceEventType.Error, msg))

        Finally
            Me._isConnected = Me._serialPort.IsOpen
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onConnectionChanged), New ConnectionEventArgs(Me._isConnected))
        End Try

    End Sub

    ''' <summary>
    ''' Disconnects the port
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub Disconnect() Implements IPort.Disconnect
        Try
            Me._serialPort.DiscardInBuffer()
            Me._serialPort.Close()
        Catch ex As Exception
            Dim msg As String = BuildMessage(ex, "@Disconnect()")
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onMessageAvailable), New MessageEventArgs(TraceEventType.Error, msg))
        Finally
            Me._isConnected = Me._serialPort.IsOpen
            Me.onConnectionChanged(New ConnectionEventArgs(Me._isConnected))
        End Try
    End Sub

#End Region

#Region " DATA MANAGEMENT "

    ''' <summary>
    ''' Propagates a sync context.
    ''' </summary>
    Private _SyncContext As SynchronizationContext

    ''' <summary>
    ''' Holds received data.
    ''' </summary>
    Private _DataBuffer() As Byte

    ''' <summary>
    ''' Sends data
    ''' </summary>
    ''' <param name="data">byte array data</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub SendData(ByVal data() As Byte) Implements IPort.SendData

        Try
            If data IsNot Nothing Then
                Me._serialPort.Write(data, 0, data.Length)
            End If
            Me._IsLastSendOkay = True
        Catch ex As Exception
            Me._IsLastSendOkay = False
            Dim msg As String = BuildMessage(ex, "@ SendData(byte())")
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onMessageAvailable), New MessageEventArgs(TraceEventType.Error, msg))
        Finally
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onDataSent), New PortEventArgs(Me._IsLastSendOkay, data))
        End Try

    End Sub

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.IO.Ports.SerialDataReceivedEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _serialPort_DataReceived(ByVal sender As System.Object,
                                         ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles _serialPort.DataReceived

        If Not Me._isConnected Then
            Me._serialPort.DiscardInBuffer()
            Return
        End If

        Dim okay As Boolean = False
        Try
            Thread.Sleep(Me._receiveDelay)
            Dim len As Integer = Me._serialPort.BytesToRead
            Me._DataBuffer = New Byte(len - 1) {}
            Me._serialPort.Read(Me._DataBuffer, 0, len)
            okay = True
        Catch ex As Exception
            Dim msg As String = BuildMessage(ex, "@port_DataReceived(object,SerialDataReceivedEventArgs)")
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onMessageAvailable), New MessageEventArgs(TraceEventType.Error, msg))
            okay = False
        Finally
            ' data from secondary thread
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf onDataReceived), New PortEventArgs(okay, Me._DataBuffer))
        End Try

    End Sub

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Public Event ConnectionChanged As EventHandler(Of ConnectionEventArgs) Implements IPort.ConnectionChanged

    ''' <summary>
    ''' Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' </summary>
    Private Sub onConnectionChanged(ByVal e As ConnectionEventArgs)
        Dim evt As EventHandler(Of ConnectionEventArgs) = Me.ConnectionChangedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when data is sent.
    ''' Is used with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    Private Sub onConnectionChanged(ByVal obj As Object)
        Me.onConnectionChanged(CType(obj, ConnectionEventArgs))
    End Sub

    ''' <summary>
    ''' Occurs when data is received.
    ''' Reception status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Public Event DataReceived As EventHandler(Of PortEventArgs) Implements IPort.DataReceived

    ''' <summary>
    ''' Invokes the <see cref="DataReceived">data received event</see>.
    ''' </summary>
    Private Sub onDataReceived(ByVal e As PortEventArgs)
        Dim evt As EventHandler(Of PortEventArgs) = Me.DataReceivedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when data is received.
    ''' Is used with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    Private Sub onDataReceived(ByVal obj As Object)
        Me.onDataReceived(CType(obj, PortEventArgs))
    End Sub

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Reception status is report along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Public Event DataSent As EventHandler(Of PortEventArgs) Implements IPort.DataSent

    ''' <summary>
    ''' Invokes the <see cref="DataSent">data Sent event</see>.
    ''' </summary>
    Private Sub onDataSent(ByVal e As PortEventArgs)
        Dim evt As EventHandler(Of PortEventArgs) = Me.DataSentEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when data is sent.
    ''' Is used with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj">The obj.</param>
    Private Sub onDataSent(ByVal obj As Object)
        Me.onDataSent(CType(obj, PortEventArgs))
    End Sub

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of MessageEventArgs) Implements IPort.MessageAvailable

    ''' <summary>
    ''' Invokes the <see cref="MessageAvailable">Message Available event</see>.
    ''' </summary>
    Private Sub onMessageAvailable(ByVal e As MessageEventArgs)
        Dim evt As EventHandler(Of MessageEventArgs) = Me.MessageAvailableEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' Is used with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    Private Sub onMessageAvailable(ByVal obj As Object)
        Me.onMessageAvailable(CType(obj, MessageEventArgs))
    End Sub

    ''' <summary>
    ''' Build a standard message
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> Private Function BuildMessage(ByVal format As String, ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "<SerialPort {0}> {1}", Me._serialPort.PortName,
                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Build an exception message.
    ''' </summary>
    Private Function BuildMessage(ByVal ex As Exception, ByVal format As String, ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "<SerialPort {0}> {1}. {2}Details: {3}", Me._serialPort.PortName,
                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args), Environment.NewLine, ex)
    End Function

#End Region

End Class

''' <summary>
''' Enumerates the index into the port parameters array.
''' </summary>
Public Enum PortParameterIndex
    PortNumber = 0
    BaudRate = 1
    DataBits = 2
    Parity = 3
    StopBits = 4
    Threshold = 5
    DelayTime = 6
End Enum

