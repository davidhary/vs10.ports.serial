﻿Imports System.Text
Imports System.Globalization
Imports System.IO
Imports System.IO.Ports
Imports System.Drawing
''' <summary>
''' Demo Serial Port terminal Ellen RAMCKE 2012
''' </summary>
''' <remarks>update 2012-06-27</remarks>
Public Class TerminalPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TerminalPanel" /> class.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        Me.New(New Port)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TerminalPanel" /> class.
    ''' </summary>
    ''' <param name="port">Specifies the <see cref="Port">port</see>.</param>
    Public Sub New(ByVal port As Port)

        MyBase.New()

        Me.InitializeComponent()

        Me._port = port
        characterWidth = 8.3 ' pixels / Char 
        charsInline = 80  ' def n start
        Me._portParameters = port.DefaultPortParameters
        configFileName = My.Application.Info.AssemblyName & ".ini"
    End Sub

    ''' <summary>
    ''' Disposes the managed resources.
    ''' </summary>
    Private Sub disposeManagedResources()
        If Me._port IsNot Nothing Then
            Me._port.Dispose()
            Me._port = Nothing
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Handles the Load event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.onFormShown()
    End Sub

    ''' <summary>
    '''  form resize
    ''' </summary>
    Private Sub form_ResizeEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ResizeEnd
        Me.charsInline = setRuler(Me._recvTextBox, Me._ShowReceivedHexCheckBox.Checked)
        Me.charsInline = setRuler(Me._xmitTextBox, Me._ShowXmitHexCheckBox.Checked)
    End Sub

    ''' <summary>
    ''' Handles the display of the controls.
    ''' </summary>
    Private Sub onFormShown()

        ' set the form caption
        Me.Text = ApplicationInfo.BuildDefaultCaption("")

        Me.Text &= String.Format(Globalization.CultureInfo.CurrentCulture,
                                 " Version {0}.{1:00}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        Me._StatusLabel.Text = "Terminal connect: none"
        setRuler(Me._recvTextBox, False)
        setRuler(Me._xmitTextBox, False)

        ' read available ports on system
        Dim portNames As String() = SerialPort.GetPortNames

        If portNames.Length > 0 Then
            Me._PortNumberCombo.Text = portNames(0)
        Else
            Me._StatusLabel.Text = "no ports detected"
            Exit Sub
        End If

        Me._PortNumberCombo.Items.Clear()
        Me._PortNumberCombo.Items.AddRange(portNames)
        Me.onConnectionChanged(Me._port.IsConnected)

    End Sub

    ''' <summary>
    '''  exit
    ''' </summary>
    Private Sub _ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitMenuItem.Click
        Me.Close()
    End Sub

#End Region

#Region " TRANSMIT MANAGEMENT "

    ''' <summary>
    ''' Handles the Click event of the Transmit CopyMenuItem control.
    ''' Copies a transmit box selection.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _xmitCopyMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _xmitCopyMenuItem.Click
        Me._xmitTextBox.Copy()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit Paste Menu Item control.
    ''' Pases a transmit box selection.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _xmitPasteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _xmitPasteMenuItem.Click
        Me._xmitTextBox.Paste()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit Cut Menu Item control.
    ''' Cuts the XMIT text box item.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _xmitCutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _xmitCutMenuItem.Click
        Me._xmitTextBox.Cut()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Transmit SendMenuItem control.
    ''' Sends position of caret line from transmit text box 
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _XmitSendMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitSendMenuItem.Click
        Dim loc As Integer = Me._xmitTextBox.GetFirstCharIndexOfCurrentLine
        Dim ln As Integer = Me._xmitTextBox.GetLineFromCharIndex(loc)
        If ln > 0 Then
            SendData(Me._xmitTextBox.Lines(ln))
            Me._XmitMessageCombo.Text = ""
        End If
    End Sub

    ''' <summary>
    ''' Sends only selection in tx box to com
    ''' </summary>
    Private Sub _xmitSendSelectionMenuItemt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _xmitSendSelectionMenuItem.Click
        If Me._xmitTextBox.SelectionLength > 0 Then
            SendData(Me._xmitTextBox.SelectedText)
            Me._XmitMessageCombo.Text = ""
        End If
    End Sub

    ''' <summary>
    ''' fetches a line from the transmit box to the transmit message box.
    ''' </summary>
    Private Sub _xmitTextBox_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _xmitTextBox.MouseClick
        Dim rb As RichTextBox = CType(sender, RichTextBox)
        Dim loc As Integer = rb.GetFirstCharIndexOfCurrentLine
        Dim ln As Integer = rb.GetLineFromCharIndex(loc)
        Me._XmitMessageCombo.Text = rb.Lines(ln)
    End Sub

    ''' <summary>
    ''' Clears the transmit box.
    ''' </summary>
    Public Sub ClearTransmitBox()
        Me._xmitTextBox.Clear()
        Me._xmitTextBox.Text = "1"
        Me.charsInline = setRuler(Me._xmitTextBox, Me._ShowXmitHexCheckBox.Checked)
        Me.TransmitCount = 0
        Me._TransmitStatusLabel.Image = If(Me.IsConnected, My.Resources.ledOrange, My.Resources.ledGray)
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ClearTransmitBoxButton control.
    ''' Clears the transmit box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ClearTransmitBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearTransmitBoxButton.Click
        Me.ClearTransmitBox()
    End Sub

    ''' <summary>
    ''' Build hex string in text box
    ''' </summary>
    Private Sub _XmitMessageCombo_TextUpdate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XmitMessageCombo.TextUpdate

        If Me._EnterXmitHexCheckBox.Checked Then
            Dim cb As ToolStripComboBox = CType(sender, ToolStripComboBox)
            cb.Text = cb.Text.ToHexFormatted
            cb.SelectionStart = cb.Text.Length
        End If

    End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the cboEnterMessage control.
    ''' Enter only allowed keys in hex mode
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs" /> instance containing the event data.</param>
    Private Sub _XmitMessageCombo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _XmitMessageCombo.KeyPress
        If e.KeyChar <> vbCr Then
            If Me._EnterXmitHexCheckBox.Checked Then
                If Not e.KeyChar.IsHexadecimal Then
                    e.Handled = True
                End If
            End If
        Else
            SendData(Me._XmitMessageCombo.Text)
            Me._XmitMessageCombo.Text = ""
        End If

    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _ShowXmitHexCheckBox control.
    ''' Toggles showing the transmit data in hex.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ShowXmitHexCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowXmitHexCheckBox.CheckedChanged
        Me.charsInline = setRuler(Me._xmitTextBox, Me._ShowXmitHexCheckBox.Checked)
    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether show transmit data in hex.
    ''' </summary>
    ''' <value><c>True</c> if [show transmit hex data]; otherwise, <c>False</c>.</value>
    Public Property ShowTransmitHexData As Boolean
        Get
            Return Me._ShowXmitHexCheckBox.Checked
        End Get
        Set(value As Boolean)
            Me._ShowXmitHexCheckBox.Checked = value
        End Set
    End Property


    Private _transmitCount As Integer
    ''' <summary>
    ''' Gets or sets the transmit count.
    ''' </summary>
    ''' <value>The transmit count.</value>
    Public Property TransmitCount As Integer
        Get
            Return Me._transmitCount
        End Get
        Set(value As Integer)
            Me._transmitCount = value
            Me._transmitCountLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:D6}", Me.TransmitCount)
        End Set
    End Property

    ''' <summary>
    ''' Sends the data.
    ''' </summary>
    ''' <param name="textData">The text data.</param>
    Public Sub SendData(ByVal textData As String)

        Dim data() As Byte = Nothing
        If String.IsNullOrWhiteSpace(textData) Then
            Return
        End If

        If Not Me._EnterXmitHexCheckBox.Checked Then
            If Me._AddCarriageReturnCheckBox.Checked Then textData &= ControlChars.Cr
            If Me._AddLineFeedCheckBox.Checked Then textData &= ControlChars.Lf
            data = Encoding.ASCII.GetBytes(textData)
        Else
            data = ConvertHexToBytes(textData)
        End If

        ' send data:
        Me._port.SendData(data)

        Me.TransmitCount += data.Length
        Me._TransmitStatusLabel.Image = My.Resources.ledGray

        ' display in box:
        If Me._ShowXmitHexCheckBox.Checked And Not Me._ShowXmitAsciiCheckBox.Checked Then
            appendBytes(Me._xmitTextBox, data, Me.charsInline, False)
        ElseIf Me._ShowXmitHexCheckBox.Checked And Me._ShowXmitAsciiCheckBox.Checked Then
            appendBytes(Me._xmitTextBox, data, Me.charsInline, True)
        ElseIf Not Me._ShowXmitHexCheckBox.Checked And Me._ShowXmitAsciiCheckBox.Checked Then
            Me._xmitTextBox.ScrollToCaret()
            Me._xmitTextBox.AppendText(textData & vbCr)
        End If

        ' Store the data in the transmit combo box.
        Me._XmitMessageCombo.Items.Add(textData)

    End Sub

#End Region

#Region " RECEIVE HANDLERS "

    Private _ReceiveCount As Integer
    ''' <summary>
    ''' Gets or sets the Receive count.
    ''' </summary>
    ''' <value>The Receive count.</value>
    Public Property ReceiveCount As Integer
        Get
            Return Me._ReceiveCount
        End Get
        Set(value As Integer)
            Me._ReceiveCount = value
            Me._receiveCountLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:D6}", Me.ReceiveCount)
        End Set
    End Property

    ''' <summary>
    ''' Handles the Click event of the _ClearReceiveBoxButton control.
    ''' clear receive box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ClearReceiveBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearReceiveBoxButton.Click
        Me._recvTextBox.Clear()
        Me._recvTextBox.Text = "1"
        Me.charsInline = setRuler(Me._recvTextBox, Me._ShowReceivedHexCheckBox.Checked)
        Me.ReceiveCount = 0
        Me._receiveStatusLabel.Image = If(Me.IsConnected, My.Resources.ledOrange, My.Resources.ledGray)
    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _ShowReceivedHexCheckBox control.
    ''' View hex in receive box.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ShowReceivedHexCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowReceivedHexCheckBox.CheckedChanged
        Me.charsInline = setRuler(Me._recvTextBox, Me._ShowReceivedHexCheckBox.Checked)
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _SaveReceiveTextBoxButton control.
    ''' Save receive box box to file
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _SaveReceiveTextBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveReceiveTextBoxButton.Click

        Me._SaveFileDialog.DefaultExt = "*.TXT"
        Me._SaveFileDialog.Filter = "txt Files | *.TXT"
        If Me._SaveFileDialog.ShowDialog() = DialogResult.OK Then

            Dim fullpath As String = Me._SaveFileDialog.FileName()
            Me._recvTextBox.SaveFile(fullpath, RichTextBoxStreamType.PlainText)
            MessageBox.Show(IO.Path.GetFileName(fullpath) & " written")

        Else
            MessageBox.Show("no data chosen")
        End If
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _LoadTransmitFileButton control.
    ''' load file into tx box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _LoadTransmitFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadTransmitFileButton.Click

        Me._OpenFileDialog.DefaultExt = "*.TXT"
        Me._OpenFileDialog.Filter = "txt Files | *.TXT"
        If Me._OpenFileDialog.ShowDialog() = DialogResult.OK Then

            Dim fullpath As String = Me._OpenFileDialog.FileName()
            Me._xmitTextBox.Clear()
            Me._xmitTextBox.LoadFile(fullpath, RichTextBoxStreamType.PlainText)

        Else
            MessageBox.Show("no data chosen")
        End If
    End Sub

#End Region

#Region " CONNECTION MANAGEMENT "

    Const disconnectTitle As String = "DISCONNECT"
    Const connectTitle As String = "      CONNECT"

    ''' <summary>
    ''' Handles the Click event of the _ConnectButton control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ConnectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConnectButton.Click
        onConnecting(Me.IsConnected)
    End Sub

    Private _portParameters As String()
    ''' <summary>
    ''' Returns the port parameters.
    ''' </summary>
    Public Function PortParameters() As String()
        Return Me._portParameters
    End Function

    ''' <summary>
    ''' Process connecting.
    ''' </summary>
    ''' <param name="isConnected">if set to <c>True</c> [is connected].</param>
    Private Sub onConnecting(ByVal isConnected As Boolean)
        If isConnected Then
            Me._port.Disconnect()
        Else
            Me.PortParameters(PortParameterIndex.PortNumber) = Me._PortNumberCombo.Text
            Me.PortParameters(PortParameterIndex.BaudRate) = Me._BaudRateCombo.Text
            Me.PortParameters(PortParameterIndex.DataBits) = Me._DataBitsCombo.Text
            Me.PortParameters(PortParameterIndex.Parity) = Me._ParityCombo.Text
            Me.PortParameters(PortParameterIndex.StopBits) = Me._StopBitsCombo.Text
            Me.PortParameters(PortParameterIndex.DelayTime) = Me._ReceiveDelayCombo.Text
            Me.PortParameters(PortParameterIndex.Threshold) = Me._ReceiveThresholdCombo.Text
            Me._port.Connect(PortParameters)
        End If
    End Sub

    ''' <summary>
    ''' Handles the change of connection.
    ''' </summary>
    ''' <param name="isConnected">if set to <c>True</c> [is connected].</param>
    Private Sub onConnectionChanged(ByVal isConnected As Boolean)

        If isConnected Then
            Me._ParityCombo.Enabled = False
            Me._StopBitsCombo.Enabled = False
            Me._PortNumberCombo.Enabled = False
            Me._BaudRateCombo.Enabled = False
            Me._DataBitsCombo.Enabled = False
            Me._ReceiveDelayCombo.Enabled = False
            Me._ReceiveThresholdCombo.Enabled = False
            Me._ConnectButton.Text = disconnectTitle
            Me._ConnectStatusLabel.Image = My.Resources.ledGreen
            Me._receiveStatusLabel.Image = My.Resources.ledOrange
            Me._TransmitStatusLabel.Image = My.Resources.ledOrange
            Me.ShowPortParameters(PortParameters)
        Else
            Me._ParityCombo.Enabled = True
            Me._StopBitsCombo.Enabled = True
            Me._PortNumberCombo.Enabled = True
            Me._BaudRateCombo.Enabled = True
            Me._DataBitsCombo.Enabled = True
            Me._ReceiveDelayCombo.Enabled = True
            Me._ReceiveThresholdCombo.Enabled = True
            Me._ConnectButton.Text = connectTitle
            Me._ConnectStatusLabel.Image = My.Resources.ledRed
            Me._receiveStatusLabel.Image = My.Resources.ledGray
            Me._TransmitStatusLabel.Image = My.Resources.ledGray
            Me._StatusLabel.Text = "Disconnected"
        End If
    End Sub

#End Region

#Region " CONFIGURATION FILE MANAGEMENT "

    Dim configFileName As String
    ''' <summary>
    ''' load config
    ''' </summary>
    Private Sub LoadConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadConfigMenuItem.Click
        Try
            Using sr As New StreamReader(configFileName)
                Me._PortNumberCombo.Text = sr.ReadLine()
                Me._BaudRateCombo.Text = sr.ReadLine()
                Me._DataBitsCombo.Text = sr.ReadLine()
                Me._ParityCombo.Text = sr.ReadLine()
                Me._StopBitsCombo.Text = sr.ReadLine()
                Me._ReceiveThresholdCombo.Text = sr.ReadLine
                Me._ReceiveDelayCombo.Text = sr.ReadLine
            End Using
            If sender IsNot Nothing Then MessageBox.Show(configFileName & " read")
        Catch ex As IOException
            MessageBox.Show(configFileName & " error: " & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' save port parameters.
    ''' </summary>
    Private Sub _SaveConfigMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveConfigMenuItem.Click
        Using sw As New StreamWriter(configFileName)
            sw.WriteLine(Me._PortNumberCombo.Text)
            sw.WriteLine(Me._BaudRateCombo.Text)
            sw.WriteLine(Me._DataBitsCombo.Text)
            sw.WriteLine(Me._ParityCombo.Text)
            sw.WriteLine(Me._StopBitsCombo.Text)
            sw.WriteLine(Me._ReceiveThresholdCombo.Text)
            sw.WriteLine(Me._ReceiveDelayCombo.Text)
        End Using
        MessageBox.Show(configFileName & " written")
    End Sub

#End Region

#Region " COM PORT MANAGEMENT "

    Private WithEvents _port As Port
    ''' <summary>
    ''' Gets or sets the port.
    ''' </summary>
    ''' <value>The port.</value>
    Public Property Port As Port
        Get
            Return Me._port
        End Get
        Set(value As Port)
            Me._port = value
            Me.onConnectionChanged(Me.IsConnected)
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected As Boolean
        Get
            If Me._port Is Nothing Then
                Return False
            Else
                Return Me._port.IsConnected
            End If
        End Get
    End Property

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control.
    ''' Updates teh data boxes and the received status.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="comTerm.PortEventArgs" /> instance containing the event data.</param>
    Private Sub _serialPort_DataReceived(sender As Object, e As PortEventArgs) Handles _port.DataReceived

        If e.StatusOkay Then
            Me._receiveStatusLabel.Image = My.Resources.ledGreen

            If e.DataBuffer IsNot Nothing Then
                Me.ReceiveCount += e.DataBuffer.Length

                If Me._ShowReceivedHexCheckBox.Checked And Not Me._ShowReceiveAsciiCheckBox.Checked Then
                    appendBytes(Me._recvTextBox, e.DataBuffer, Me.charsInline, False)

                ElseIf Me._ShowReceivedHexCheckBox.Checked And Me._ShowReceiveAsciiCheckBox.Checked Then
                    appendBytes(Me._recvTextBox, e.DataBuffer, Me.charsInline, True)

                ElseIf Not Me._ShowReceivedHexCheckBox.Checked And Me._ShowReceiveAsciiCheckBox.Checked Then
                    Dim s As String = Encoding.ASCII.GetString(e.DataBuffer, 0, e.DataBuffer.Length)
                    Me._recvTextBox.ScrollToCaret()
                    'TO_DO
                    Me._recvTextBox.AppendText(s) ' & vbCr)
                End If
            End If

        Else
            Me._receiveStatusLabel.Image = My.Resources.ledRed
        End If

    End Sub

    ''' <summary>
    ''' Handles the DataSent event of the _serialPort control.
    ''' Updates controls and data.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="comTerm.PortEventArgs" /> instance containing the event data.</param>
    Private Sub _serialPort_DataSent(sender As Object, e As PortEventArgs) Handles _port.DataSent
        If e.StatusOkay Then
            Me._TransmitStatusLabel.Image = My.Resources.ledGreen
        Else
            Me._TransmitStatusLabel.Image = My.Resources.ledRed
        End If
    End Sub

    ''' <summary>
    ''' Handles the ConnectionChanged event of the _serialPort control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="comTerm.ConnectionEventArgs" /> instance containing the event data.</param>
    Private Sub _serialPort_ConnectionChanged(sender As Object, e As ConnectionEventArgs) Handles _port.ConnectionChanged
        Me.onConnectionChanged(e.IsConnected)
    End Sub

    ''' <summary>
    ''' Handles the MessageAvailable event of the _serialPort control.
    ''' Displays an error message box.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="comTerm.MessageEventArgs" /> instance containing the event data.</param>
    Private Sub _serialPort_MessageAvailable(sender As Object, e As MessageEventArgs) Handles _port.MessageAvailable
        'Me.status0.Text = msg
        Dim icon As MessageBoxIcon = MessageBoxIcon.Information
        Select Case e.Severity
            Case TraceEventType.Critical, TraceEventType.Error
                icon = MessageBoxIcon.Error
            Case TraceEventType.Information
                icon = MessageBoxIcon.Information
            Case TraceEventType.Verbose
                icon = MessageBoxIcon.Information
            Case TraceEventType.Warning
                icon = MessageBoxIcon.Exclamation
            Case Else
                icon = MessageBoxIcon.Information
        End Select
        MessageBox.Show(e.Details, "Port Message", MessageBoxButtons.OK, icon,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    End Sub


#End Region

#Region " DISPLAY MAANAGEMENT "

    ''' <summary>
    ''' Average character width.
    ''' </summary>
    Private characterWidth As Single

    ''' <summary>
    ''' Number of characters per line.
    ''' </summary>
    Private charsInline As Integer

    ''' <summary>
    ''' set ruler in box
    ''' </summary>
    ''' <returns>length of ruler</returns>
    Private Function setRuler(ByRef rb As RichTextBox, ByVal isHex As Boolean) As Integer

        Dim rbWidth As Integer = rb.Width
        Dim s As New System.Text.StringBuilder
        Dim anzMarks As Integer

        If Not isHex Then
            anzMarks = CInt((rbWidth / characterWidth) / 5)
            For i As Integer = 1 To anzMarks
                If i < 2 Then
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "    {0:0}", i * 5))
                ElseIf i < 20 Then
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "   {0:00}", i * 5))
                Else
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "  {0:000}", i * 5))
                End If
            Next
        Else
            anzMarks = CInt((rbWidth / characterWidth) / 3)
            For i As Integer = 1 To anzMarks
                s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, " {0:00}", i))
            Next
        End If

        ' coloring ruler
        Dim cl As Color = rb.BackColor
        rb.Select(0, rb.Lines(0).Length)
        rb.SelectionBackColor = Color.LightGray
        rb.SelectedText = s.ToString
        If rb.Lines.Length = 1 Then rb.AppendText(vbCr)
        rb.SelectionBackColor = cl
        rb.SelectionLength = 0
        Return s.Length

    End Function

    ''' <summary>
    ''' select a font
    ''' </summary>
    Private Sub fontMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LargeFontMenuItem.Click, _MediumFontMenuItem.Click, _SmallFontMenuItem.Click

        Dim s As String = CType(sender, ToolStripMenuItem).Text

        If s = "Large" Then
            Me._recvTextBox.Font = New Font("Lucida Console", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Medium" Then
            Me._recvTextBox.Font = New Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Small" Then
            Me._recvTextBox.Font = New Font("Lucida Console", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        End If

        Dim g As Graphics = Me._xmitTextBox.CreateGraphics
        ' measure with test string
        Dim szF As SizeF = g.MeasureString("0123456789", Me._recvTextBox.Font)
        Me.characterWidth = 0.1F * szF.Width
        g.Dispose()

        Me._xmitTextBox.Font = Me._recvTextBox.Font

    End Sub

    ''' <summary>
    ''' Append frame in one RichTextBox
    ''' </summary>
    ''' <param name="rb">tx or rx box here</param>
    ''' <param name="data">data frame</param>
    ''' <param name="currentLength">Max chars in box</param>
    ''' <param name="showHexAndAscii">determines whether also displaying Hex True</param>
    Private Shared Sub appendBytes(ByVal rb As RichTextBox, ByVal data() As Byte, ByVal currentLength As Integer, ByVal showHexAndAscii As Boolean)

        Dim HexString As New System.Text.StringBuilder
        Dim CharString As New System.Text.StringBuilder
        Dim count As Integer = 0

        For i As Integer = 0 To data.Length - 1

            HexString.Append(String.Format(Globalization.CultureInfo.CurrentCulture, " {0:X2}", data(i)))
            If data(i) > 31 Then
                CharString.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "  {0}", Chr(data(i))))
            Else
                CharString.Append("  .")
            End If
            count += 3

            ' start a new line
            If count >= currentLength Then
                rb.ScrollToCaret()
                HexString.AppendLine()
                rb.AppendText(HexString.ToString)
                If showHexAndAscii Then
                    rb.ScrollToCaret()
                    CharString.AppendLine()
                    rb.AppendText(CharString.ToString)
                End If
                HexString = New System.Text.StringBuilder
                CharString = New System.Text.StringBuilder
                count = 0
            End If

        Next

        rb.ScrollToCaret()
        HexString.AppendLine()
        rb.AppendText(HexString.ToString)
        If showHexAndAscii Then
            rb.ScrollToCaret()
            CharString.AppendLine()
            rb.AppendText(CharString.ToString)
        End If
    End Sub

    ''' <summary>
    ''' Convert HEX string to its representing binary values
    ''' </summary>
    Private Shared Function ConvertHexToBytes(ByVal hex As String) As Byte()
        Dim data() As Byte = Nothing
        Dim s() As String = hex.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
        data = New Byte(s.Length - 1) {}
        For i As Integer = 0 To data.Length - 1
            If Not Byte.TryParse(s(i), NumberStyles.HexNumber, CultureInfo.CurrentCulture, data(i)) Then
                data(i) = 255
                MessageBox.Show("Conversion failed", "Conversion failed!", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End If
        Next
        Return data
    End Function

    ''' <summary>
    ''' Shows the port parameters.
    ''' </summary>
    Private Sub ShowPortParameters(ByVal portParameters() As String)

        Dim s As New System.Text.StringBuilder
        s.Append(" Terminal connect: ")
        For Each param As String In portParameters
            s.Append(" -")
            s.Append(param)
        Next
        Me._StatusLabel.Text = s.ToString
    End Sub

#End Region

End Class

Module Extensions
    ''' <summary>
    ''' Determines whether the specified value is allowed.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns><c>True</c> if the specified value is allowed; otherwise, <c>False</c>.</returns>
    <Runtime.CompilerServices.Extension()>
    Friend Function IsHexadecimal(ByVal value As Char) As Boolean
        Dim allowedChars As String = "0123456789ABCDEFabcdef"
        Return Not String.IsNullOrWhiteSpace(value) AndAlso allowedChars.Contains(value)
    End Function

    ''' <summary>
    ''' Builds the hex string by adding spaces between pairs.
    ''' </summary>
    ''' <param name="value">The value.</param>
    <Runtime.CompilerServices.Extension()>
    Friend Function ToHexFormatted(ByVal value As String) As String

        If String.IsNullOrWhiteSpace(value) Then Return ""
        Const space As Char = " "c
        Dim builder As New System.Text.StringBuilder
        'remove existing spaces.
        value = value.ToUpperInvariant
        For Each s As Char In value
            If Not s.Equals(space) Then
                builder.Append(s)
            End If
        Next
        value = builder.ToString
        builder = New System.Text.StringBuilder

        'insert space every 2 characters.
        Dim pairCount As Integer = 0
        For Each s As Char In value
            pairCount += 1
            If pairCount > 2 Then
                builder.Append(space)
                pairCount = 1
            End If
            builder.Append(s)
        Next
        value = builder.ToString
        Return value

    End Function

End Module