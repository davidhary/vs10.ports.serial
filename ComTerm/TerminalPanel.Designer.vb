﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class TerminalPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                disposeManagedResources()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TerminalPanel))
        Me._OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me._SaveFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._PortNumberComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PortNumberCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._BaudRateComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BaudRateCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._DataBitsComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._DataBitsCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ParityComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ParityCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._StopBitsComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StopBitsCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ReceiveDelayComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReceiveDelayCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ReceiveThresholdComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReceiveThresholdCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ConnectButton = New System.Windows.Forms.ToolStripButton()
        Me._ConnectStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._MainMenu = New System.Windows.Forms.MenuStrip()
        Me._FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LoadConfigMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveConfigMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OptionsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReceiveBoxFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LargeFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MediumFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SmallFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FontDialog = New System.Windows.Forms.FontDialog()
        Me._SplitContainer = New System.Windows.Forms.SplitContainer()
        Me._recvTextBox = New System.Windows.Forms.RichTextBox()
        Me._ShowReceivedHexCheckBox = New System.Windows.Forms.CheckBox()
        Me._ShowReceiveAsciiCheckBox = New System.Windows.Forms.CheckBox()
        Me._ReceiveToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReceiveTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator2 = New System.Windows.Forms.ToolStripSeparator()
        Me._ClearReceiveBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator = New System.Windows.Forms.ToolStripSeparator()
        Me._SaveReceiveTextBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator7 = New System.Windows.Forms.ToolStripSeparator()
        Me._receiveCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator8 = New System.Windows.Forms.ToolStripSeparator()
        Me._receiveStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator9 = New System.Windows.Forms.ToolStripSeparator()
        Me._AddCarriageReturnCheckBox = New System.Windows.Forms.CheckBox()
        Me._AddLineFeedCheckBox = New System.Windows.Forms.CheckBox()
        Me._EnterXmitHexCheckBox = New System.Windows.Forms.CheckBox()
        Me._xmitTextBox = New System.Windows.Forms.RichTextBox()
        Me._xmitContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._xmitCopyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._xmitPasteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._xmitCutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._XmitSendMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._xmitSendSelectionMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SendDataToolStrip = New System.Windows.Forms.ToolStrip()
        Me._XmitMessageCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ShowXmitHexCheckBox = New System.Windows.Forms.CheckBox()
        Me._ShowXmitAsciiCheckBox = New System.Windows.Forms.CheckBox()
        Me._XmitToolStrip = New System.Windows.Forms.ToolStrip()
        Me._xmitTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._ClearTransmitBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator4 = New System.Windows.Forms.ToolStripSeparator()
        Me._LoadTransmitFileButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator5 = New System.Windows.Forms.ToolStripSeparator()
        Me._transmitCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator6 = New System.Windows.Forms.ToolStripSeparator()
        Me._TransmitStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator3 = New System.Windows.Forms.ToolStripSeparator()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStrip.SuspendLayout()
        Me._StatusStrip.SuspendLayout()
        Me._MainMenu.SuspendLayout()
        Me._SplitContainer.Panel1.SuspendLayout()
        Me._SplitContainer.Panel2.SuspendLayout()
        Me._SplitContainer.SuspendLayout()
        Me._ReceiveToolStrip.SuspendLayout()
        Me._xmitContextMenu.SuspendLayout()
        Me._SendDataToolStrip.SuspendLayout()
        Me._XmitToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PortNumberComboLabel, Me._PortNumberCombo, Me._BaudRateComboLabel, Me._BaudRateCombo, Me._DataBitsComboLabel, Me._DataBitsCombo, Me._ParityComboLabel, Me._ParityCombo, Me._StopBitsComboLabel, Me._StopBitsCombo, Me._ReceiveDelayComboLabel, Me._ReceiveDelayCombo, Me._ReceiveThresholdComboLabel, Me._ReceiveThresholdCombo, Me._ConnectButton, Me._ConnectStatusLabel})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 24)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(731, 25)
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_PortNumberComboLabel
        '
        Me._PortNumberComboLabel.Name = "_PortNumberComboLabel"
        Me._PortNumberComboLabel.Size = New System.Drawing.Size(32, 22)
        Me._PortNumberComboLabel.Text = " Port"
        Me._PortNumberComboLabel.ToolTipText = "Port Number"
        '
        '_PortNumberCombo
        '
        Me._PortNumberCombo.AutoSize = False
        Me._PortNumberCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PortNumberCombo.MergeAction = System.Windows.Forms.MergeAction.MatchOnly
        Me._PortNumberCombo.Name = "_PortNumberCombo"
        Me._PortNumberCombo.Size = New System.Drawing.Size(60, 23)
        Me._PortNumberCombo.ToolTipText = "Port number"
        '
        '_BaudRateComboLabel
        '
        Me._BaudRateComboLabel.Name = "_BaudRateComboLabel"
        Me._BaudRateComboLabel.Size = New System.Drawing.Size(37, 22)
        Me._BaudRateComboLabel.Text = " Baud"
        Me._BaudRateComboLabel.ToolTipText = "Baud Rate"
        '
        '_BaudRateCombo
        '
        Me._BaudRateCombo.DropDownWidth = 50
        Me._BaudRateCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BaudRateCombo.Items.AddRange(New Object() {"2400", "4800", "9600", "19200", "38400", "115200"})
        Me._BaudRateCombo.Name = "_BaudRateCombo"
        Me._BaudRateCombo.Size = New System.Drawing.Size(75, 25)
        Me._BaudRateCombo.Text = "9600"
        Me._BaudRateCombo.ToolTipText = "Baud Rate"
        '
        '_DataBitsComboLabel
        '
        Me._DataBitsComboLabel.AutoToolTip = True
        Me._DataBitsComboLabel.Name = "_DataBitsComboLabel"
        Me._DataBitsComboLabel.Size = New System.Drawing.Size(34, 22)
        Me._DataBitsComboLabel.Text = " Data"
        Me._DataBitsComboLabel.ToolTipText = "Data bits"
        '
        '_DataBitsCombo
        '
        Me._DataBitsCombo.AutoSize = False
        Me._DataBitsCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DataBitsCombo.Items.AddRange(New Object() {"7", "8"})
        Me._DataBitsCombo.Name = "_DataBitsCombo"
        Me._DataBitsCombo.Size = New System.Drawing.Size(30, 23)
        Me._DataBitsCombo.Text = "8"
        Me._DataBitsCombo.ToolTipText = "Data bits"
        '
        '_ParityComboLabel
        '
        Me._ParityComboLabel.Name = "_ParityComboLabel"
        Me._ParityComboLabel.Size = New System.Drawing.Size(40, 22)
        Me._ParityComboLabel.Text = " Parity"
        Me._ParityComboLabel.ToolTipText = "Parity"
        '
        '_ParityCombo
        '
        Me._ParityCombo.AutoSize = False
        Me._ParityCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ParityCombo.Items.AddRange(New Object() {"None", "Even", "Mark", "Odd", "Space"})
        Me._ParityCombo.Name = "_ParityCombo"
        Me._ParityCombo.Size = New System.Drawing.Size(50, 23)
        Me._ParityCombo.Text = "None"
        Me._ParityCombo.ToolTipText = "Parity"
        '
        '_StopBitsComboLabel
        '
        Me._StopBitsComboLabel.Name = "_StopBitsComboLabel"
        Me._StopBitsComboLabel.Size = New System.Drawing.Size(34, 22)
        Me._StopBitsComboLabel.Text = " Stop"
        Me._StopBitsComboLabel.ToolTipText = "Stop bits"
        '
        '_StopBitsCombo
        '
        Me._StopBitsCombo.AutoSize = False
        Me._StopBitsCombo.DropDownWidth = 50
        Me._StopBitsCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StopBitsCombo.Items.AddRange(New Object() {"None", "One", "Two"})
        Me._StopBitsCombo.Name = "_StopBitsCombo"
        Me._StopBitsCombo.Size = New System.Drawing.Size(50, 25)
        Me._StopBitsCombo.Text = "One"
        Me._StopBitsCombo.ToolTipText = "Stop bits"
        '
        '_ReceiveDelayComboLabel
        '
        Me._ReceiveDelayComboLabel.Name = "_ReceiveDelayComboLabel"
        Me._ReceiveDelayComboLabel.Size = New System.Drawing.Size(36, 22)
        Me._ReceiveDelayComboLabel.Text = "Delay"
        Me._ReceiveDelayComboLabel.ToolTipText = "Delay in ms"
        '
        '_ReceiveDelayCombo
        '
        Me._ReceiveDelayCombo.AutoSize = False
        Me._ReceiveDelayCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveDelayCombo.Items.AddRange(New Object() {"1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"})
        Me._ReceiveDelayCombo.Name = "_ReceiveDelayCombo"
        Me._ReceiveDelayCombo.Size = New System.Drawing.Size(45, 23)
        Me._ReceiveDelayCombo.Text = "1"
        Me._ReceiveDelayCombo.ToolTipText = "Data received handle delay in ms"
        '
        '_ReceiveThresholdComboLabel
        '
        Me._ReceiveThresholdComboLabel.Name = "_ReceiveThresholdComboLabel"
        Me._ReceiveThresholdComboLabel.Size = New System.Drawing.Size(30, 22)
        Me._ReceiveThresholdComboLabel.Text = "Lim."
        '
        '_ReceiveThresholdCombo
        '
        Me._ReceiveThresholdCombo.AutoSize = False
        Me._ReceiveThresholdCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me._ReceiveThresholdCombo.Items.AddRange(New Object() {"1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"})
        Me._ReceiveThresholdCombo.Name = "_ReceiveThresholdCombo"
        Me._ReceiveThresholdCombo.Size = New System.Drawing.Size(45, 23)
        Me._ReceiveThresholdCombo.Text = "1"
        Me._ReceiveThresholdCombo.ToolTipText = "received bytes threshold property"
        '
        '_ConnectButton
        '
        Me._ConnectButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me._ConnectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ConnectButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectButton.Name = "_ConnectButton"
        Me._ConnectButton.Size = New System.Drawing.Size(77, 22)
        Me._ConnectButton.Text = "*CONNECT*"
        Me._ConnectButton.ToolTipText = "Connect/Disconnect"
        '
        '_ConnectStatusLabel
        '
        Me._ConnectStatusLabel.Image = Global.ComTerm.My.Resources.Resources.ledGray
        Me._ConnectStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._ConnectStatusLabel.Name = "_ConnectStatusLabel"
        Me._ConnectStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._ConnectStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._ConnectStatusLabel.ToolTipText = "connection state"
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 484)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(731, 22)
        Me._StatusStrip.TabIndex = 1
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusLabel
        '
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(121, 17)
        Me._StatusLabel.Text = "ToolStripStatusLabel1"
        '
        '_MainMenu
        '
        Me._MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileMenuItem, Me._OptionsMenuItem})
        Me._MainMenu.Location = New System.Drawing.Point(0, 0)
        Me._MainMenu.Name = "_MainMenu"
        Me._MainMenu.Size = New System.Drawing.Size(731, 24)
        Me._MainMenu.TabIndex = 2
        Me._MainMenu.Text = "MenuStrip1"
        '
        '_FileMenuItem
        '
        Me._FileMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LoadConfigMenuItem, Me._SaveConfigMenuItem, Me._ExitMenuItem})
        Me._FileMenuItem.Name = "_FileMenuItem"
        Me._FileMenuItem.Size = New System.Drawing.Size(37, 20)
        Me._FileMenuItem.Text = "File"
        '
        '_LoadConfigMenuItem
        '
        Me._LoadConfigMenuItem.Image = Global.ComTerm.My.Resources.Resources.Disk
        Me._LoadConfigMenuItem.Name = "_LoadConfigMenuItem"
        Me._LoadConfigMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._LoadConfigMenuItem.Text = "Load Config"
        '
        '_SaveConfigMenuItem
        '
        Me._SaveConfigMenuItem.Image = Global.ComTerm.My.Resources.Resources.Disk_download
        Me._SaveConfigMenuItem.Name = "_SaveConfigMenuItem"
        Me._SaveConfigMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._SaveConfigMenuItem.Text = "Save Config"
        '
        '_ExitMenuItem
        '
        Me._ExitMenuItem.Image = Global.ComTerm.My.Resources.Resources.Standby
        Me._ExitMenuItem.Name = "_ExitMenuItem"
        Me._ExitMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._ExitMenuItem.Text = "Exit"
        '
        '_OptionsMenuItem
        '
        Me._OptionsMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReceiveBoxFontMenuItem})
        Me._OptionsMenuItem.Name = "_OptionsMenuItem"
        Me._OptionsMenuItem.Size = New System.Drawing.Size(61, 20)
        Me._OptionsMenuItem.Text = "Options"
        '
        '_ReceiveBoxFontMenuItem
        '
        Me._ReceiveBoxFontMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LargeFontMenuItem, Me._MediumFontMenuItem, Me._SmallFontMenuItem})
        Me._ReceiveBoxFontMenuItem.Name = "_ReceiveBoxFontMenuItem"
        Me._ReceiveBoxFontMenuItem.Size = New System.Drawing.Size(98, 22)
        Me._ReceiveBoxFontMenuItem.Text = "Font"
        '
        '_LargeFontMenuItem
        '
        Me._LargeFontMenuItem.Name = "_LargeFontMenuItem"
        Me._LargeFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._LargeFontMenuItem.Text = "Large"
        '
        '_MediumFontMenuItem
        '
        Me._MediumFontMenuItem.Name = "_MediumFontMenuItem"
        Me._MediumFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._MediumFontMenuItem.Text = "Medium"
        '
        '_SmallFontMenuItem
        '
        Me._SmallFontMenuItem.Name = "_SmallFontMenuItem"
        Me._SmallFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._SmallFontMenuItem.Text = "Small"
        '
        '_SplitContainer
        '
        Me._SplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SplitContainer.Location = New System.Drawing.Point(0, 49)
        Me._SplitContainer.Name = "_SplitContainer"
        Me._SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '_SplitContainer.Panel1
        '
        Me._SplitContainer.Panel1.Controls.Add(Me._recvTextBox)
        Me._SplitContainer.Panel1.Controls.Add(Me._ShowReceivedHexCheckBox)
        Me._SplitContainer.Panel1.Controls.Add(Me._ShowReceiveAsciiCheckBox)
        Me._SplitContainer.Panel1.Controls.Add(Me._ReceiveToolStrip)
        '
        '_SplitContainer.Panel2
        '
        Me._SplitContainer.Panel2.Controls.Add(Me._AddCarriageReturnCheckBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._AddLineFeedCheckBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._EnterXmitHexCheckBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._xmitTextBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._SendDataToolStrip)
        Me._SplitContainer.Panel2.Controls.Add(Me._ShowXmitHexCheckBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._ShowXmitAsciiCheckBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._XmitToolStrip)
        Me._SplitContainer.Size = New System.Drawing.Size(731, 435)
        Me._SplitContainer.SplitterDistance = 270
        Me._SplitContainer.TabIndex = 3
        '
        '_recvTextBox
        '
        Me._recvTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me._recvTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._recvTextBox.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recvTextBox.Location = New System.Drawing.Point(0, 25)
        Me._recvTextBox.Name = "_recvTextBox"
        Me._recvTextBox.ReadOnly = True
        Me._recvTextBox.Size = New System.Drawing.Size(729, 243)
        Me._recvTextBox.TabIndex = 3
        Me._recvTextBox.Text = "*"
        Me._recvTextBox.WordWrap = False
        '
        '_ShowReceivedHexCheckBox
        '
        Me._ShowReceivedHexCheckBox.AutoSize = True
        Me._ShowReceivedHexCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._ShowReceivedHexCheckBox.Location = New System.Drawing.Point(330, 3)
        Me._ShowReceivedHexCheckBox.Name = "_ShowReceivedHexCheckBox"
        Me._ShowReceivedHexCheckBox.Size = New System.Drawing.Size(73, 17)
        Me._ShowReceivedHexCheckBox.TabIndex = 2
        Me._ShowReceivedHexCheckBox.Text = "Show Hex"
        Me._ToolTip.SetToolTip(Me._ShowReceivedHexCheckBox, "show Hex data")
        Me._ShowReceivedHexCheckBox.UseVisualStyleBackColor = True
        '
        '_ShowReceiveAsciiCheckBox
        '
        Me._ShowReceiveAsciiCheckBox.AutoSize = True
        Me._ShowReceiveAsciiCheckBox.Checked = True
        Me._ShowReceiveAsciiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowReceiveAsciiCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._ShowReceiveAsciiCheckBox.Location = New System.Drawing.Point(410, 3)
        Me._ShowReceiveAsciiCheckBox.Name = "_ShowReceiveAsciiCheckBox"
        Me._ShowReceiveAsciiCheckBox.Size = New System.Drawing.Size(81, 17)
        Me._ShowReceiveAsciiCheckBox.TabIndex = 1
        Me._ShowReceiveAsciiCheckBox.Text = "Show ASCII"
        Me._ShowReceiveAsciiCheckBox.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me._ToolTip.SetToolTip(Me._ShowReceiveAsciiCheckBox, "show received data in ASCII")
        Me._ShowReceiveAsciiCheckBox.UseVisualStyleBackColor = True
        '
        '_ReceiveToolStrip
        '
        Me._ReceiveToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReceiveTextBoxLabel, Me._Separator2, Me._ClearReceiveBoxButton, Me._Separator, Me._SaveReceiveTextBoxButton, Me._Separator7, Me._receiveCountLabel, Me._Separator8, Me._receiveStatusLabel, Me._Separator9})
        Me._ReceiveToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ReceiveToolStrip.Name = "_ReceiveToolStrip"
        Me._ReceiveToolStrip.Size = New System.Drawing.Size(729, 25)
        Me._ReceiveToolStrip.TabIndex = 0
        Me._ReceiveToolStrip.Text = "ToolStrip2"
        '
        '_ReceiveTextBoxLabel
        '
        Me._ReceiveTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveTextBoxLabel.Name = "_ReceiveTextBoxLabel"
        Me._ReceiveTextBoxLabel.Size = New System.Drawing.Size(51, 22)
        Me._ReceiveTextBoxLabel.Text = "RECEIVE"
        '
        '_Separator2
        '
        Me._Separator2.Name = "_Separator2"
        Me._Separator2.Size = New System.Drawing.Size(6, 25)
        '
        '_ClearReceiveBoxButton
        '
        Me._ClearReceiveBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearReceiveBoxButton.Image = Global.ComTerm.My.Resources.Resources.Doc_Del
        Me._ClearReceiveBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearReceiveBoxButton.Name = "_ClearReceiveBoxButton"
        Me._ClearReceiveBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._ClearReceiveBoxButton.ToolTipText = "clear receive box"
        '
        '_Separator
        '
        Me._Separator.Name = "_Separator"
        Me._Separator.Size = New System.Drawing.Size(6, 25)
        '
        '_SaveReceiveTextBoxButton
        '
        Me._SaveReceiveTextBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SaveReceiveTextBoxButton.Image = Global.ComTerm.My.Resources.Resources.Disk_download
        Me._SaveReceiveTextBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SaveReceiveTextBoxButton.Name = "_SaveReceiveTextBoxButton"
        Me._SaveReceiveTextBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._SaveReceiveTextBoxButton.ToolTipText = "save received data to text file"
        '
        '_Separator7
        '
        Me._Separator7.Name = "_Separator7"
        Me._Separator7.Size = New System.Drawing.Size(6, 25)
        '
        '_receiveCountLabel
        '
        Me._receiveCountLabel.Name = "_receiveCountLabel"
        Me._receiveCountLabel.Size = New System.Drawing.Size(37, 22)
        Me._receiveCountLabel.Text = "00000"
        Me._receiveCountLabel.ToolTipText = "number of bytes received"
        '
        '_Separator8
        '
        Me._Separator8.Name = "_Separator8"
        Me._Separator8.Size = New System.Drawing.Size(6, 25)
        '
        '_receiveStatusLabel
        '
        Me._receiveStatusLabel.Image = Global.ComTerm.My.Resources.Resources.ledGray
        Me._receiveStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._receiveStatusLabel.Name = "_receiveStatusLabel"
        Me._receiveStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._receiveStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._receiveStatusLabel.ToolTipText = "receive status"
        '
        '_Separator9
        '
        Me._Separator9.Name = "_Separator9"
        Me._Separator9.Size = New System.Drawing.Size(6, 25)
        '
        '_AddCarriageReturnCheckBox
        '
        Me._AddCarriageReturnCheckBox.AutoSize = True
        Me._AddCarriageReturnCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._AddCarriageReturnCheckBox.Location = New System.Drawing.Point(599, 28)
        Me._AddCarriageReturnCheckBox.Name = "_AddCarriageReturnCheckBox"
        Me._AddCarriageReturnCheckBox.Size = New System.Drawing.Size(39, 17)
        Me._AddCarriageReturnCheckBox.TabIndex = 7
        Me._AddCarriageReturnCheckBox.Text = "CR"
        Me._ToolTip.SetToolTip(Me._AddCarriageReturnCheckBox, "add carriage return to the transmitted data")
        Me._AddCarriageReturnCheckBox.UseVisualStyleBackColor = True
        '
        '_AddLineFeedCheckBox
        '
        Me._AddLineFeedCheckBox.AutoSize = True
        Me._AddLineFeedCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._AddLineFeedCheckBox.Location = New System.Drawing.Point(559, 28)
        Me._AddLineFeedCheckBox.Name = "_AddLineFeedCheckBox"
        Me._AddLineFeedCheckBox.Size = New System.Drawing.Size(36, 17)
        Me._AddLineFeedCheckBox.TabIndex = 6
        Me._AddLineFeedCheckBox.Text = "LF"
        Me._ToolTip.SetToolTip(Me._AddLineFeedCheckBox, "add line feed to the transmitted data")
        Me._AddLineFeedCheckBox.UseVisualStyleBackColor = True
        '
        '_EnterXmitHexCheckBox
        '
        Me._EnterXmitHexCheckBox.AutoSize = True
        Me._EnterXmitHexCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._EnterXmitHexCheckBox.Location = New System.Drawing.Point(244, 3)
        Me._EnterXmitHexCheckBox.Name = "_EnterXmitHexCheckBox"
        Me._EnterXmitHexCheckBox.Size = New System.Drawing.Size(71, 17)
        Me._EnterXmitHexCheckBox.TabIndex = 5
        Me._EnterXmitHexCheckBox.Text = "Enter Hex"
        Me._ToolTip.SetToolTip(Me._EnterXmitHexCheckBox, "enter mode in hex")
        Me._EnterXmitHexCheckBox.UseVisualStyleBackColor = True
        '
        '_xmitTextBox
        '
        Me._xmitTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._xmitTextBox.ContextMenuStrip = Me._xmitContextMenu
        Me._xmitTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._xmitTextBox.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._xmitTextBox.Location = New System.Drawing.Point(0, 50)
        Me._xmitTextBox.Name = "_xmitTextBox"
        Me._xmitTextBox.ReadOnly = True
        Me._xmitTextBox.Size = New System.Drawing.Size(729, 109)
        Me._xmitTextBox.TabIndex = 4
        Me._xmitTextBox.Text = "*"
        Me._ToolTip.SetToolTip(Me._xmitTextBox, "press right mouse button")
        Me._xmitTextBox.WordWrap = False
        '
        '_xmitContextMenu
        '
        Me._xmitContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._xmitCopyMenuItem, Me._xmitPasteMenuItem, Me._xmitCutMenuItem, Me._XmitSendMenuItem, Me._xmitSendSelectionMenuItem})
        Me._xmitContextMenu.Name = "MenuTxBox"
        Me._xmitContextMenu.Size = New System.Drawing.Size(150, 114)
        '
        '_xmitCopyMenuItem
        '
        Me._xmitCopyMenuItem.Image = Global.ComTerm.My.Resources.Resources.Copy
        Me._xmitCopyMenuItem.Name = "_xmitCopyMenuItem"
        Me._xmitCopyMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._xmitCopyMenuItem.Text = "Copy"
        '
        '_xmitPasteMenuItem
        '
        Me._xmitPasteMenuItem.Image = Global.ComTerm.My.Resources.Resources.Paste
        Me._xmitPasteMenuItem.Name = "_xmitPasteMenuItem"
        Me._xmitPasteMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._xmitPasteMenuItem.Text = "Paste"
        '
        '_xmitCutMenuItem
        '
        Me._xmitCutMenuItem.Image = Global.ComTerm.My.Resources.Resources.Clipboard_Cut
        Me._xmitCutMenuItem.Name = "_xmitCutMenuItem"
        Me._xmitCutMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._xmitCutMenuItem.Text = "Cut"
        '
        '_XmitSendMenuItem
        '
        Me._XmitSendMenuItem.Image = Global.ComTerm.My.Resources.Resources.Arrow1_Right
        Me._XmitSendMenuItem.Name = "_XmitSendMenuItem"
        Me._XmitSendMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._XmitSendMenuItem.Text = "send line"
        '
        '_xmitSendSelectionMenuItem
        '
        Me._xmitSendSelectionMenuItem.Image = Global.ComTerm.My.Resources.Resources.Arrow2_Right
        Me._xmitSendSelectionMenuItem.Name = "_xmitSendSelectionMenuItem"
        Me._xmitSendSelectionMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._xmitSendSelectionMenuItem.Text = "send selection"
        '
        '_SendDataToolStrip
        '
        Me._SendDataToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._XmitMessageCombo})
        Me._SendDataToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._SendDataToolStrip.Name = "_SendDataToolStrip"
        Me._SendDataToolStrip.Size = New System.Drawing.Size(729, 25)
        Me._SendDataToolStrip.TabIndex = 3
        Me._SendDataToolStrip.Text = "ToolStrip1"
        '
        '_XmitMessageCombo
        '
        Me._XmitMessageCombo.Name = "_XmitMessageCombo"
        Me._XmitMessageCombo.Size = New System.Drawing.Size(530, 25)
        Me._XmitMessageCombo.Text = "enter message to sent here and type<enter>"
        Me._XmitMessageCombo.ToolTipText = "enter message here and type<enter>"
        '
        '_ShowXmitHexCheckBox
        '
        Me._ShowXmitHexCheckBox.AutoSize = True
        Me._ShowXmitHexCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._ShowXmitHexCheckBox.Location = New System.Drawing.Point(330, 3)
        Me._ShowXmitHexCheckBox.Name = "_ShowXmitHexCheckBox"
        Me._ShowXmitHexCheckBox.Size = New System.Drawing.Size(73, 17)
        Me._ShowXmitHexCheckBox.TabIndex = 2
        Me._ShowXmitHexCheckBox.Text = "Show Hex"
        Me._ToolTip.SetToolTip(Me._ShowXmitHexCheckBox, "show HEX")
        Me._ShowXmitHexCheckBox.UseVisualStyleBackColor = True
        '
        '_ShowXmitAsciiCheckBox
        '
        Me._ShowXmitAsciiCheckBox.AutoSize = True
        Me._ShowXmitAsciiCheckBox.Checked = True
        Me._ShowXmitAsciiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowXmitAsciiCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me._ShowXmitAsciiCheckBox.Location = New System.Drawing.Point(410, 3)
        Me._ShowXmitAsciiCheckBox.Name = "_ShowXmitAsciiCheckBox"
        Me._ShowXmitAsciiCheckBox.Size = New System.Drawing.Size(81, 17)
        Me._ShowXmitAsciiCheckBox.TabIndex = 1
        Me._ShowXmitAsciiCheckBox.Text = "Show ASCII"
        Me._ToolTip.SetToolTip(Me._ShowXmitAsciiCheckBox, "show transmitted data in ASCII")
        Me._ShowXmitAsciiCheckBox.UseVisualStyleBackColor = True
        '
        '_XmitToolStrip
        '
        Me._XmitToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._xmitTextBoxLabel, Me._Separator1, Me._ClearTransmitBoxButton, Me._Separator4, Me._LoadTransmitFileButton, Me._Separator5, Me._transmitCountLabel, Me._Separator6, Me._TransmitStatusLabel, Me._Separator3})
        Me._XmitToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._XmitToolStrip.Name = "_XmitToolStrip"
        Me._XmitToolStrip.Size = New System.Drawing.Size(729, 25)
        Me._XmitToolStrip.TabIndex = 0
        Me._XmitToolStrip.Text = "ToolStrip3"
        '
        '_xmitTextBoxLabel
        '
        Me._xmitTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._xmitTextBoxLabel.Name = "_xmitTextBoxLabel"
        Me._xmitTextBoxLabel.Size = New System.Drawing.Size(62, 22)
        Me._xmitTextBoxLabel.Text = "TRANSMIT"
        '
        '_Separator1
        '
        Me._Separator1.Name = "_Separator1"
        Me._Separator1.Size = New System.Drawing.Size(6, 25)
        '
        '_ClearTransmitBoxButton
        '
        Me._ClearTransmitBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearTransmitBoxButton.Image = Global.ComTerm.My.Resources.Resources.Doc_Del
        Me._ClearTransmitBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearTransmitBoxButton.Name = "_ClearTransmitBoxButton"
        Me._ClearTransmitBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._ClearTransmitBoxButton.ToolTipText = "Clear transmit box"
        '
        '_Separator4
        '
        Me._Separator4.Name = "_Separator4"
        Me._Separator4.Size = New System.Drawing.Size(6, 25)
        '
        '_LoadTransmitFileButton
        '
        Me._LoadTransmitFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._LoadTransmitFileButton.Image = Global.ComTerm.My.Resources.Resources.Disk
        Me._LoadTransmitFileButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LoadTransmitFileButton.Name = "_LoadTransmitFileButton"
        Me._LoadTransmitFileButton.Size = New System.Drawing.Size(23, 22)
        Me._LoadTransmitFileButton.ToolTipText = "load file into transmit text box"
        '
        '_Separator5
        '
        Me._Separator5.Name = "_Separator5"
        Me._Separator5.Size = New System.Drawing.Size(6, 25)
        '
        '_transmitCountLabel
        '
        Me._transmitCountLabel.Name = "_transmitCountLabel"
        Me._transmitCountLabel.Size = New System.Drawing.Size(37, 22)
        Me._transmitCountLabel.Text = "00000"
        Me._transmitCountLabel.ToolTipText = "number of bytes sent"
        '
        '_Separator6
        '
        Me._Separator6.Name = "_Separator6"
        Me._Separator6.Size = New System.Drawing.Size(6, 25)
        '
        '_TransmitStatusLabel
        '
        Me._TransmitStatusLabel.Image = Global.ComTerm.My.Resources.Resources.ledGray
        Me._TransmitStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._TransmitStatusLabel.Name = "_TransmitStatusLabel"
        Me._TransmitStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._TransmitStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._TransmitStatusLabel.ToolTipText = "send status"
        '
        '_Separator3
        '
        Me._Separator3.Name = "_Separator3"
        Me._Separator3.Size = New System.Drawing.Size(6, 25)
        '
        'TerminalPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 506)
        Me.Controls.Add(Me._SplitContainer)
        Me.Controls.Add(Me._StatusStrip)
        Me.Controls.Add(Me._ToolStrip)
        Me.Controls.Add(Me._MainMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me._MainMenu
        Me.MinimumSize = New System.Drawing.Size(716, 250)
        Me.Name = "TerminalPanel"
        Me.Text = "Ellen's terminal II   2011 - 2012 -"
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me._MainMenu.ResumeLayout(False)
        Me._MainMenu.PerformLayout()
        Me._SplitContainer.Panel1.ResumeLayout(False)
        Me._SplitContainer.Panel1.PerformLayout()
        Me._SplitContainer.Panel2.ResumeLayout(False)
        Me._SplitContainer.Panel2.PerformLayout()
        Me._SplitContainer.ResumeLayout(False)
        Me._ReceiveToolStrip.ResumeLayout(False)
        Me._ReceiveToolStrip.PerformLayout()
        Me._xmitContextMenu.ResumeLayout(False)
        Me._SendDataToolStrip.ResumeLayout(False)
        Me._SendDataToolStrip.PerformLayout()
        Me._XmitToolStrip.ResumeLayout(False)
        Me._XmitToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Private WithEvents _SaveFileDialog As System.Windows.Forms.SaveFileDialog
    Private WithEvents _ToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _MainMenu As System.Windows.Forms.MenuStrip
    Private WithEvents _FileMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _LoadConfigMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SaveConfigMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _OptionsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReceiveBoxFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FontDialog As System.Windows.Forms.FontDialog
    Private WithEvents _SplitContainer As System.Windows.Forms.SplitContainer
    Private WithEvents _ReceiveToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _ReceiveTextBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _XmitToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _xmitTextBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ClearTransmitBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ClearReceiveBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _Separator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _Separator4 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ShowXmitHexCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ShowXmitAsciiCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _LoadTransmitFileButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator5 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _transmitCountLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator6 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ShowReceiveAsciiCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SaveReceiveTextBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator7 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _receiveCountLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator8 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ShowReceivedHexCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _receiveStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator9 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _TransmitStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator3 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _recvTextBox As System.Windows.Forms.RichTextBox
    Private WithEvents _SendDataToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _XmitMessageCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _xmitTextBox As System.Windows.Forms.RichTextBox
    Private WithEvents _StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _PortNumberComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PortNumberCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _BaudRateComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _BaudRateCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ParityComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ParityCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _StopBitsComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _StopBitsCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ConnectButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ConnectStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _LargeFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _MediumFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SmallFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _EnterXmitHexCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _xmitContextMenu As System.Windows.Forms.ContextMenuStrip
    Private WithEvents _xmitCopyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _xmitPasteMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _xmitCutMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _AddLineFeedCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _AddCarriageReturnCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _DataBitsComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _DataBitsCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _XmitSendMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _xmitSendSelectionMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReceiveDelayComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ReceiveDelayCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ReceiveThresholdComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ReceiveThresholdCombo As System.Windows.Forms.ToolStripComboBox

End Class
