﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Terminal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._PortConsole = New PortConsole()
        Me.SuspendLayout()
        '
        '_PortConsole
        '
        Me._PortConsole.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PortConsole.Location = New System.Drawing.Point(0, 0)
        Me._PortConsole.MinimumSize = New System.Drawing.Size(716, 250)
        Me._PortConsole.Name = "_PortConsole"
        Me._PortConsole.Size = New System.Drawing.Size(766, 605)
        Me._PortConsole.TabIndex = 0
        '
        'Terminal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(766, 605)
        Me.Controls.Add(Me._PortConsole)
        Me.Name = "Terminal"
        Me.Text = "Terminal"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _PortConsole As isr.Ports.Serial.PortConsole
End Class
