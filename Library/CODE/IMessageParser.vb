﻿Imports System.ComponentModel

''' <summary>
''' Defines a message parser entity. 
''' </summary>
''' <remarks>
''' This entity receives the input buffer that the port accumulates. It then 
''' parses the message and returns true upon deciding that a valid message was received.
''' </remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Interface IMessageParser
    Function Parse(ByVal values As Byte()) As MessageParserOutcome
End Interface

''' <summary>
''' Enumerates the message parser outcomes.
''' </summary>
Public Enum MessageParserOutcome

    ''' <summary>
    ''' Indicates that the message is complete
    ''' </summary>
    <Description("Complete")> Complete

    ''' <summary>
    ''' Indicates that the message is invalid.
    ''' This means that the provided buffer has invalid structure and needs to be 
    ''' cleared.
    ''' </summary>
    <Description("Invalid")> Invalid

    ''' <summary>
    ''' Indicates that the message is incomplete.
    ''' This means that the message parser expects additional information. 
    ''' </summary>
    <Description("Message incomplete")> Incomplete

End Enum