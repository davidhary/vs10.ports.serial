﻿''' <summary>
''' Defines an event arguments class for <see cref="Port">port messages</see>.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class PortEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortEventArgs" /> class.
    ''' </summary>
    Public Sub New()
        Me.new(True, New Byte() {})
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortEventArgs" /> class.
    ''' </summary>
    ''' <param name="okay">if set to <c>True</c> [okay].</param>
    Public Sub New(ByVal okay As Boolean)
        Me.new(okay, New Byte() {})
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortEventArgs" /> class.
    ''' </summary>
    ''' <param name="okay">if set to <c>True</c> [okay].</param>
    ''' <param name="data">The data.</param>
    Public Sub New(ByVal okay As Boolean, ByVal data As Byte())
        MyBase.new()
        Me._statusOkay = okay
        Me._dataBuffer = data
    End Sub

#End Region

    Private _statusOkay As Boolean
    ''' <summary>
    ''' Gets a value indicating whether status is okay, true; Otherwise, False.
    ''' </summary>
    Public ReadOnly Property StatusOkay As Boolean
        Get
            Return Me._statusOkay
        End Get
    End Property

    Private _dataBuffer As Byte()
    ''' <summary>
    ''' Gets the data buffer.
    ''' </summary>
    Public Function DataBuffer() As Byte()
        Return Me._dataBuffer
    End Function

End Class
