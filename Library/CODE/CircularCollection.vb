Imports System
Imports System.Collections
Imports System.Threading
Imports System.Reflection

''' <summary>
''' <para>Implements a <b>Circular Buffer</b>. </para>
''' The Circular Buffer is a memory queue where memory locations
''' are reused when the data producer (writer) overwrites (modulo
''' the buffer size) previously used locations.
''' The Circular Buffer operates in two modes of operation: the
''' synchronous mode and the asynchronous mode. The synchronous mode is
''' useful where one process, either the producer (writer) or the consumer
''' (reader), operates much faster than the other. The faster process
''' would otherwise waste time waiting for the slower process.
''' Instead the faster process can "burst" off the data into the
''' Circular Buffer and continue. The slower process will read the
''' data at its own rate. However, in the synchronous mode,
''' the producer and consumer of the Circular Buffer must access the queue
''' at the same average rate over time or an overflow or underflow of
''' data will occur.
''' In the asynchronous mode, we are only interested in the last data
''' items written by the producer--earlier data will be lost. This is useful
''' in debug tracing where debug information is continuously written into
''' the Circular Buffer over a period of time until the error
''' condition is detected. The Circular Buffer is then examined for the
''' sequence of states, commands, etc. written just before the error
''' was detected to help isolate the bug.
''' The actual capacity of the Circular Buffer is one less (N-1) than the
''' total length of the Buffer(N) so that a full buffer and an empty
''' buffer can be differentiated. The default length is N=256 for a
''' default capacity of 255.
''' A client process can be notified that the Circular Buffer has reached
''' a specified count (WaterMark) using the CBEventHandler delegate
'''   <para>
''' Example
'''   </para><para><code>
''' ' notify every time circular buffer is at count 8
''' theCB.SetWaterMark(8);
'''   </code></para><para><code>
''' theCB.WaterMarkNotify += new QueueCB.CBEventHandler(OnWaterMarkEvent);
'''   </code></para>
''' </summary>
''' <typeparam name="T"></typeparam>
''' <license>
''' Copyright (C) 2002 Robert HinRichs. All rights reserved.
''' BOBH@INAV.NET
''' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
''' EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
''' WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
'''   </license>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface")>
Public Class CircularCollection(Of T)
    Implements IEnumerable, IDisposable

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' Default Constructor creates N=256 element
    ''' Circular Buffer.
    ''' </summary>
    Public Sub New()
        Me.New(256)
    End Sub

    ''' <summary>
    ''' Constructor creates N=len element 
    ''' Circular Buffer with type unspecified
    ''' </summary>
    ''' <param name="length">Circular Buffer Length (# of elements)</param>
    Public Sub New(ByVal length As System.Int32)
        MyBase.New()
        Me._NumberToAdd = 0
        Me._buffer = New T(length - 1) {}
        Me.Capacity = length
        Me._add = 1 - Capacity
        Me._len = Capacity
        Me._Clear()
        Me._SynchronousMode = True
        Me._WatermarkCheckingEnabled = True
        Me._CountEvent = New CountEvent()
    End Sub

#Region " IDisposable Support "

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me.WatermarkNotifyEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.WatermarkNotifyEvent.GetInvocationList
                            RemoveHandler Me.WatermarkNotify, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If
                    If Me._CountEvent IsNot Nothing Then
                        Me._CountEvent.Dispose()
                        Me._CountEvent = Nothing
                    End If
                End If
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

    ''' <summary>
    ''' Occurs when the read pointer hits the watermark count.
    ''' </summary>
    Public Event WatermarkNotify As EventHandler(Of System.EventArgs)

    Private _Count As System.Int32
    ''' <summary>
    ''' Returns current number of items in the Circular Buffer.
    ''' In the asynchronous mode, count will saturate at N-1 if an 
    ''' overflow conditions exists. The read index will then follow the 
    ''' write index so that the latest items are always available.
    ''' </summary>
    Public ReadOnly Property Count() As System.Int32
        Get
            Return Me._Count
        End Get
    End Property

    Private _ReadIndex As System.Int32
    ''' <summary>
    ''' Returns next Read Index
    ''' </summary>
    Public ReadOnly Property ReadIndex() As System.Int32
        Get
            Return Me._ReadIndex
        End Get
    End Property

    Private _WriteIndex As System.Int32
    ''' <summary>
    ''' Returns next Write Index
    ''' </summary>
    Public ReadOnly Property WriteIndex() As System.Int32
        Get
            Return Me._WriteIndex
        End Get
    End Property

    Private _SynchronousMode As System.Boolean
    ''' <remarks>
    ''' 1> Synchronous Mode -- In the synchronous mode the emptying of the 
    ''' Circular Buffer must occur at the same average rate as the filling 
    ''' of the Circular Buffer so that the overrun condition never occurs. 
    ''' That is, as the tail index chases the head index around the circle 
    ''' of the buffer, the head index always stays ahead (in a modulo N sense)
    ''' of the tail index. No data is lost. An exception will be thrown when 
    ''' a buffer overrun condition occurs.
    ''' <para>
    ''' 2> Asynchronous Mode -- In the asynchronous mode, data will be lost 
    ''' as the Circular Buffer is overwritten. Usually in this mode, only 
    ''' the last item written are of interest. When the Write index 
    ''' (head index) catches the Read Index (tail index) the Read Index is 
    ''' automatically incremented and the Circular Buffer count will indicate 
    ''' N-1 items in the Buffer.
    ''' The synchronous mode is pretty straight forward as most of the 
    ''' responsibility for avoiding buffer overflow is a system issue for the 
    ''' producer and consumer of the data. The circular buffer just checks 
    ''' for error conditions.
    ''' The asynchronous mode is more difficult as buffer overruns are allowed.
    ''' So things operate as normal until the maximum occupancy is reached in 
    ''' the asynchronous mode. From then on, while the buffer is still at 
    ''' maximum occupancy, the read index follows the write index. This 
    ''' simulates synchronous operation. I call this the saturation
    ''' mode. While saturation exists, an additional complication must be 
    ''' handled when the data is read. The read index is stored ahead of valid 
    ''' data (write index so an adjustment must be made before it is used to 
    ''' output data from the circular buffer. 
    ''' Capacity is N-1
    ''' </para>
    ''' </remarks>		
    ''' <summary>
    ''' Used to set synchronous or asynchronous modes of operation
    ''' Modes differ in how they handle the buffer overflow 
    ''' condition.
    ''' </summary>
    ''' <value>Property <c>SynchronousMode</c>set to true of false</value>
    Public Property SynchronousMode() As System.Boolean
        Get
            Return Me._SynchronousMode
        End Get
        Set(ByVal value As System.Boolean)
            SyncLock Me
                Me._SynchronousMode = value
            End SyncLock
        End Set
    End Property

    Private _WatermarkCount As System.Int32
    ''' <summary>
    ''' Sets the level (queue count) at which the WaterMarkNotify event
    ''' will fire. 
    ''' </summary>
    ''' <value>Property <c>Watermark</c>Pos. integer at which event fires</value>
    Public Property WatermarkCount() As System.Int32
        Get
            Return Me._WatermarkCount
        End Get
        Set(ByVal value As System.Int32)
            SyncLock Me
                Me._WatermarkCount = value
            End SyncLock
        End Set
    End Property

    Private _WatermarkCheckingEnabled As System.Boolean
    ''' <summary>
    ''' Gets or sets a value indicating whether watermark checking is enabled.
    ''' </summary>
    ''' <value><c>True</c> if watermark checking is enabled; otherwise, <c>False</c>.</value>
    Public Property WatermarkCheckingEnabled() As System.Boolean
        Get
            Return Me._WatermarkCheckingEnabled
        End Get
        Set(ByVal value As System.Boolean)
            SyncLock Me
                Me._WatermarkCheckingEnabled = value
            End SyncLock
        End Set
    End Property

    ''' <summary>
    ''' Returns assembly version string
    ''' </summary>
    ''' <return>Version string "n.n.n.n"</return>
    Public ReadOnly Property GetVersion() As String
        Get
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim asmn As AssemblyName = asm.GetName()
            Dim ver As Version = asmn.Version
            Return ver.ToString()
        End Get
    End Property

    Private _add As System.Int32
    Private _len As System.Int32
    Private _NumberToAdd As System.Int32
    Private _Capacity As System.Int32
    ''' <summary>
    ''' Gets or sets the capacity.
    ''' Client must set through method.
    ''' </summary>
    ''' <value>The capacity.</value>
    Public Property Capacity() As System.Int32
        Get
            Return Me._Capacity
        End Get
        Set(ByVal value As System.Int32)
            Me._Capacity = value
            Me._add = 1 - Capacity
            Me._len = Capacity
            Me.Clear()
        End Set
    End Property

    Private _buffer() As T
    ''' <summary>
    ''' Resynchronizes the buffer pointers to beginning of buffer, i.e., it sets the
    ''' Read pointer to the write pointer. 
    ''' This is equivalent to dumping all the data in the buffer.
    ''' </summary>
    Public Sub Resync()
        Me._Clear()
    End Sub

    ''' <summary>
    ''' Clears the Circular Buffer and initializes with null
    ''' </summary>
    Private Sub _Clear()
        Me._ReadIndex = 0
        Me._WriteIndex = 0
        Me._Count = 0
        Array.Clear(Me._buffer, 0, Capacity)
    End Sub

    ''' <summary>
    ''' Clears the Circular Buffer and initializes with null
    ''' </summary>
    Public Sub Clear()
        Me._Clear()
    End Sub

    ''' <summary>
    ''' Enable Water Mark (queue count) checking
    ''' </summary>
    Public Sub WatermarkEnable()
        Me._WatermarkCheckingEnabled = True
    End Sub

    ''' <summary>
    ''' Disable Water Mark (queue count) checking
    ''' </summary>
    Public Sub WatermarkDisable()
        Me._WatermarkCheckingEnabled = False
    End Sub

    ''' <summary>
    ''' Sets Asynchronous Mode of Circular Buffer operation--
    ''' see class summary
    ''' </summary>
    Public Sub SetAsynchronousMode()
        Me.SynchronousMode = False
    End Sub

    ''' <summary>
    ''' Sets Synchronous Mode of Circular Buffer operation--
    ''' see class summary
    ''' </summary>
    Public Sub SetSynchronousMode()
        Me.SynchronousMode = True
    End Sub

    ''' <summary>
    ''' Sets WaterMark (queue count) value for
    ''' WaterMark event operations
    ''' </summary>
    ''' <param name="mark">Positive Integer WaterMark event value</param>
    Public Sub SetWatermark(ByVal mark As System.Int32)
        If mark <= (Capacity - 1) Or (mark < 0) Then
            Me.WatermarkCount = mark
        Else
            Throw New IndexRangeException("Watermark out of range")
        End If
    End Sub

    ''' <summary>
    ''' Returns item at read index without modifying the queue
    ''' <exception> throws exception if queue empty</exception>
    ''' </summary>
    ''' <returns>Returns the object at the read index 
    ''' <c>without</c> removing it (modifying the index)</returns>
    Public Function Peek() As T
        SyncLock Me
            Dim temp As T

            If Count >= 1 Then
                temp = Me._buffer(Me._ReadIndex)
                Return (temp)
            Else
                Throw New IndexRangeException("Too few items in circular buffer")
            End If

        End SyncLock ' unlock
    End Function

    ''' <summary>
    ''' Add single item to the Circular Buffer
    ''' </summary>
    ''' <param name="item">Generic item.</param>
    Public Sub Enqueue(ByVal item As T)
        If (Count + 1 > Capacity - 1) AndAlso (SynchronousMode = True) Then
            Throw New IndexRangeException("Circular Buffer capacity exceeded--Synchronous Mode")
        Else ' Async mode
            Me._NumberToAdd = 1
            Me._Write(item)
        End If
    End Sub

    ''' <summary>
    ''' Add entire Array[] to the Circular Buffer
    ''' </summary>
    ''' <param name="items">Array of items</param>
    Public Sub Enqueue(ByVal items As Array)

        If items Is Nothing Then Return
        If (Count + items.Length > Capacity - 1) AndAlso (SynchronousMode = True) Then
            Throw New IndexRangeException("Circular Buffer capacity exceeded--Synchronous Mode")
        Else ' Async mode
            Me._NumberToAdd = items.Length
            Me._Write(items)
        End If

    End Sub

    Private _CountEvent As CountEvent
    ''' <summary>
    ''' Add a single item to the Circular Buffer but block if no room.
    ''' Used with synchronous mode only.
    ''' </summary>
    ''' <param name="item">Generic item</param>
    Public Sub EnqueueBlocking(ByVal item As T)
        If SynchronousMode = True Then
            If Count <= (Capacity - 1) Then ' if room is available
                Me._Write(item) ' write the item
            Else
                Me._CountEvent.WaitLoad(1)
                Me._Write(item) ' write the item
            End If

            Me._CountEvent.SetCopy(1) ' in case waiting for items

        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Sub

    ''' <summary>
    ''' Add entire Array[] to the Circular Buffer but block if no room.
    ''' Used with synchronous mode only.
    ''' </summary>
    ''' <param name="items">Array of items</param>
    Public Sub EnqueueBlocking(ByVal items As Array)
        If items Is Nothing Then Return
        If SynchronousMode = True Then
            If (Count + items.Length) <= (Capacity - 1) Then ' if room is available
                Me._Write(items) ' write the item
            Else
                Me._CountEvent.WaitLoad(items.Length)
                Me._Write(items) ' write the item
            End If

            Me._CountEvent.SetCopy(items.Length) ' in case waiting for items
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Sub

    ''' <summary>
    ''' Return/Remove single item from the Circular Buffer
    ''' Will throw exception if no items are in the Circular Buffer 
    ''' regardless of mode.
    ''' This and CopyTo(Array, index, number) are the only queue 
    ''' item removal methods that will check for underflow and throw an 
    ''' exception. Others will either block or return the number 
    ''' of items they were able to successfully remove.
    ''' </summary>
    ''' <returns>returns/removes a single object from the 
    ''' Circular Buffer</returns>
    Public Function Dequeue() As T
        If Count > 0 Then
            Return Me._Read()
        Else
            Throw New IndexRangeException("No items in circular buffer")
        End If
    End Function

    ''' <summary>
    ''' Return/Remove single item from the Circular Buffer but block
    ''' if empty. Must be in synchronous mode.
    ''' </summary>
    ''' <returns>returns/removes a single object from the Circular Buffer</returns>
    Public Function DequeueBlocking() As T
        Dim tempo As T

        If SynchronousMode = True Then
            If Count <= 0 Then
                Me._CountEvent.WaitCopy(1)
            End If

            tempo = Me._Read()
            Me._CountEvent.SetLoad(1) ' this is for a blocked enqueue
            Return tempo
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] 
    ''' with Array offset of index.
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <param name="index">Target Array[] offset</param>
    ''' <returns>Number of items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyTo(ByVal items As Array, ByVal index As System.Int32) As System.Int32

        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                Me._Read(items, index, items.Length)
                Return cacheCount
            End If
        End SyncLock
    End Function

    ''' <summary>
    ''' Copy/Remove items from Circular Buffer to an Array[] 
    ''' with Array offset of index.
    ''' This and <see cref="Dequeue">dequeue</see> are the only queue 
    ''' item removal methods that will check for underflow and throw an 
    ''' exception. Others will either block or return the number 
    ''' of items they were able to successfully remove.
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <param name="index">Target Array[] offset</param>
    ''' <param name="itemCount">Number of items to copy/remove</param>
    ''' <returns>Number of items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyTo(ByVal items As Array, ByVal index As System.Int32, ByVal itemCount As System.Int32) As System.Int32
        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Count
            If cacheCount >= itemCount Then
                Me._Read(items, index, itemCount)
            Else
                Throw New IndexRangeException("Not enough items in circular buffer")
            End If
            Return cacheCount
        End SyncLock
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[]
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <returns>Number if items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyTo(ByVal items As Array) As System.Int32
        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        SyncLock Me
            cacheCount = Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                Me._Read(items, 0, cacheCount)
                Return cacheCount
            End If
        End SyncLock
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] 
    ''' with Array offset of index but block if empty. Must be in
    ''' synchronous mode.
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <param name="index">Target Array[] offset</param>
    ''' <returns>Number if items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyToBlocking(ByVal items As Array, ByVal index As System.Int32) As System.Int32
        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        If SynchronousMode = True Then
            ' returns all available items from the circular buffer 
            ' to caller's array
            SyncLock Me
                cacheCount = Count
            End SyncLock
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                If cacheCount > 0 Then
                    Me._Read(items, index, cacheCount)
                    ' this is the only reason method is called blocking
                    ' this if for a blocked enqueue
                    Me._CountEvent.SetLoad(cacheCount)
                End If
            End If
            Return cacheCount
        Else
            Throw New ArgumentException("must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] 
    ''' with Array offset of index but block if empty. Must be in
    ''' synchronous mode.
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <param name="index">Target Array[] offset</param>
    ''' <param name="itemCount">number to copy</param>
    ''' <returns>Number if items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyToBlocking(ByVal items As Array, ByVal index As System.Int32, ByVal itemCount As System.Int32) As System.Int32
        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        If SynchronousMode = True Then
            If Count < itemCount Then
                ' since a specific number was asked for.
                Me._CountEvent.WaitCopy(itemCount)
                cacheCount = itemCount
                Me._Read(items, index, itemCount)
            Else
                cacheCount = itemCount
                Me._Read(items, index, itemCount)
            End If

            ' this if for a blocked enqueue
            Me._CountEvent.SetLoad(itemCount)
            Return cacheCount
        Else
            Throw New ArgumentException("Must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Copy/Remove items from entire Circular Buffer to an Array[] 
    ''' with Array offset of zero but block if empty. Must be in
    ''' synchronous mode.
    ''' </summary>
    ''' <param name="items">Target Array[]</param>
    ''' <returns>Number if items copied/removed from Circular Buffer
    ''' </returns>
    Public Function CopyToBlocking(ByVal items As Array) As System.Int32
        If items Is Nothing Then Return Count
        Dim cacheCount As System.Int32
        If SynchronousMode = True Then
            ' returns all available items from the circular buffer 
            ' to caller's array
            cacheCount = Count
            If items.Length < cacheCount Then
                Throw New IndexRangeException("Too many items for destination array")
            Else
                If cacheCount > 0 Then
                    Me._Read(items, 0, items.Length)
                    ' this is the only reason method is 
                    ' called "blocking"
                    ' this if for a blocked enqueue
                    Me._CountEvent.SetLoad(cacheCount)
                End If
            End If

            Return cacheCount
        Else
            Throw New ArgumentException("Must be in synchronous mode")
        End If
    End Function

    ''' <summary>
    ''' Displays Circular Buffer contents and or state information.
    ''' Overridable.
    ''' </summary>
    Public Overridable Sub DumpCircularBuffer()
    End Sub

    ''' <summary>
    ''' Raise the watermark event by invoking the delegates
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub OnWatermarkNotify()
        Try
            ' fire the generic event
            Dim evt As EventHandler(Of EventArgs) = Me.WatermarkNotifyEvent
            If evt IsNot Nothing Then evt.Invoke(Me, System.EventArgs.Empty)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached)
        End Try
    End Sub

#Region " PRIVATE SUPPORT FUNCTIONS "

    ''' <summary>
    ''' Writes the item to the circular collection.
    ''' </summary>
    ''' <param name="item">The item.</param>
    Private Sub _Write(ByVal item As T)
        SyncLock Me
            Dim i As System.Int32 = Me._WriteIndex
            Dim tempCount As System.Int32


            Me._buffer(i) = item
            ' for speed, we only update the count after the operation
            ' but if watermark check is on, we must do it here also
            ' but don't bother if nobody has registered for the event
            If (WatermarkCheckingEnabled = True) AndAlso (WatermarkNotifyEvent IsNot Nothing) Then
                tempCount = i - Me._ReadIndex
                If tempCount < 0 Then
                    tempCount += Me._Capacity ' modulo buffer size
                End If

                If tempCount = WatermarkCount Then
                    OnWatermarkNotify()
                End If
            End If

            i += Me._add
            If i < 0 Then
                i += Me._len
            End If

            Me._WriteIndex = i
            updateCount()
        End SyncLock ' unlock
    End Sub

    ''' <summary>
    ''' Writes the items from the array into the buffer.
    ''' </summary>
    ''' <param name="items">The items.</param>
    Private Sub _Write(ByVal items As Array)
        If items Is Nothing Then Return
        SyncLock Me
            Dim i As System.Int32 = Me._WriteIndex
            Dim n As System.Int32 = items.Length
            Dim offset As System.Int32 = 0
            Dim tempCount As System.Int32

            Dim tempVar As Boolean = n > 0
            n -= 1
            Do While tempVar
                Me._buffer(i) = CType(items.GetValue(offset), T)
                offset += 1

                ' for speed, we only update the count after the operation
                ' but if watermark check is on, we must do it here also
                ' but don't bother if nobody has registered for the event
                If (WatermarkCheckingEnabled = True) AndAlso (WatermarkNotifyEvent IsNot Nothing) Then
                    tempCount = i - Me._ReadIndex
                    If tempCount < 0 Then
                        tempCount += Me._Capacity ' modulo buffer size
                    End If

                    If tempCount = WatermarkCount Then
                        OnWatermarkNotify()
                    End If
                End If

                i += Me._add
                If i < 0 Then
                    i += Me._len
                End If
                tempVar = n > 0
                n -= 1
            Loop
            Me._WriteIndex = i
            updateCount()
        End SyncLock ' unlock
    End Sub

    ''' <summary>
    ''' Reads an item from the circular collection.
    ''' </summary>
    Private Function _Read() As T

        SyncLock Me
            Dim i As System.Int32 = Me._ReadIndex
            Dim temp As T
            Dim tempCount As System.Int32

            ' A modification to the read index may be required
            ' if we have been operating in asynchronous saturated mode
            If SynchronousMode = False Then
                If Count >= (Capacity - 1) Then
                    i = (Me._ReadIndex - 1) Mod Capacity
                End If
            End If

            temp = Me._buffer(i)

            ' for speed, we only update the count after the operation
            ' but if watermark check is on, we must do it here also
            ' but don't bother if nobody has registered for the event
            If (WatermarkCheckingEnabled = True) AndAlso (WatermarkNotifyEvent IsNot Nothing) Then
                tempCount = Me._ReadIndex - i ' change
                If tempCount < 0 Then
                    tempCount += Me._Capacity ' modulo buffer size
                End If

                If tempCount = WatermarkCount Then
                    OnWatermarkNotify()
                End If
            End If

            i += Me._add
            If i < 0 Then
                i += Me._len
            End If
            Me._ReadIndex = i
            updateCount()
            Return temp
        End SyncLock ' unlock
    End Function

    ''' <summary>
    ''' Reads the specified items from the circular buffer into the array.
    ''' </summary>
    ''' <param name="items">The items.</param>
    ''' <param name="index">The index.</param>
    ''' <param name="itemsCount">The items count.</param>
    Private Sub _Read(ByVal items As Array, ByVal index As System.Int32, ByVal itemsCount As System.Int32)

        If items Is Nothing Then Return
        SyncLock Me
            Dim i As System.Int32 = Me._ReadIndex
            Dim n As System.Int32 = itemsCount - index
            Dim offset As System.Int32 = index
            Dim tempCount As System.Int32

            ' A modification to the read index may be required
            ' if we have been operating in asynchronous saturated mode
            'if(SynchronousMode==false)
            '	if(Count >= (capacity-1))
            '		I = (n.r.p-1)%capacity;
            If Me.Count >= n Then
                Dim tempVar As Boolean = n > 0
                n -= 1
                Do While tempVar
                    items.SetValue(Me._buffer(i), offset)
                    offset += 1
                    ' for speed, we only update the count after the 
                    ' operation but if watermark check is on, we must 
                    ' do it here also but don't bother if nobody has
                    ' registered for the event
                    If (WatermarkCheckingEnabled = True) AndAlso (WatermarkNotifyEvent IsNot Nothing) Then
                        tempCount = Me._ReadIndex - i ' change
                        If tempCount < 0 Then
                            tempCount += Me._Capacity ' modulo buffer size
                        End If

                        If tempCount = WatermarkCount Then
                            OnWatermarkNotify()
                        End If
                    End If
                    i += Me._add
                    If i < 0 Then
                        i += Me._len
                    End If
                    tempVar = n > 0
                    n -= 1
                Loop
            Else
                Throw New IndexRangeException("Too few items in circular buffer")
            End If
            Me._ReadIndex = i
            updateCount()
        End SyncLock ' unlock
    End Sub

    Private Sub updateCount()
        If SynchronousMode = True Then
            Me._Count = Me._WriteIndex - Me._ReadIndex
            If Count < 0 Then
                Me._Count += Me._Capacity ' modulo buffer size
            End If
            ' if asynchronousMode, adjust the read index to follow the 
            ' write index if buffer is full (saturation)
        Else
            ' check to see if we are in saturation
            If Me._Count + Me._NumberToAdd >= (Capacity - 1) Then
                Me._ReadIndex = (Me._WriteIndex + Me._NumberToAdd) Mod Capacity
                ' and Count remains at max capacity
                Me._Count = Capacity - 1
            Else
                Me._Count = Me._WriteIndex - Me._ReadIndex
                If Count < 0 Then
                    Me._Count += Me._Capacity ' modulo buffer size
                End If
            End If
        End If

    End Sub
#End Region

#Region " IEnumerator Support "

    ''' <summary>
    ''' IEnumerator support
    ''' </summary>
    Public Function GetEnumerator() As CircularCollectionEnumerator
        Return New CircularCollection(Of T).CircularCollectionEnumerator(Me)
    End Function

    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return Me.GetEnumerator()
    End Function

    ''' <summary>
    ''' Enumerator class for the circular Collection
    ''' </summary>
    Public Class CircularCollectionEnumerator
        Implements IEnumerator

        Private buffer As CircularCollection(Of T)

        Friend Sub New(ByVal buffer As CircularCollection(Of T))
            Me.buffer = buffer
        End Sub

        ' IEnumerator support
        ''' <summary>
        ''' IEnumerator support
        ''' </summary>
        Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext
            SyncLock Me
                If buffer.Count = 0 Then
                    Return False
                Else
                    Return True
                End If
            End SyncLock
        End Function

        ''' <summary>
        ''' IEnumerator support
        ''' </summary>
    Public Sub Reset() Implements IEnumerator.Reset
        End Sub

        ''' <summary>
        ''' Gets the current element in the collection.
        ''' </summary>
    ''' <returns>The current element in the collection.</returns>
        '''   <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element.</exception>
        ''' <remarks>
        ''' Client must set through method.
        ''' </remarks>
        Private ReadOnly Property _Current() As Object Implements IEnumerator.Current
            Get
                SyncLock Me
                    Dim i As System.Int32 = buffer._ReadIndex
                    Dim temp As T

                    ' A modification to the read index may be required
                    ' if we have been operating in asynchronous 
                    ' saturated mode
                    If buffer.SynchronousMode = False Then
                        If buffer.Count >= (buffer.Capacity - 1) Then
                            i = (buffer._ReadIndex - 1) Mod buffer.Capacity
                        End If
                    End If

                    temp = buffer._buffer(i)
                    i += buffer._add
                    If i < 0 Then
                        i += buffer._len
                    End If
                    buffer._ReadIndex = i
                    buffer.updateCount()
                    Return (temp)
                End SyncLock ' unlock
            End Get
        End Property

        ''' <summary>
        ''' Gets the current element in the collection.
        ''' </summary>
    ''' <returns>The current element in the collection.</returns>
        '''   <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element.</exception>
            Public ReadOnly Property Current As T
            Get
                Return CType(Me._Current, T)
            End Get
        End Property

    End Class ' end class CBEnumerator
#End Region


End Class ' end class QueueCB

#Region "Blocking Support"

''' <summary>
''' Blocking Support Class.
''' </summary>
Friend Class CountEvent
    Implements IDisposable

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CountEvent" /> class.
    ''' </summary>
    Friend Sub New()
        MyBase.new()
        Me._LoadAutoEvent = New AutoResetEvent(False)
        Me._CopyAutoEvent = New AutoResetEvent(False)
        Me._CopyEventCount = 0
        Me._LoadEventCount = 0
    End Sub

    Private _LoadAutoEvent As AutoResetEvent
    Private _CopyAutoEvent As AutoResetEvent

    Private _LoadEventCount As System.Int32 = 0
    Private _CopyEventCount As System.Int32 = 0

    ''' <summary>
    ''' Requests to continue when the number specified can be written. 
    ''' </summary>
    ''' <param name="requestedCount">The requested count.</param>
    ''' <remarks>
    ''' WaitLoad() and SetLoad() are part of a blocked enqueue operation.
    ''' </remarks>
    Friend Sub WaitLoad(ByVal requestedCount As System.Int32)
        SyncLock Me
            Me._LoadEventCount = requestedCount ' wait for count to reach this value
        End SyncLock
        Me._LoadAutoEvent.WaitOne()
    End Sub

    ''' <summary>
    ''' Updates the count as items are removed from the circular buffer.
    ''' </summary>
    ''' <param name="requestedCount">The requested count.</param>
    ''' <remarks>
    ''' WaitLoad() and SetLoad() are part of a blocked enqueue operation.
    ''' </remarks>
    Friend Sub SetLoad(ByVal requestedCount As System.Int32)
        SyncLock Me
            If Me._LoadEventCount > 0 Then
                Me._LoadEventCount -= requestedCount ' queue has been emptied by this amount
                If Me._LoadEventCount <= 0 Then
                    Me._LoadAutoEvent.Set()
                End If
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Requests to continue when the number specified can be read.
    ''' </summary>
    ''' <param name="requestedCount">The requested count.</param>
    ''' <remarks>
    ''' WaitCopy() and SetCopy() are part of a blocked dequeue operation.
    ''' </remarks>
    Friend Sub WaitCopy(ByVal requestedCount As System.Int32)
        SyncLock Me
            Me._CopyEventCount = requestedCount ' countdown this value
        End SyncLock

        Me._CopyAutoEvent.WaitOne()
    End Sub

    ''' <summary>
    ''' Updates the count as items are written into the circular buffer.
    ''' </summary>
    ''' <param name="requestedCount">The requested count.</param>
    ''' <remarks>
    ''' WaitCopy() and SetCopy() are part of a blocked dequeue operation.
    ''' </remarks>
    Friend Sub SetCopy(ByVal requestedCount As System.Int32)
        SyncLock Me
            If Me._CopyEventCount > 0 Then
                Me._CopyEventCount -= requestedCount ' queue has been emptied by this amount
                If Me._CopyEventCount <= 0 Then
                    Me._CopyAutoEvent.Set()
                End If
            End If
        End SyncLock
    End Sub

#Region "IDisposable Support"

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._LoadAutoEvent IsNot Nothing Then
                        Me._LoadAutoEvent.Dispose()
                        Me._LoadAutoEvent = Nothing
                    End If
                    If Me._CopyAutoEvent IsNot Nothing Then
                        Me._CopyAutoEvent.Dispose()
                        Me._CopyAutoEvent = Nothing
                    End If
                End If
                ' TO_DO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TO_DO: set large fields to null.
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class ' end class CountEvent

#End Region

