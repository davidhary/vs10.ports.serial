﻿Imports System.ComponentModel

''' <summary>
''' Enumerates the key into the port parameters dictionary.
''' </summary>
Public Enum PortParameterKey
    <Description("None")> None
    <Description("Port Name")> PortName
    <Description("Baud Rate")> BaudRate
    <Description("Data Bits")> DataBits
    <Description("Parity")> Parity
    <Description("Stop Bits")> StopBits
    ''' <summary>
    ''' The number of bytes in the internal serial buffer before a data receive event occurs
    ''' </summary>
    <Description("Received Bytes Threshold")> ReceivedBytesThreshold
    ''' <summary>
    ''' The time in ms the Data Received handler waits.
    ''' </summary>
    <Description("Receive Delay")> ReceiveDelay
    ''' <summary>
    ''' Handshaking protocol.
    ''' </summary>
    <Description("Handshake")> Handshake
    ''' <summary>
    ''' Request to send signal is enabled.
    ''' </summary>
    <Description("RtsEnable")> RtsEnable
    <Description("Read Timeout")> ReadTimeout
    <Description("Read Buffer Size")> ReadBufferSize
    <Description("Write Buffer Size")> WriteBufferSize
End Enum

''' <summary>
''' Enumerates the data buffering option.
''' </summary>
Public Enum DataBufferingOption
    <Description("Not defined")> None
    <Description("Linear Buffer")> LinearBuffer
    <Description("Circular Buffer")> CircularBuffer
End Enum