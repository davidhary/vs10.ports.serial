﻿Imports System.IO
Imports System.IO.Ports
Imports System.Threading
''' <summary>
''' Defines a wrapper around the Visual Studio <see cref="SerialPort">serial port</see>
''' </summary>
''' <remarks>
''' Based on Extended Serial Port Windows Forms Sample
''' http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37
''' </remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Port

    Implements IPort

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' initialize a new instance
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        Me.New(New System.IO.Ports.SerialPort())
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Port" /> class.
    ''' </summary>
    ''' <param name="serialPort">The <see cref="SerialPort">serial port.</see></param>
    Public Sub New(ByVal serialPort As System.IO.Ports.SerialPort)

        MyBase.New()

        Me._receiveDelay = 1
        Me._SyncContext = SynchronizationContext.Current

        Me._timeoutTimer = New Timers.Timer

        ' create a new instance of the serial port.
        Me._serialPort = serialPort

        ' initialize the default port parameters.
        Me.FromPortParameters(Port.DefaultPortParameters)

        Me._InputBufferingOption = Serial.DataBufferingOption.LinearBuffer

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me.ConnectionChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ConnectionChangedEvent.GetInvocationList
                            RemoveHandler Me.ConnectionChanged, CType(d, Global.System.EventHandler(Of isr.Ports.Serial.ConnectionEventArgs))
                        Next
                    End If

                    If Me.DataReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DataReceivedEvent.GetInvocationList
                            RemoveHandler Me.DataReceived, CType(d, Global.System.EventHandler(Of isr.Ports.Serial.PortEventArgs))
                        Next
                    End If

                    If Me.DataSentEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DataSentEvent.GetInvocationList
                            RemoveHandler Me.DataSent, CType(d, Global.System.EventHandler(Of isr.Ports.Serial.PortEventArgs))
                        Next
                    End If

                    If Me.MessageAvailableEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                            RemoveHandler Me.MessageAvailable, CType(d, Global.System.EventHandler(Of isr.Ports.Serial.MessageEventArgs))
                        Next
                    End If

                    If Me.SerialPortErrorReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.SerialPortErrorReceivedEvent.GetInvocationList
                            RemoveHandler Me.SerialPortErrorReceived, CType(d, Global.System.EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs))
                        Next
                    End If

                    If Me.TimeoutEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.TimeoutEvent.GetInvocationList
                            RemoveHandler Me.Timeout, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If

                    If Me._timeoutTimer IsNot Nothing Then
                        Me._timeoutTimer.Enabled = False
                        Me._timeoutTimer.Dispose()
                        Me._timeoutTimer = Nothing
                    End If

                    If Me._serialPort IsNot Nothing Then
                        ' terminate the reference to the serial port
                        Me._serialPort.Dispose()
                        Me._serialPort = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " CONNECTION INFO "

    Private _receiveDelay As Integer
    ''' <summary>
    ''' Gets or sets the time in ms the Data Received handler waits.
    ''' </summary>
    Public Property ReceiveDelay() As Integer Implements IPort.ReceiveDelay
        Get
            Return Me._receiveDelay
        End Get
        Set(ByVal value As Integer)
            Me._receiveDelay = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the port parameters.
    ''' </summary>
    ''' <returns>A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></returns>
    Public Function ToPortParameters() As PortParametersDictionary Implements IPort.ToPortParameters
        If Me._serialPort IsNot Nothing Then
            Dim dix As New PortParametersDictionary
            dix.Add(PortParameterKey.PortName, Me._serialPort.PortName)
            dix.Add(PortParameterKey.BaudRate, Me._serialPort.BaudRate.ToString)
            dix.Add(PortParameterKey.DataBits, Me._serialPort.DataBits.ToString)
            dix.Add(PortParameterKey.Parity, Me._serialPort.Parity.ToString)
            dix.Add(PortParameterKey.StopBits, Me._serialPort.StopBits.ToString)
            dix.Add(PortParameterKey.ReceivedBytesThreshold, Me._serialPort.ReceivedBytesThreshold.ToString)
            dix.Add(PortParameterKey.Handshake, Me._serialPort.Handshake.ToString)
            dix.Add(PortParameterKey.RtsEnable, Me._serialPort.RtsEnable.ToString)
            dix.Add(PortParameterKey.ReadTimeout, Me._serialPort.ReadTimeout.ToString)
            dix.Add(PortParameterKey.ReadBufferSize, Me._serialPort.ReadBufferSize.ToString)
            dix.Add(PortParameterKey.WriteBufferSize, Me._serialPort.WriteBufferSize.ToString)
            dix.Add(PortParameterKey.ReceiveDelay, Me.ReceiveDelay.ToString)
            Return dix
        Else
            Return Port.DefaultPortParameters
        End If
    End Function

    ''' <summary>
    ''' Assign port parameters to the port.
    ''' </summary>
    ''' <param name="portParameters">A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></param>
    Public Sub FromPortParameters(ByVal portParameters As PortParametersDictionary) Implements IPort.FromPortParameters
        If Me._serialPort IsNot Nothing AndAlso portParameters IsNot Nothing Then
            With Me._serialPort
                .PortName = portParameters.Item(PortParameterKey.PortName)
                .BaudRate = CInt(portParameters.Item(PortParameterKey.BaudRate))
                .Parity = DirectCast([Enum].Parse(GetType(Parity), portParameters.Item(PortParameterKey.Parity)), Parity)
                .DataBits = CInt(portParameters.Item(PortParameterKey.DataBits))
                .StopBits = DirectCast([Enum].Parse(GetType(StopBits), portParameters.Item(PortParameterKey.StopBits)), StopBits)
                .ReceivedBytesThreshold = CInt(portParameters.Item(PortParameterKey.ReceivedBytesThreshold))
                .Handshake = DirectCast([Enum].Parse(GetType(Handshake), portParameters.Item(PortParameterKey.Handshake)), Handshake)
                .RtsEnable = Boolean.Parse(portParameters.Item(PortParameterKey.RtsEnable))
                ' 5000ms will never be reached because we read only bytes present in read buffer
                .ReadTimeout = CInt(portParameters.Item(PortParameterKey.ReadTimeout))
                .ReadBufferSize = CInt(portParameters.Item(PortParameterKey.ReadBufferSize))
                .WriteBufferSize = CInt(portParameters.Item(PortParameterKey.WriteBufferSize))
                .Encoding = System.Text.ASCIIEncoding.ASCII
            End With
            Me._receiveDelay = CInt(portParameters.Item(PortParameterKey.ReceiveDelay))
        End If
    End Sub

    Shared _DefaultPortParameters As PortParametersDictionary
    ''' <summary>
    ''' Returns the default port parameters.
    ''' </summary>
    ''' <returns>A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></returns>
    Public Shared Function DefaultPortParameters() As PortParametersDictionary
        If Port._DefaultPortParameters Is Nothing Then
            Port._DefaultPortParameters = New PortParametersDictionary
            Port._DefaultPortParameters.Add(PortParameterKey.PortName, "COM1")
            Port._DefaultPortParameters.Add(PortParameterKey.BaudRate, 9600.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.DataBits, 8.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.Parity, Parity.None.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.StopBits, StopBits.One.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.ReceivedBytesThreshold, 1.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.ReceiveDelay, 1.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.Handshake, Handshake.None.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.RtsEnable, False.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.ReadTimeout, 2000.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.ReadBufferSize, 4096.ToString)
            Port._DefaultPortParameters.Add(PortParameterKey.WriteBufferSize, 2048.ToString)
        End If
        Return Port._DefaultPortParameters
    End Function

#End Region

#Region " CONNECTION STATUS "

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected As Boolean Implements IPort.IsConnected
        Get
            Return Not Me._serialPort Is Nothing AndAlso Me._serialPort.IsOpen
        End Get
    End Property

#End Region

#Region " CONNECTION MANAGEMENT "

    Private WithEvents _serialPort As New SerialPort
    ''' <summary>
    ''' Gets or sets the <see cref="SerialPort">Serial Port</see>.
    ''' </summary>
    Public Property SerialPort As SerialPort Implements IPort.SerialPort
        Get
            Return Me._serialPort
        End Get
        Set(value As SerialPort)
            Me._serialPort = value
            Me.SafeInvokeConnectionChanged()
        End Set
    End Property

    ''' <summary>
    ''' Connects the port. 
    ''' </summary>
    ''' <param name="portParameters">A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></param>
    ''' <returns>True if connected; otherwise, false.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function Connect(ByVal portParameters As PortParametersDictionary) As Boolean Implements IPort.Connect
        If portParameters Is Nothing Then
            Throw New ArgumentNullException("portParameters")
        End If
        Dim wasOpen As Boolean = Me.IsConnected
        Try
            Me.SafeInvokeMessageAvailable(TraceEventType.Verbose, "Connecting...")
            Me.FromPortParameters(portParameters)
            ' open and check device if is available?
            Me._serialPort.Open()

        Catch ex As IOException
            Me.SafeInvokeMessageAvailable(ex, "Connection failed @ Connect(PortParametersDictionary)")
        Catch ex As Exception
            Me.SafeInvokeMessageAvailable(ex, "Connection failed @ Connect(PortParametersDictionary)")
        Finally
            ' if port was closed and is now open
            If wasOpen <> Me.IsConnected Then
                Me.SafeInvokeConnectionChanged()
            End If
        End Try
        Return Me.IsConnected
    End Function

    ''' <summary>
    ''' Disconnects the port
    ''' </summary>
    ''' <returns>True if disconnected; otherwise, false.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function Disconnect() As Boolean Implements IPort.Disconnect
        Dim isOpen As Boolean = Me.IsConnected
        Try
            Me._serialPort.DiscardInBuffer()
            Me._serialPort.Close()
        Catch ex As Exception
            Me.SafeInvokeMessageAvailable(ex, "@Disconnect()")
        Finally
            If isOpen <> Me.IsConnected Then
                Me.SafeInvokeConnectionChanged()
            End If
        End Try
        Return Not Me.IsConnected
    End Function

#End Region

#Region " BUFFERS "

    ''' <summary>
    ''' Holds received data.
    ''' </summary>
    Private _inputBuffer() As Byte

    ''' <summary>
    ''' Holds the transmitted data.
    ''' </summary>
    Private _outputBuffer() As Byte


    ''' <summary>
    ''' Resynchronizes the circular buffer or clears the receive buffer.
    ''' </summary>
    Public Sub Resync() Implements IPort.Resync
        If Me.InputBufferingOption = DataBufferingOption.CircularBuffer Then
            Me._receiveBuffer.Resync()
        Else
            Me._serialPort.ReadExisting()
        End If
    End Sub

#End Region

#Region " CIRCULAR BUFFER "

    Private WithEvents _receiveBuffer As CircularCollection(Of Byte)

    ''' <summary>
    ''' Returns the data count in the circular buffer.
    ''' </summary>
    Public Function DataCount() As Integer Implements IPort.DataCount
        Return Me._receiveBuffer.Count
    End Function

    ''' <summary>
    ''' Reads the next byte from the circular buffer.
    ''' </summary>
    Public Function ReadNext() As Byte Implements IPort.ReadNext
        Return Me._receiveBuffer.Dequeue
    End Function

    ''' <summary>
    ''' Notify of buffer overflow.
    ''' </summary>
    Private Sub _buffer_WaterMarkNotify(sender As Object, e As System.EventArgs) Handles _receiveBuffer.WatermarkNotify
    End Sub

    Private _InputBufferingOption As DataBufferingOption
    ''' <summary>
    ''' Gets or sets the data buffering option.
    ''' </summary>
    ''' <value>The data buffering option.</value>
    Public Property InputBufferingOption As DataBufferingOption Implements IPort.InputBufferingOption
        Get
            Return Me._InputBufferingOption
        End Get
        Set(value As DataBufferingOption)
            Me._InputBufferingOption = value
            If value = DataBufferingOption.CircularBuffer AndAlso (Me._receiveBuffer Is Nothing OrElse 0 > _receiveBuffer.Capacity) Then
                Me._receiveBuffer = New CircularCollection(Of Byte)(1024)
                Me._inputBuffer = New Byte() {}
            End If
        End Set
    End Property

#End Region

#Region " SERIAL PORT EVENTS "

    Private _sendLock As Object
    ''' <summary>
    ''' Sends data
    ''' </summary>
    ''' <param name="data">byte array data</param>
    ''' <remarks>Clears the input buffer.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub SendData(ByVal data() As Byte) Implements IPort.SendData
        If Me._sendLock Is Nothing Then
            Me._sendLock = New Object
        End If
        Dim portEventArgs As PortEventArgs = Nothing
        Try
            SyncLock Me._sendLock
                ' clear the input buffer in preparation from the returned message.
                Me._inputBuffer = New Byte() {}
                If data Is Nothing OrElse data.Length = 0 Then
                    Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Attempt at sending data ignored -- nothing to send")
                    Me._outputBuffer = New Byte() {}
                    portEventArgs = New PortEventArgs(False, Me._outputBuffer)
                Else
                    Me._outputBuffer = New Byte(data.Length - 1) {}
                    data.CopyTo(Me._outputBuffer, 0)
                    If Me._outputBuffer.Length < 192 Then
                        Me.SafeInvokeMessageAvailable(TraceEventType.Verbose, "sending {0}", Me._outputBuffer.ToHex)
                    Else
                        Me.SafeInvokeMessageAvailable(TraceEventType.Verbose, "sending {0} bytes", Me._outputBuffer.Length)
                    End If
                    Me._serialPort.Write(Me._outputBuffer, 0, Me._outputBuffer.Length)
                    Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Data sent.")
                    portEventArgs = New PortEventArgs(True, Me._outputBuffer)
                End If
            End SyncLock
        Catch ex As Exception
            portEventArgs = New PortEventArgs(False, Me._outputBuffer)
            SafeInvokeMessageAvailable(ex, "@ SendData(byte())")
        Finally
            SafeInvokeDataSent(portEventArgs)
        End Try

    End Sub

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.IO.Ports.SerialDataReceivedEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _serialPort_DataReceived(ByVal sender As System.Object,
                                         ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles _serialPort.DataReceived
        Dim portEventArgs As PortEventArgs = Nothing
        Try
            If Me.IsConnected Then
                Thread.Sleep(Me._receiveDelay)
                Dim len As Integer = Me._serialPort.BytesToRead
                If len > 0 Then
                    Me.SafeInvokeMessageAvailable(TraceEventType.Verbose, "Reading {0} bytes from the port...", len)
                    ' append the input buffer.
                    If Me._inputBuffer Is Nothing Then
                        Me._inputBuffer = New Byte() {}
                    End If
                    Dim startIndex As Integer = Me._inputBuffer.Length
                    Array.Resize(Me._inputBuffer, startIndex + len)
                    Me._serialPort.Read(Me._inputBuffer, startIndex, len)
                    If Me._inputBuffer.Length < 192 Then
                        Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Received {0}", Me._inputBuffer.ToHex)
                    Else
                        Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Received {0} bytes", Me._inputBuffer.Length)
                    End If
                    If Me.MessageParser IsNot Nothing Then
                        ' if the parser is defined, use it to parse the message. 
                        Dim outcome As MessageParserOutcome = Me.MessageParser.Parse(Me._inputBuffer)
                        If outcome = MessageParserOutcome.Complete Then
                            portEventArgs = New PortEventArgs(True, Me._inputBuffer)
                            Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Message complete.")
                        ElseIf outcome = MessageParserOutcome.Invalid Then
                            portEventArgs = New PortEventArgs(False, Me._inputBuffer)
                            Me.SafeInvokeMessageAvailable(TraceEventType.Warning, "Failed parsing the input message -- message is invalid.")
                        Else
                            ' if not complete, wait for more characters.
                            Me.SafeInvokeMessageAvailable(TraceEventType.Information, "Message incomplete -- waiting for more characters.")
                        End If
                    Else
                        Me.SafeInvokeMessageAvailable(TraceEventType.Verbose, "Message received.")
                        portEventArgs = New PortEventArgs(True, Me._inputBuffer)
                    End If
                End If
            Else
                Me.SafeInvokeMessageAvailable(TraceEventType.Warning, "Data received event aborted - port no longer connected.")
                Me._serialPort.DiscardInBuffer()
                portEventArgs = New PortEventArgs(False, Me._inputBuffer)
            End If
        Catch ex As Exception
            portEventArgs = New PortEventArgs(False, Me._inputBuffer)
            Me.SafeInvokeMessageAvailable(ex, "@port_DataReceived(Object, SerialDataReceivedEventArgs)")
        Finally
            If portEventArgs IsNot Nothing Then
                Me.SafeInvokeDataReceived(portEventArgs)
            End If
        End Try

    End Sub

#End Region

#Region " MESSAGE PARSER "

    ''' <summary>
    ''' Gets or sets the message parser.
    ''' </summary>
    Property MessageParser As IMessageParser Implements IPort.MessageParser

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Propagates a sync context.
    ''' </summary>
    Private _SyncContext As SynchronizationContext

#Region " SERIAL PORT ERROR RECEIVED "

    ''' <summary>
    ''' Occurs when Serial Port Error Received.
    ''' SerialPortErrorReceived status is reported with the <see cref="SerialErrorReceivedEventArgs">SerialPortErrorReceived event arguments.</see>
    ''' </summary>
    Public Event SerialPortErrorReceived As EventHandler(Of SerialErrorReceivedEventArgs) Implements IPort.SerialPortErrorReceived

    ''' <summary>
    ''' Invokes the <see cref="SerialPortErrorReceived">Serial Port Error Received event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeSerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        Try
            UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                              "Serial port error type {0} occurred", e.EventType))
            Dim evt As EventHandler(Of SerialErrorReceivedEventArgs) = Me.SerialPortErrorReceivedEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            Me.UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                                  BuildMessage(ex, "@onSerialPortErrorReceived(SerialErrorReceivedEventArgs)")))
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="SerialPortErrorReceived">Serial Port Error Received event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub UnsafeInvokeSerialPortErrorReceived(ByVal obj As Object)
        Me.UnsafeInvokeSerialPortErrorReceived(CType(obj, SerialErrorReceivedEventArgs))
    End Sub

    ''' <summary>
    ''' Safely posts the <see cref="DataSent">data Sent event</see>.
    ''' Uses the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="PortEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeSerialPortErrorReceived(ByVal e As SerialErrorReceivedEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeSerialPortErrorReceived(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeSerialPortErrorReceived), e)
        End If
    End Sub

    ''' <summary>
    ''' Handles the ErrorReceived event of the _serialPort control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.IO.Ports.SerialErrorReceivedEventArgs" /> instance containing the event data.</param>
    Private Sub _serialPort_ErrorReceived(sender As Object, e As System.IO.Ports.SerialErrorReceivedEventArgs) Handles _serialPort.ErrorReceived
        SafeInvokeSerialPortErrorReceived(e)
    End Sub

#End Region

#Region " CONNECTION CHANGED "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Public Event ConnectionChanged As EventHandler(Of ConnectionEventArgs) Implements IPort.ConnectionChanged

    ''' <summary>
    ''' Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.ConnectionEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeConnectionChanged(ByVal e As ConnectionEventArgs)
        Try
            ' this is already thread save....
            If IsConnected Then
                Me.UnsafeInvokeMessageAvailable(New MessageEventArgs(BuildMessage("Connected")))
            Else
                Me.UnsafeInvokeMessageAvailable(New MessageEventArgs(BuildMessage("Disconnected")))
            End If
            Dim evt As EventHandler(Of ConnectionEventArgs) = Me.ConnectionChangedEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            Me.UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                                  BuildMessage(ex, "@onConnectionChanged(ConnectionEventArgs)")))
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub UnsafeInvokeConnectionChanged(ByVal obj As Object)
        Me.UnsafeInvokeConnectionChanged(CType(obj, ConnectionEventArgs))
    End Sub

    ''' <summary>
    ''' Safely posts the <see cref="DataSent">data Sent event</see>.
    ''' Uses the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="PortEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeConnectionChanged(ByVal e As ConnectionEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeConnectionChanged(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeConnectionChanged), e)
        End If
    End Sub

    ''' <summary>
    ''' Safely Invokes the <see cref="ConnectionChanged">connection changed event</see>.
    ''' </summary>
    Private Sub SafeInvokeConnectionChanged()
        SafeInvokeConnectionChanged(New ConnectionEventArgs(Me.IsConnected))
    End Sub

#End Region

#Region " DATA RECEIVED "

    ''' <summary>
    ''' Occurs when data is received.
    ''' Reception status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Public Event DataReceived As EventHandler(Of PortEventArgs) Implements IPort.DataReceived

    ''' <summary>
    ''' Invokes the <see cref="DataReceived">data received event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeDataReceived(ByVal e As PortEventArgs)
        Try
            If Me._timeoutTimer IsNot Nothing Then
                Me._timeoutTimer.Enabled = False
            End If
            Dim evt As EventHandler(Of PortEventArgs) = Me.DataReceivedEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                              BuildMessage(ex, "@onDataReceived(PortEventArgs")))
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="DataReceived">data received event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    Private Sub UnsafeInvokeDataReceived(ByVal obj As Object)
        Me.UnsafeInvokeDataReceived(CType(obj, PortEventArgs))
    End Sub

    ''' <summary>
    ''' Safely posts the <see cref="DataReceived">data received event</see>.
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeDataReceived(ByVal e As PortEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeDataReceived(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeDataReceived), e)
        End If
    End Sub

#End Region

#Region " DATA SENT "

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Reception status is report along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Public Event DataSent As EventHandler(Of PortEventArgs) Implements IPort.DataSent

    ''' <summary>
    ''' Invokes the <see cref="DataSent">data Sent event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeDataSent(ByVal e As PortEventArgs)
        Try
            If e.StatusOkay AndAlso Me._serialPort.ReadTimeout > 0 Then
                Me._timeoutTimer.AutoReset = False
                Me._timeoutTimer.Interval = Me._serialPort.ReadTimeout
                Me._timeoutTimer.Enabled = True
            End If
            Dim evt As EventHandler(Of PortEventArgs) = Me.DataSentEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                              BuildMessage(ex, "@onDataSent(PortEventArgs")))
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="DataSent">data Sent event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub UnsafeInvokeDataSent(ByVal obj As Object)
        Me.UnsafeInvokeDataSent(CType(obj, PortEventArgs))
    End Sub

    ''' <summary>
    ''' Safely posts the <see cref="DataSent">data Sent event</see>.
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.PortEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeDataSent(ByVal e As PortEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeDataSent(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeDataSent), e)
        End If
    End Sub

#End Region

#Region " MESSAGE AVAILABLE "

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of MessageEventArgs) Implements IPort.MessageAvailable

    ''' <summary>
    ''' Invokes the <see cref="MessageAvailable">Message Available event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Ports.Serial.MessageEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeMessageAvailable(ByVal e As MessageEventArgs)
        Try
            Dim evt As EventHandler(Of MessageEventArgs) = Me.MessageAvailableEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="MessageAvailable">Message Available event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub UnsafeInvokeMessageAvailable(ByVal obj As Object)
        Me.UnsafeInvokeMessageAvailable(CType(obj, MessageEventArgs))
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="MessageAvailable">message available event</see>.
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal e As MessageEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeMessageAvailable(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeMessageAvailable), e)
        End If
    End Sub

    ''' <summary>
    ''' Safely invokes the message available event.
    ''' </summary>
    ''' <param name="ex">The ex.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        SafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error, BuildMessage(ex, format, args)))
    End Sub

    ''' <summary>
    ''' Safely invokes the message available event.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal severity As Diagnostics.TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        SafeInvokeMessageAvailable(New MessageEventArgs(severity, BuildMessage(format, args)))
    End Sub

    ''' <summary>
    ''' Safely invokes the message available event.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        SafeInvokeMessageAvailable(New MessageEventArgs(severity, BuildMessage(details)))
    End Sub

    ''' <summary>
    ''' Builds the message.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Function BuildMessage(ByVal format As String, ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}: {1}", Me._serialPort.PortName,
                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Build an exception message.
    ''' </summary>
    ''' <param name="ex">The exception.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Function BuildMessage(ByVal ex As Exception, ByVal format As String, ParamArray args() As Object) As String
        Return Me.BuildMessage("{2}. {0}Details: {1}", Environment.NewLine, ex,
                                String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

#End Region

#End Region

#Region " TIMEOUT MANAGER "

    ''' <summary>
    ''' Occurs when timeout.
    ''' </summary>
    Public Event Timeout As EventHandler(Of System.EventArgs) Implements IPort.Timeout

    ''' <summary>
    ''' Invokes the <see cref="Timeout">Timeout event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event Message.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnsafeInvokeTimeout(ByVal e As System.EventArgs)
        Try
            Dim evt As EventHandler(Of System.EventArgs) = Me.TimeoutEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            UnsafeInvokeMessageAvailable(New MessageEventArgs(TraceEventType.Error,
                                                              BuildMessage(ex, "@onTimeout(System.EventArgs)")))
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="Timeout">Timeout event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub UnsafeInvokeTimeout(ByVal obj As Object)
        Me.UnsafeInvokeTimeout(CType(obj, System.EventArgs))
    End Sub

    ''' <summary>
    ''' Handles the timeout management
    ''' </summary>
    Private WithEvents _timeoutTimer As Timers.Timer

    ''' <summary>
    ''' Handles the Elapsed event of the _timeoutTimer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Timers.ElapsedEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _timeoutTimer_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles _timeoutTimer.Elapsed
        Try
            If Me._SyncContext Is Nothing Then
                UnsafeInvokeTimeout(System.EventArgs.Empty)
            Else
                Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeTimeout), System.EventArgs.Empty)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
            Me.SafeInvokeMessageAvailable(ex, "@_timeoutTimer_Elapsed(Object, SerialDataReceivedEventArgs)")
        End Try
    End Sub

#End Region

End Class

