﻿''' <summary>
''' Defines the interface for the <see cref="Port">serial port</see>
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Interface IPort

    Inherits IDisposable

#Region " CONNECTION INFO "

    ''' <summary>
    ''' Gets or sets the time in ms the Data Received handler waits.
    ''' </summary>
    Property ReceiveDelay() As Integer

    ''' <summary>
    ''' Returns the port parameters.
    ''' </summary>
    ''' <returns>A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see>
    ''' Includes, in addition to the standard parameters, the Threshold count, Delay time in milliseconds and timeout in milliseconds.</returns>
    Function ToPortParameters() As PortParametersDictionary

    ''' <summary>
    ''' Assign port parameters to the port.
    ''' </summary>
    ''' <param name="portParameters">A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></param>
    Sub FromPortParameters(ByVal portParameters As PortParametersDictionary)

#End Region

#Region " CONNECTION STATUS "

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    ReadOnly Property IsConnected As Boolean

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary>
    ''' Gets or sets the <see cref="System.IO.Ports.SerialPort">Serial Port</see>.
    ''' </summary>
    Property SerialPort As System.IO.Ports.SerialPort

    ''' <summary>
    ''' Connects the port. 
    ''' </summary>
    ''' <param name="portParameters">A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the 
    ''' <see cref="PortParameterKey">parameter key</see></param>
    ''' <returns>True if connected; otherwise, false.</returns>
    Function Connect(ByVal portParameters As PortParametersDictionary) As Boolean

    ''' <summary>
    ''' Disconnects the port
    ''' </summary>
    ''' <returns>True if disconnected; otherwise, false.</returns>
    Function Disconnect() As Boolean

#End Region

#Region " DATA MANAGEMENT "

    ''' <summary>
    ''' Returns the data count in the circular buffer.
    ''' </summary>
    Function DataCount() As Integer

    ''' <summary>
    ''' Reads the next byte from the circular buffer.
    ''' </summary>
    Function ReadNext() As Byte

    ''' <summary>
    ''' Resynchronizes the circular buffer.
    ''' </summary>
    Sub Resync()

    ''' <summary>
    ''' Sends data
    ''' </summary>
    ''' <param name="data">byte array data</param>
    Sub SendData(ByVal data() As Byte)

    ''' <summary>
    ''' Gets or sets the input (receive) data buffering option.
    ''' </summary>
    ''' <value>The data buffering option.</value>
    Property InputBufferingOption As DataBufferingOption

#End Region

#Region " MESSAGE PARSER "

    ''' <summary>
    ''' Gets or sets the message parser.
    ''' </summary>
    Property MessageParser As IMessageParser

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Occurs when connection changed.
    ''' Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
    ''' </summary>
    Event ConnectionChanged As EventHandler(Of ConnectionEventArgs)

    ''' <summary>
    ''' Occurs when data is received.
    ''' Reception status is report along with the received data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataReceived As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when data is Sent.
    ''' Reception status is report along with the Sent data in the receive buffer using the <see cref="PortEventArgs">port event arguments.</see>
    ''' </summary>
    Event DataSent As EventHandler(Of PortEventArgs)

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' </summary>
    Event MessageAvailable As EventHandler(Of MessageEventArgs)

    ''' <summary>
    ''' Occurs when Serial Port Error Received.
    ''' SerialPortErrorReceived status is reported with the 
    ''' <see cref="System.IO.Ports.SerialErrorReceivedEventArgs">SerialPortErrorReceived event arguments.</see>
    ''' </summary>
    Event SerialPortErrorReceived As EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs)

    ''' <summary>
    ''' Occurs when timeout.
    ''' </summary>
    Event Timeout As EventHandler(Of System.EventArgs)

#End Region

End Interface

