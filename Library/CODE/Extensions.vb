﻿Public Module Extensions

    ''' <summary>
    ''' Determines whether the specified value is hex type.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns><c>True</c> if the specified value is allowed; otherwise, <c>False</c>.</returns>
    <Runtime.CompilerServices.Extension()>
    Public Function IsHexadecimal(ByVal value As Char) As Boolean
        Dim allowedChars As String = "0123456789ABCDEFabcdef"
        Return Not String.IsNullOrWhiteSpace(value) AndAlso allowedChars.Contains(value)
    End Function

    ''' <summary>
    ''' Builds the hex string by adding delimiter between each pair of nibbles.
    ''' </summary>
    ''' <param name="value">The value.</param>
    <Runtime.CompilerServices.Extension()>
    Public Function ToByteFormatted(ByVal value As String, ByVal delimiter As String) As String
        Return value.RemoveDelimiter(delimiter).InsertDelimiter(2, delimiter)
    End Function

    ''' <summary>
    ''' Inserts the delimiter.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="everyLength">Length of the every.</param>
    ''' <param name="delimiter">The delimiter.</param>
    <Runtime.CompilerServices.Extension()>
    Public Function InsertDelimiter(ByVal value As String, ByVal everyLength As Integer, ByVal delimiter As String) As String
        If String.IsNullOrWhiteSpace(value) Then Return value
        If String.IsNullOrWhiteSpace(delimiter) Then Return value
        Dim sb As New System.Text.StringBuilder
        For i As Integer = 0 To value.Length - 1 Step everyLength
            If sb.Length > 0 Then
                sb.Append(delimiter)
            End If
            Dim maxLength As Integer = Math.Min(value.Length - i, everyLength)
            sb.Append(value.Substring(i, maxLength))
        Next
        Return sb.ToString
    End Function

    ''' <summary>
    ''' Removes the delimiter.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="delimiter">The delimiter.</param>
    <Runtime.CompilerServices.Extension()>
    Public Function RemoveDelimiter(ByVal value As String, ByVal delimiter As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        Else
            If String.IsNullOrWhiteSpace(delimiter) Then
                Return value
            Else
                Dim values() As String = value.Split(New String() {delimiter}, StringSplitOptions.RemoveEmptyEntries)
                Dim sb As New System.Text.StringBuilder
                For Each v As String In values
                    sb.Append(v.Trim)
                Next
                Return sb.ToString
            End If
        End If
    End Function


    ''' <summary>
    ''' Convert a HEX string to its representing binary values.
    ''' </summary>
    ''' <param name="hexText">The hex values.</param>
    ''' <returns>The converted bytes.</returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHexBytes(ByVal hexText As String) As Byte()
        If String.IsNullOrWhiteSpace(hexText) Then
            Return New Byte() {}
        Else
            Dim d As String = " "
            Return ToHexBytes(InsertDelimiter(hexText, 2, d), d)
        End If
    End Function

    ''' <summary>
    ''' Convert a delimited HEX string to its representing binary values.
    ''' </summary>
    ''' <param name="hexText">The hex values.</param>
    ''' <param name="delimiter">The delimiter.</param>
    ''' <returns>The converted bytes. If error, the first byte is set to 255 and the second byte is set to the ASCII value of the 
    ''' offending character.</returns>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHexBytes(ByVal hexText As String, ByVal delimiter As String) As Byte()
        Dim data() As Byte = New Byte() {}
        If Not String.IsNullOrWhiteSpace(hexText) Then
            If String.IsNullOrWhiteSpace(hexText) Then Return data
            Dim s() As String = hexText.Split(New String() {delimiter}, StringSplitOptions.RemoveEmptyEntries)
            data = New Byte(s.Length - 1) {}
            For i As Integer = 0 To data.Length - 1
                If Not Byte.TryParse(s(i), Globalization.NumberStyles.HexNumber, Globalization.CultureInfo.CurrentCulture, data(i)) Then
                    ' if error, tag the first byte at 255 and point to the location of the error.
                    data = New Byte(1) {}
                    data(0) = 255
                    data(1) = Asc(CChar(s(i)))
                End If
            Next
        End If
        Return data
    End Function

    ''' <summary>Returns an HEX string using 2 nibble presentation.
    ''' </summary>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X2}", value)
    End Function

    ''' <summary>
    ''' Returns an HEX string using 2 nibble presentation.
    ''' </summary>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte()) As String
        If value Is Nothing Then
            Return ""
        Else
            Dim sb As New System.Text.StringBuilder
            If value IsNot Nothing Then
                For Each b In value
                    sb.Append(b.ToHex)
                Next
            End If
            Return sb.ToString
        End If
    End Function

    ''' <summary>
    ''' Returns an HEX string using 2 nibble presentation.
    ''' </summary>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte(), ByVal startIndex As Integer, ByVal length As Integer) As String
        If value Is Nothing Then
            Return ""
        Else
            Dim sb As New System.Text.StringBuilder
            If value IsNot Nothing Then
                If value.Length > startIndex Then
                    length = Math.Min(length, value.Length - startIndex)
                    For i As Integer = startIndex To startIndex + length - 1
                        sb.Append(value(i).ToHex)
                    Next
                End If
            End If
            Return sb.ToString
        End If
    End Function

    ''' <summary>Returns an HEX string.
    ''' </summary>
    <Runtime.CompilerServices.Extension()>
    Public Function ToHex(ByVal value As Byte, ByVal nibbleCount As Byte) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
    End Function

    ''' <summary>
    ''' Returns a byte.
    ''' </summary>
    ''' <param name="value">The value.</param>
    <Runtime.CompilerServices.Extension()>
    Public Function ToByte(ByVal value As String) As Byte
        If String.IsNullOrWhiteSpace(value) Then
            Throw New ArgumentNullException("value")
        End If
        Return Byte.Parse(value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary>
    ''' Returns an integer.
    ''' </summary>
    ''' <param name="value">The value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="integer")>
    <Runtime.CompilerServices.Extension()>
    Public Function ToInteger(ByVal value As String) As Integer
        If String.IsNullOrWhiteSpace(value) Then
            Throw New ArgumentNullException("value")
        End If
        Return Integer.Parse(value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary>
    ''' Sets the ruler on a <see cref="Windows.Forms.RichTextBox">Rich Text Box</see>.
    ''' </summary>
    ''' <param name="textBox">The <see cref="Windows.Forms.RichTextBox">Rich Text Box</see>.</param>
    ''' <param name="characterWidth">Width of the character.</param>
    ''' <param name="isHex">if set to <c>True</c> if displaying hex characters.</param>
    ''' <returns>length of ruler</returns>
    <Runtime.CompilerServices.Extension()>
    Public Function SetRuler(ByVal textBox As System.Windows.Forms.RichTextBox, ByVal characterWidth As Single, ByVal isHex As Boolean) As Integer

        If textBox Is Nothing Then Return 0
        Dim rbWidth As Integer = textBox.Width
        Dim s As New System.Text.StringBuilder
        Dim anzMarks As Integer

        If Not isHex Then
            anzMarks = CInt((rbWidth / characterWidth) / 5)
            For i As Integer = 1 To anzMarks
                If i < 2 Then
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "    {0:0}", i * 5))
                ElseIf i < 20 Then
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "   {0:00}", i * 5))
                Else
                    s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "  {0:000}", i * 5))
                End If
            Next
        Else
            anzMarks = CInt((rbWidth / characterWidth) / 3)
            For i As Integer = 1 To anzMarks
                s.Append(String.Format(Globalization.CultureInfo.CurrentCulture, " {0:00}", i))
            Next
        End If

        ' coloring ruler
        Dim cl As System.Drawing.Color = textBox.BackColor
        textBox.Select(0, textBox.Lines(0).Length)
        textBox.SelectionBackColor = System.Drawing.Color.LightGray
        textBox.SelectedText = s.ToString
        If textBox.Lines.Length = 1 Then textBox.AppendText(vbCr)
        textBox.SelectionBackColor = cl
        textBox.SelectionLength = 0
        Return s.Length

    End Function

    ''' <summary>
    ''' Append data bytes to the <see cref="Windows.Forms.TextBoxBase">Text Box</see>.
    ''' </summary>
    ''' <param name="textBox">The <see cref="Windows.Forms.TextBoxBase">Text Box</see>.</param>
    ''' <param name="data">data frame</param>
    ''' <param name="currentLength">Max chars in box</param>
    ''' <param name="showHexAndAscii">True if displaying both hex and ASCII; Otherwise, False.</param>
    <Runtime.CompilerServices.Extension()>
    Public Sub AppendBytes(ByVal textBox As System.Windows.Forms.TextBoxBase, ByVal data() As Byte, ByVal currentLength As Integer,
                           ByVal showHexAndAscii As Boolean)

        If textBox Is Nothing Then Return
        If data Is Nothing OrElse data.Length = 0 Then Return
        Dim HexString As New System.Text.StringBuilder
        Dim CharString As New System.Text.StringBuilder
        Dim count As Integer = 0

        For i As Integer = 0 To data.Length - 1

            HexString.Append(String.Format(Globalization.CultureInfo.CurrentCulture, " {0:X2}", data(i)))
            If data(i) > 31 Then
                CharString.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "  {0}", Chr(data(i))))
            Else
                CharString.Append("  .")
            End If
            count += 3

            ' start a new line
            If count >= currentLength Then
                textBox.ScrollToCaret()
                HexString.AppendLine()
                textBox.AppendText(HexString.ToString)
                If showHexAndAscii Then
                    textBox.ScrollToCaret()
                    CharString.AppendLine()
                    textBox.AppendText(CharString.ToString)
                End If
                HexString = New System.Text.StringBuilder
                CharString = New System.Text.StringBuilder
                count = 0
            End If

        Next

        textBox.ScrollToCaret()
        HexString.AppendLine()
        textBox.AppendText(HexString.ToString)
        If showHexAndAscii Then
            textBox.ScrollToCaret()
            CharString.AppendLine()
            textBox.AppendText(CharString.ToString)
        End If
    End Sub

End Module
