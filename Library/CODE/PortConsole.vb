﻿Imports System.Text
Imports System.Globalization
Imports System.IO
Imports System.IO.Ports
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Threading
Imports System.ComponentModel

''' <summary>
''' Hosts the <see cref="IPort">port</see>.
''' </summary>
''' <remarks>
''' Based on Extended Serial Port Windows Forms Sample
''' http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37
''' </remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class PortConsole

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortConsole" /> class.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        Me.New(New Port)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortConsole" /> class.
    ''' </summary>
    ''' <param name="port">Specifies the <see cref="IPort">port</see>.</param>
    Public Sub New(ByVal port As IPort)

        MyBase.New()

        Me._SyncContext = SynchronizationContext.Current
        Me._errorProvider = New ErrorProvider
        Me._port = port
        characterWidth = 8.3 ' pixels / Char 
        charsInline = 80  ' def n start
        configFileName = My.Application.Info.AssemblyName & ".ini"
        Me.InitializeComponent()

    End Sub

    ''' <summary>
    ''' Disposes the managed resources.
    ''' </summary>
    Private Sub disposeManagedResources()

        If Me.MessageAvailableEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                RemoveHandler Me.MessageAvailable, CType(d, Global.System.EventHandler(Of MessageEventArgs))
            Next
        End If

        If Me.ViewMessageAvailableEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.ViewMessageAvailableEvent.GetInvocationList
                RemoveHandler Me.ViewMessageAvailable, CType(d, Global.System.EventHandler(Of MessageEventArgs))
            Next
        End If

        If Me._port IsNot Nothing Then
            Me._port.Dispose()
            Me._port = Nothing
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Handles the Load event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.onFormShown()
        Catch ex As Exception
            MessageBox.Show(Me, ex.ToString, "Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the ResizeEnd event of the form control.
    ''' Adjusts the character counts. 
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_ResizeEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            Me.charsInline = Me._ReceiveTextBox.SetRuler(Me.characterWidth, Me._ReceiveShowHEXToolStripMenuItem.Checked)
            Me.charsInline = Me._TransmitTextBox.SetRuler(Me.characterWidth, Me._TransmitShowHEXToolStripMenuItem.Checked)
        Catch ex As Exception
            SafeInvokeViewMessageAvailable(ex, "@form_ResizeEnd(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Clears the selections.
    ''' A workaround to prevent highlights. 
    ''' </summary>
    Private Sub ClearSelections()
        Me._PortNamesCombo.ComboBox.Select(0, 0)
        Me._PortNamesCombo.ComboBox.Select(0, 0)
        Me._BaudRateCombo.ComboBox.Select(0, 0)
        Me._DataBitsCombo.ComboBox.Select(0, 0)
        Me._ParityCombo.ComboBox.Select(0, 0)
        Me._StopBitsCombo.ComboBox.Select(0, 0)
        Me._ReceiveDelayCombo.ComboBox.Select(0, 0)
        Me._ReceiveThresholdCombo.ComboBox.Select(0, 0)
    End Sub

    Private Sub MessengerConsole_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
        Me.ClearSelections()
    End Sub

    ''' <summary>
    ''' Handles the display of the controls.
    ''' </summary>
    Private Sub onFormShown()

        ' set the form caption
        Me.Text = ApplicationInfo.BuildDefaultCaption("")

        Me.Text &= String.Format(Globalization.CultureInfo.CurrentCulture,
                                 " Version {0}.{1:00}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        Me._StatusLabel.Text = "not connected"
        Me._ReceiveTextBox.SetRuler(Me.characterWidth, False)
        Me._TransmitTextBox.SetRuler(Me.characterWidth, False)

        Dim portNames As String() = SerialPort.GetPortNames
        Me._PortNamesCombo.Items.Clear()
        Me._PortNamesCombo.Items.AddRange(portNames)

        ' read available ports on system
        If portNames.Length > 0 Then
            Me._PortNamesCombo.Text = portNames(0)
            For Each pn As String In portNames
                If pn.Equals(My.Settings.PortName, System.StringComparison.OrdinalIgnoreCase) Then
                    Me._PortNamesCombo.Text = My.Settings.PortName
                End If
            Next
        Else
            Me._StatusLabel.Text = "no ports detected"
            Me._PortNamesCombo.Text = "NO PORTS"
            Me._ConnectButton.Text = "NO PORTS"
            Me._ConnectButton.Enabled = False
        End If
        ' Me._PortNamesCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.PortName)
        Me._BaudRateCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.BaudRate)
        Me._DataBitsCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.DataBits)
        Me._ParityCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.Parity)
        Me._StopBitsCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.StopBits)
        Me._ReceiveDelayCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.ReceiveDelay)
        Me._ReceiveThresholdCombo.Text = Ports.Serial.Port.DefaultPortParameters.Item(PortParameterKey.ReceivedBytesThreshold)

        Me._PortNamesCombo.ComboBox.Select(0, 0)
        Me._BaudRateCombo.ComboBox.Select(0, 0)
        Me._DataBitsCombo.ComboBox.Select(0, 0)
        Me._ParityCombo.ComboBox.Select(0, 0)
        Me._StopBitsCombo.ComboBox.Select(0, 0)
        Me._ReceiveDelayCombo.ComboBox.Select(0, 0)
        Me._ReceiveThresholdCombo.ComboBox.Select(0, 0)

        Me.onConnectionChanged(Me._port.IsConnected)

    End Sub

#End Region

#Region " TRANSMIT MANAGEMENT "

    Const delimiter As String = " "
    ''' <summary>
    ''' Handles the Click event of the _TransmitCopyMenuItem control.
    ''' Copies a transmit box selection.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _TransmitCopyMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitCopyMenuItem.Click
        Me._TransmitTextBox.Copy()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitPasteMenuItem control.
    ''' Pastes a transmit box selection.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _TransmitPasteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitPasteMenuItem.Click
        Me._TransmitTextBox.Paste()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitCutMenuItem control.
    ''' Cuts the transmit text box item.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _TransmitCutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitCutMenuItem.Click
        Me._TransmitTextBox.Cut()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _TransmitSendMenuItem control.
    ''' Sends position of caret line from transmit text box 
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _TransmitSendMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitSendMenuItem.Click
        Try
            Dim loc As Integer = Me._TransmitTextBox.GetFirstCharIndexOfCurrentLine
            Dim ln As Integer = Me._TransmitTextBox.GetLineFromCharIndex(loc)
            If ln > 0 Then
                SendData(Me._TransmitTextBox.Lines(ln))
                Me._TransmitMessageCombo.Text = ""
            End If
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@f_TransmitSendMenuItem_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Sends only selection in transmit box to com
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _TransmitSendSelectionMenuItemt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitSendSelectionMenuItem.Click
        Try
            If Me._TransmitTextBox.SelectionLength > 0 Then
                SendData(Me._TransmitTextBox.SelectedText)
                Me._TransmitMessageCombo.Text = ""
            End If
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_TransmitSendSelectionMenuItem_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' fetches a line from the transmit box to the transmit message box.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _TransmitTextBox_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _TransmitTextBox.MouseClick
        Try
            Dim rb As RichTextBox = CType(sender, RichTextBox)
            Dim loc As Integer = rb.GetFirstCharIndexOfCurrentLine
            Dim ln As Integer = rb.GetLineFromCharIndex(loc)
            Me._TransmitMessageCombo.Text = rb.Lines(ln)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_TransmitTextBox_MouseClick(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Clears the transmit box.
    ''' </summary>
    Public Sub ClearTransmitBox()
        Me._TransmitTextBox.Clear()
        Me._TransmitTextBox.Text = "1"
        Me.charsInline = Me._TransmitTextBox.SetRuler(Me.characterWidth, Me._TransmitShowHEXToolStripMenuItem.Checked)
        Me.TransmitCount = 0
        Me._TransmitStatusLabel.Image = If(Me.IsConnected, My.Resources.ledCornerOrange, My.Resources.ledCornerGray)
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ClearTransmitBoxButton control.
    ''' Clears the transmit box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearTransmitBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearTransmitBoxButton.Click
        Try
            Me.ClearTransmitBox()
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_ClearTransmitBoxButton_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Build hex string in the transmit box
    ''' </summary>
    Private Sub _TransmitMessageCombo_TextUpdate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitMessageCombo.TextUpdate

        If Me._hasNewHexCharacter AndAlso Me._TransmitEnterHEXToolStripMenuItem.Checked Then
            Dim cb As ToolStripComboBox = CType(sender, ToolStripComboBox)
            cb.Text = cb.Text.ToByteFormatted(delimiter)
            cb.SelectionStart = cb.Text.Length
        End If

    End Sub

    Private _hasNewHexCharacter As Boolean
    ''' <summary>
    ''' Handles the KeyPress event of the cboEnterMessage control.
    ''' Enter only allowed keys in hex mode
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs" /> instance containing the event data.</param>
    Private Sub _TransmitMessageCombo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _TransmitMessageCombo.KeyPress

        Me._hasNewHexCharacter = False
        If e.KeyChar <> vbCr Then
            If Me._TransmitEnterHEXToolStripMenuItem.Checked Then
                Me._hasNewHexCharacter = e.KeyChar.IsHexadecimal
                If Char.IsControl(e.KeyChar) Then
                ElseIf Char.IsLetterOrDigit(e.KeyChar) AndAlso Not e.KeyChar.IsHexadecimal Then
                    e.Handled = True
                ElseIf Char.IsPunctuation(e.KeyChar) Then
                    e.Handled = True
                End If
            End If
        Else
            SendData(Me._TransmitMessageCombo.Text)
            Me._TransmitMessageCombo.Text = ""
        End If

    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _TransmitShowHEXToolStripMenuItem control.
    ''' Toggles showing the transmit data in hex.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _TransmitShowHEXToolStripMenuItem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TransmitShowHEXToolStripMenuItem.CheckedChanged
        Try
            Me.charsInline = Me._TransmitTextBox.SetRuler(Me.characterWidth, Me._TransmitShowHEXToolStripMenuItem.Checked)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_TransmitShowHEXToolStripMenuItem_CheckedChanged)")
        End Try
    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether show transmit data in hex.
    ''' </summary>
    ''' <value><c>True</c> if [show transmit hex data]; otherwise, <c>False</c>.</value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ShowTransmitHexData As Boolean
        Get
            Return Me._TransmitShowHEXToolStripMenuItem.Checked
        End Get
        Set(value As Boolean)
            Me._TransmitShowHEXToolStripMenuItem.Checked = value
        End Set
    End Property


    Private _transmitCount As Integer
    ''' <summary>
    ''' Gets or sets the transmit count.
    ''' </summary>
    ''' <value>The transmit count.</value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property TransmitCount As Integer
        Get
            Return Me._transmitCount
        End Get
        Set(value As Integer)
            Me._transmitCount = value
            Me._transmitCountLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:D6}", Me.TransmitCount)
        End Set
    End Property

    ''' <summary>
    ''' Sends the data.
    ''' </summary>
    ''' <param name="textData">The text data.</param>
    Public Sub SendData(ByVal textData As String)

        Dim data() As Byte = Nothing
        If String.IsNullOrWhiteSpace(textData) Then
            Return
        End If

        If Me._TransmitEnterHEXToolStripMenuItem.Checked Then
            data = textData.RemoveDelimiter(delimiter).ToHexBytes
            If data(0) = 255 Then
                Me.SafeInvokeViewMessageAvailable(Diagnostics.TraceEventType.Warning,
                                                  PortConsole.BuildMessage("Conversion of '{0}' failed; the character {1} is not an a valid HEX character!",
                                                  textData, Chr(data(1))))
            End If
        Else
            If Me._AppendCRToolStripMenuItem.Checked Then textData &= ControlChars.Cr
            If Me._AppendLFToolStripMenuItem.Checked Then textData &= ControlChars.Lf
            data = Encoding.ASCII.GetBytes(textData)
        End If

        ' send data:
        Me._port.SendData(data)

#If False Then
        Me.TransmitCount += data.Length
        Me._TransmitStatusLabel.Image = My.Resources.ledCornerGray

        ' display in box:
        If Me._TransmitShowHEXToolStripMenuItem.Checked And Not Me._TransmitShowASCIIToolStripMenuItem.Checked Then
            Me._TransmitTextBox.AppendBytes(data, Me.charsInline, False)
        ElseIf Me._TransmitShowHEXToolStripMenuItem.Checked And Me._TransmitShowASCIIToolStripMenuItem.Checked Then
            Me._TransmitTextBox.AppendBytes(data, Me.charsInline, True)
        ElseIf Not Me._TransmitShowHEXToolStripMenuItem.Checked And Me._TransmitShowASCIIToolStripMenuItem.Checked Then
            Me._TransmitTextBox.ScrollToCaret()
            Me._TransmitTextBox.AppendText(textData & vbCr)
        End If
#End If
        ' Store the data in the transmit combo box.
        Me._TransmitMessageCombo.Items.Add(textData)

    End Sub

#End Region

#Region " TRANSMIT REPEAT "


    Private Sub _TransmitPlayButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _TransmitPlayButton.CheckedChanged
        If Me._TransmitPlayButton.Enabled AndAlso Me._TransmitPlayButton.Visible Then
            If Me._TransmitPlayButton.Checked Then
                Me._TransmitPlayButton.Text = "STOP"
            Else
                Me._TransmitPlayButton.Text = "PLAY"
            End If
        End If
    End Sub

#End Region

#Region " RECEIVE HANDLERS "

    Private _ReceiveCount As Integer
    ''' <summary>
    ''' Gets or sets the Receive count.
    ''' </summary>
    ''' <value>The Receive count.</value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ReceiveCount As Integer
        Get
            Return Me._ReceiveCount
        End Get
        Set(value As Integer)
            Me._ReceiveCount = value
            Me._receiveCountLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:D6}", Me.ReceiveCount)
        End Set
    End Property

    ''' <summary>
    ''' Handles the Click event of the _ClearReceiveBoxButton control.
    ''' clear receive box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearReceiveBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearReceiveBoxButton.Click
        Try
            Me._ReceiveTextBox.Clear()
            Me._ReceiveTextBox.Text = "1"
            Me.charsInline = Me._ReceiveTextBox.SetRuler(Me.characterWidth, Me._ReceiveShowHEXToolStripMenuItem.Checked)
            Me.ReceiveCount = 0
            Me._receiveStatusLabel.Image = If(Me.IsConnected, My.Resources.ledCornerOrange, My.Resources.ledCornerGray)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_ClearReceiveBoxButton_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the _ReceiveShowHEXToolStripMenuItem control.
    ''' View hex in receive box.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ReceiveShowHEXToolStripMenuItem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReceiveShowHEXToolStripMenuItem.CheckedChanged
        Try
            Me.charsInline = Me._ReceiveTextBox.SetRuler(Me.characterWidth, Me._ReceiveShowHEXToolStripMenuItem.Checked)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_ReceiveShowHEXToolStripMenuItem_CheckedChanged(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _SaveReceiveTextBoxButton control.
    ''' Save receive box to file
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _SaveReceiveTextBoxButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveReceiveTextBoxButton.Click
        Try
            Using saveFileDialog As New System.Windows.Forms.SaveFileDialog()
                saveFileDialog.DefaultExt = "*.TXT"
                saveFileDialog.Filter = "txt Files | *.TXT"
                If saveFileDialog.ShowDialog() = DialogResult.OK Then

                    Dim fullpath As String = saveFileDialog.FileName()
                    Me._ReceiveTextBox.SaveFile(fullpath, RichTextBoxStreamType.PlainText)
                    Me._StatusLabel.Text = IO.Path.GetFileName(fullpath) & " written"
                Else
                    Me._StatusLabel.Text = "no data chosen"
                End If
            End Using
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_SaveReceiveTextBoxButton_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _LoadTransmitFileButton control.
    ''' load file into transmit box
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _LoadTransmitFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadTransmitFileButton.Click
        Try
            Using openFileDialog As New System.Windows.Forms.OpenFileDialog()
                openFileDialog.DefaultExt = "*.TXT"
                openFileDialog.Filter = "txt Files | *.TXT"
                If openFileDialog.ShowDialog() = DialogResult.OK Then

                    Dim fullpath As String = openFileDialog.FileName()
                    Me._TransmitTextBox.Clear()
                    Me._TransmitTextBox.LoadFile(fullpath, RichTextBoxStreamType.PlainText)

                Else
                    Me._StatusLabel.Text = "no data chosen"
                End If
            End Using
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_LoadTransmitFileButton_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

#End Region

#Region " CONNECTION MANAGEMENT "

    Const disconnectTitle As String = "DISCONNECT"
    Const connectTitle As String = "      CONNECT"

    ''' <summary>
    ''' Handles the Click event of the _ConnectButton control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ConnectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConnectButton.Click
        Try
            onConnecting(Me.IsConnected)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(ex, "@_ConnectButton_Click(System.Object,System.EventArgs)")
        End Try
    End Sub

    ''' <summary>
    ''' Process connecting.
    ''' </summary>
    ''' <param name="isConnected">if set to <c>True</c> [is connected].</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub onConnecting(ByVal isConnected As Boolean)
        Me._errorProvider.Clear()
        Try
            If isConnected Then
                If Me.Port IsNot Nothing Then
                    Me.Port.Disconnect()
                End If
            Else
                If Me.Port IsNot Nothing Then
                    Dim p As PortParametersDictionary = Me.Port.ToPortParameters
                    p.Replace(PortParameterKey.PortName, Me._PortNamesCombo.Text)
                    p.Replace(PortParameterKey.BaudRate, Me._BaudRateCombo.Text)
                    p.Replace(PortParameterKey.DataBits, Me._DataBitsCombo.Text)
                    p.Replace(PortParameterKey.Parity, Me._ParityCombo.Text)
                    p.Replace(PortParameterKey.StopBits, Me._StopBitsCombo.Text)
                    p.Replace(PortParameterKey.ReceiveDelay, Me._ReceiveDelayCombo.Text)
                    p.Replace(PortParameterKey.ReceivedBytesThreshold, Me._ReceiveThresholdCombo.Text)
                    Me.Port.Connect(p)
                End If
            End If
        Catch ex As Exception
            Me._StatusLabel.Text = ex.Message
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(TraceEventType.Error,
                                                   BuildMessage(ex, "@onConnecting(Boolean)")))
        End Try
    End Sub

    ''' <summary>
    ''' Handles the change of connection.
    ''' </summary>
    ''' <param name="isConnected">if set to <c>True</c> [is connected].</param>
    Private Sub onConnectionChanged(ByVal isConnected As Boolean)
        Me._errorProvider.Clear()
        If isConnected Then
            If Me.Port IsNot Nothing Then
                Me._PortNamesCombo.Text = Me.Port.SerialPort.PortName
                Me._BaudRateCombo.Text = CStr(Me.Port.SerialPort.BaudRate)
                Me._DataBitsCombo.Text = CStr(Me.Port.SerialPort.DataBits)
                Me._ParityCombo.Text = Me.Port.SerialPort.Parity.ToString
                Me._StopBitsCombo.Text = Me.Port.SerialPort.StopBits
                Me._ReceiveDelayCombo.Text = Me.Port.ReceiveDelay
                Me._ReceiveThresholdCombo.Text = Me.Port.SerialPort.ReceivedBytesThreshold
            End If

            Me._ParityCombo.Enabled = False
            Me._StopBitsCombo.Enabled = False
            Me._PortNamesCombo.Enabled = False
            Me._BaudRateCombo.Enabled = False
            Me._DataBitsCombo.Enabled = False
            Me._ReceiveDelayCombo.Enabled = False
            Me._ReceiveThresholdCombo.Enabled = False
            Me._ConnectButton.Text = disconnectTitle
            Me._ConnectStatusLabel.Image = My.Resources.ledCornerGreen
            Me._receiveStatusLabel.Image = My.Resources.ledCornerOrange
            Me._TransmitStatusLabel.Image = My.Resources.ledCornerOrange
            If Me.Port IsNot Nothing Then
                Me.ShowPortParameters(Me.Port.ToPortParameters)
            End If
        Else
            Me._ParityCombo.Enabled = True
            Me._StopBitsCombo.Enabled = True
            Me._PortNamesCombo.Enabled = True
            Me._BaudRateCombo.Enabled = True
            Me._DataBitsCombo.Enabled = True
            Me._ReceiveDelayCombo.Enabled = True
            Me._ReceiveThresholdCombo.Enabled = True
            Me._ConnectButton.Text = connectTitle
            Me._ConnectStatusLabel.Image = My.Resources.ledCornerRed
            Me._receiveStatusLabel.Image = My.Resources.ledCornerGray
            Me._TransmitStatusLabel.Image = My.Resources.ledCornerGray
            Me._StatusLabel.Text = "Disconnected"
        End If
    End Sub

#End Region

#Region " CONFIGURATION FILE MANAGEMENT "

    Dim configFileName As String
    ''' <summary>
    ''' load config
    ''' </summary>
    Private Sub LoadConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LoadConfigMenuItem.Click
        Try
            Dim p As New PortParametersDictionary
            p.Restore(configFileName)
            Me._PortNamesCombo.Text = p.Item(PortParameterKey.PortName)
            Me._BaudRateCombo.Text = p.Item(PortParameterKey.BaudRate)
            Me._DataBitsCombo.Text = p.Item(PortParameterKey.DataBits)
            Me._ParityCombo.Text = p.Item(PortParameterKey.Parity)
            Me._StopBitsCombo.Text = p.Item(PortParameterKey.StopBits)
            Me._ReceiveThresholdCombo.Text = p.Item(PortParameterKey.ReceivedBytesThreshold)
            Me._ReceiveDelayCombo.Text = p.Item(PortParameterKey.ReceiveDelay)
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(Diagnostics.TraceEventType.Information,
                                                   "Configuration restored from {0}.", configFileName))
        Catch ex As IOException
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(Diagnostics.TraceEventType.Error,
                                                   "Exception occurred loading configuration from {0}. Details: {1}", configFileName, ex))
        End Try

    End Sub

    ''' <summary>
    ''' save port parameters.
    ''' </summary>
    Private Sub _SaveConfigMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveConfigMenuItem.Click
        Try
            Me._port.ToPortParameters.Store(configFileName)
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(Diagnostics.TraceEventType.Information,
                                                   "Configuration save to {0}.", configFileName))
        Catch ex As IOException
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(Diagnostics.TraceEventType.Error,
                                                   "Exception occurred saving configuration to {0}. Details: {1}", configFileName, ex))
        End Try
    End Sub

#End Region

#Region " COM PORT MANAGEMENT "

    Private WithEvents _port As IPort
    ''' <summary>
    ''' Gets or sets the port.
    ''' </summary>
    ''' <value>The port.</value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Port As IPort
        Get
            Return Me._port
        End Get
        Set(value As IPort)
            Me._port = value
            Me.onConnectionChanged(Me.IsConnected)
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property IsConnected As Boolean
        Get
            If Me._port Is Nothing Then
                Return False
            Else
                Return Me._port.IsConnected
            End If
        End Get
    End Property

    ''' <summary>
    ''' Handles the DataReceived event of the _serialPort control.
    ''' Updates the data boxes and the received status.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="PortEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _Port_DataReceived(sender As Object, e As PortEventArgs) Handles _port.DataReceived
        Try
            If e.StatusOkay Then
                Me._receiveStatusLabel.Image = My.Resources.ledCornerGreen

                If e.DataBuffer IsNot Nothing Then
                    Me.ReceiveCount += e.DataBuffer.Length

                    If Me._ReceiveShowHEXToolStripMenuItem.Checked And Not Me._ReceiveShowASCIIToolStripMenuItem.Checked Then
                        Me._ReceiveTextBox.AppendBytes(e.DataBuffer, Me.charsInline, False)

                    ElseIf Me._ReceiveShowHEXToolStripMenuItem.Checked And Me._ReceiveShowASCIIToolStripMenuItem.Checked Then
                        Me._ReceiveTextBox.AppendBytes(e.DataBuffer, Me.charsInline, True)

                    ElseIf Not Me._ReceiveShowHEXToolStripMenuItem.Checked And Me._ReceiveShowASCIIToolStripMenuItem.Checked Then
                        Dim s As String = Encoding.ASCII.GetString(e.DataBuffer, 0, e.DataBuffer.Length)
                        Me._ReceiveTextBox.ScrollToCaret()
                        Me._ReceiveTextBox.AppendText(s)
                        Me._ReceiveTextBox.AppendText(Environment.NewLine)
                    End If
                End If
            Else
                Me._receiveStatusLabel.Image = My.Resources.ledCornerRed
            End If
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(TraceEventType.Error,
                                                   BuildMessage(ex, "@_serialPort_DataReceived(System.Object,System.EventArgs)")))
        End Try

    End Sub

    ''' <summary>
    ''' Handles the DataSent event of the _serialPort control.
    ''' Updates controls and data.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="PortEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _Port_DataSent(sender As Object, e As PortEventArgs) Handles _port.DataSent
        Try
            If e.StatusOkay Then
                Me._TransmitStatusLabel.Image = My.Resources.ledCornerGreen

                Me.TransmitCount += e.DataBuffer.Length
                Me._TransmitStatusLabel.Image = My.Resources.ledCornerGray

                ' display in box:
                If Me._TransmitShowHEXToolStripMenuItem.Checked And Not Me._TransmitShowASCIIToolStripMenuItem.Checked Then
                    Me._TransmitTextBox.AppendBytes(e.DataBuffer, Me.charsInline, False)
                ElseIf Me._TransmitShowHEXToolStripMenuItem.Checked And Me._TransmitShowASCIIToolStripMenuItem.Checked Then
                    Me._TransmitTextBox.AppendBytes(e.DataBuffer, Me.charsInline, True)
                ElseIf Not Me._TransmitShowHEXToolStripMenuItem.Checked And Me._TransmitShowASCIIToolStripMenuItem.Checked Then
                    Me._TransmitTextBox.ScrollToCaret()
                    Dim textdata As String = Encoding.ASCII.GetString(e.DataBuffer)
                    Me._TransmitTextBox.AppendText(textdata & vbCr)
                End If

            Else
                Me._TransmitStatusLabel.Image = My.Resources.ledCornerRed
            End If
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(TraceEventType.Error,
                                                   BuildMessage(ex, "@_serialPort_DataSent(System.Object,System.EventArgs)")))
        End Try
    End Sub

    ''' <summary>
    ''' Handles the ConnectionChanged event of the _serialPort control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="ConnectionEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _Port_ConnectionChanged(sender As Object, e As ConnectionEventArgs) Handles _port.ConnectionChanged
        Try
            Me.onConnectionChanged(e.IsConnected)
        Catch ex As Exception
            Me.SafeInvokeViewMessageAvailable(
                              New MessageEventArgs(TraceEventType.Error,
                                                   BuildMessage(ex, "@_serialPort_ConnectionChanged(System.Object,System.EventArgs)")))
        End Try
    End Sub

    ''' <summary>
    ''' Handles the MessageAvailable event of the _serialPort control.
    ''' Displays an error message box.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    Private Sub _Port_MessageAvailable(sender As Object, e As MessageEventArgs) Handles _port.MessageAvailable
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeMessageAvailable(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeMessageAvailable), e)
        End If
    End Sub

    Private Sub _port_SerialErrorReceived(sender As Object, e As System.IO.Ports.SerialErrorReceivedEventArgs) Handles _port.SerialPortErrorReceived
    End Sub

    Private Sub _port_Timeout(sender As Object, e As System.EventArgs) Handles _port.Timeout
    End Sub

#End Region

#Region " DISPLAY MANAGEMENT "

    ''' <summary>
    ''' Average character width.
    ''' </summary>
    Private characterWidth As Single

    ''' <summary>
    ''' Number of characters per line.
    ''' </summary>
    Private charsInline As Integer

    ''' <summary>
    ''' select a font
    ''' </summary>
    Private Sub fontMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LargeFontMenuItem.Click, _MediumFontMenuItem.Click, _SmallFontMenuItem.Click

        Dim s As String = CType(sender, ToolStripMenuItem).Text

        If s = "Large" Then
            Me._ReceiveTextBox.Font = New Font("Lucida Console", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Medium" Then
            Me._ReceiveTextBox.Font = New Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ElseIf s = "Small" Then
            Me._ReceiveTextBox.Font = New Font("Lucida Console", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        End If

        Dim g As Graphics = Me._TransmitTextBox.CreateGraphics
        ' measure with a test string
        Dim szF As SizeF = g.MeasureString("0123456789", Me._ReceiveTextBox.Font)
        Me.characterWidth = 0.1F * szF.Width
        g.Dispose()

        Me._TransmitTextBox.Font = Me._ReceiveTextBox.Font

    End Sub

    ''' <summary>
    ''' Shows the Messenger parameters.
    ''' </summary>
    Private Sub ShowPortParameters(ByVal portParameters As PortParametersDictionary)
        Dim s As New System.Text.StringBuilder
        s.Append("Connected using ")
        s.Append(portParameters.Item(PortParameterKey.PortName))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.BaudRate))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.DataBits))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.StopBits))
        s.Append(";")
        s.Append(portParameters.Item(PortParameterKey.Parity))
        Me._StatusLabel.Text = s.ToString
    End Sub

#End Region

#Region " EVENT MANAGEMENT "

    ''' <summary>
    ''' Propagates a sync context.
    ''' </summary>
    Private _SyncContext As SynchronizationContext

    Private WithEvents _errorProvider As ErrorProvider

    ''' <summary>
    ''' Enunciates the specified control.
    ''' </summary>
    ''' <param name="control">The control.</param>
    ''' <param name="errorIconAlignment">The error icon alignment.</param>
    ''' <param name="padding">The padding.</param>
    ''' <param name="e">The <see cref="isr.Ports.Serial.MessageEventArgs" /> instance containing the event data.</param>
    Private Sub Enunciate(ByVal control As Control, ByVal errorIconAlignment As ErrorIconAlignment, ByVal padding As Integer, ByVal e As MessageEventArgs)
        If e.Severity <= TraceEventType.Warning Then
            Me._errorProvider.SetIconAlignment(control, errorIconAlignment)
            Me._errorProvider.SetIconPadding(control, padding)
            Me._errorProvider.SetError(control, e.Details)
        Else
            Me._errorProvider.Clear()
        End If
    End Sub

#Region " VIEW MESSAGE AVAILABLE "

    ''' <summary>
    ''' Occurs when View Message is Available.
    ''' </summary>
    Public Event ViewMessageAvailable As EventHandler(Of MessageEventArgs)

    ''' <summary>
    ''' Invokes the <see cref="ViewMessageAvailable">View Message Available event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Sub UnsafeInvokeViewMessageAvailable(ByVal e As MessageEventArgs)
        Try
            Me.Enunciate(Me._ToolStrip, ErrorIconAlignment.MiddleRight, -20, e)
            Dim evt As EventHandler(Of MessageEventArgs) = Me.ViewMessageAvailableEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Invokes the <see cref="ViewMessageAvailable">Message Available event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="obj"> The object. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Sub UnsafeInvokeViewMessageAvailable(ByVal obj As Object)
        Me.UnsafeInvokeViewMessageAvailable(CType(obj, MessageEventArgs))
    End Sub

    ''' <summary>
    ''' Safely invokes the view message available event.
    ''' </summary>
    ''' <param name="ex">The ex.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Sub UnsafeInvokeViewMessageAvailable(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.SafeInvokeViewMessageAvailable(
                          New MessageEventArgs(TraceEventType.Error, BuildMessage(ex, format, args)))
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="ViewMessageAvailable">View Message Available event</see>.
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeViewMessageAvailable(ByVal e As MessageEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeMessageAvailable(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeMessageAvailable), e)
        End If
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="ViewMessageAvailable">View Message Available event</see>.
    ''' </summary>
    ''' <param name="ex">The ex.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Sub SafeInvokeViewMessageAvailable(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        SafeInvokeViewMessageAvailable(New MessageEventArgs(TraceEventType.Error, BuildMessage(ex, format, args)))
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="ViewMessageAvailable">View Message Available event</see>.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Private Sub SafeInvokeViewMessageAvailable(ByVal severity As Diagnostics.TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        SafeInvokeViewMessageAvailable(New MessageEventArgs(severity, BuildMessage(format, args)))
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="ViewMessageAvailable">View Message Available event</see>.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Private Sub SafeInvokeViewMessageAvailable(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        SafeInvokeViewMessageAvailable(New MessageEventArgs(severity, BuildMessage(details)))
    End Sub

    ''' <summary>
    ''' Builds the message.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shared Function BuildMessage(ByVal format As String, ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}",
                             String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Build an exception message.
    ''' </summary>
    ''' <param name="ex">The exception.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shared Function BuildMessage(ByVal ex As Exception, ByVal format As String, ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}Details: {2}",
                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args), Environment.NewLine, ex)
    End Function

#End Region

#Region " MESSAGE AVAILABLE "

    ''' <summary>
    ''' Occurs when Message is Available.
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of MessageEventArgs)

    ''' <summary>
    ''' Invokes the <see cref="MessageAvailable">Message Available event</see>.
    ''' Must be called with the <see cref="SynchronizationContext">sync context</see>
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Sub UnsafeInvokeMessageAvailable(ByVal e As MessageEventArgs)
        Try
            Me.Enunciate(Me._ToolStrip, ErrorIconAlignment.MiddleRight, -20, e)
            Dim evt As EventHandler(Of MessageEventArgs) = Me.MessageAvailableEvent
            If evt IsNot Nothing Then evt.Invoke(Me, e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.Message)
        End Try
    End Sub

    ''' <summary> Invokes the <see cref="MessageAvailable">Message Available event</see>. Must be called with
    ''' the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Sub UnsafeInvokeMessageAvailable(ByVal obj As Object)
        Me.UnsafeInvokeMessageAvailable(CType(obj, MessageEventArgs))
    End Sub

    ''' <summary>
    ''' Safely invokes the <see cref="MessageAvailable">message available event</see>.
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal e As MessageEventArgs)
        If Me._SyncContext Is Nothing Then
            UnsafeInvokeMessageAvailable(e)
        Else
            Me._SyncContext.Post(New SendOrPostCallback(AddressOf UnsafeInvokeMessageAvailable), e)
        End If
    End Sub

    ''' <summary>
    ''' Safely invokes the message available event.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Private Sub SafeInvokeMessageAvailable(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        SafeInvokeMessageAvailable(New MessageEventArgs(severity, BuildMessage(details)))
    End Sub

#End Region

#End Region

End Class
