﻿Imports System.Windows.Forms

''' <summary>
''' Defines an event arguments class for <see cref="Port">port messages</see>.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class MessageEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal details As String)
        Me.new(Diagnostics.TraceEventType.Information, details)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal details As String)
        MyBase.new()
        Me._severity = severity
        If String.IsNullOrWhiteSpace(details) Then
            details = ""
        End If
        Me._details = details
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The arguments for the format statement.</param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        Me.new(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

#End Region

    Private _severity As Diagnostics.TraceEventType
    ''' <summary>
    ''' Gets the message severity.
    ''' </summary>
    Public ReadOnly Property Severity As Diagnostics.TraceEventType
        Get
            Return Me._severity
        End Get
    End Property

    Private _details As String
    ''' <summary>
    ''' Gets the message details
    ''' </summary>
    Public ReadOnly Property Details As String
        Get
            Return Me._details
        End Get
    End Property

    ''' <summary>
    ''' Displays the message.
    ''' </summary>
    ''' <param name="e">The <see cref="MessageEventArgs" /> instance containing the event data.</param>
    Public Shared Sub DisplayMessage(e As MessageEventArgs)
        If e Is Nothing Then Return
        Dim icon As MessageBoxIcon = MessageBoxIcon.Information
        Select Case e.Severity
            Case TraceEventType.Critical, TraceEventType.Error
                icon = MessageBoxIcon.Error
            Case TraceEventType.Information
                icon = MessageBoxIcon.Information
            Case TraceEventType.Verbose
                icon = MessageBoxIcon.Information
            Case TraceEventType.Warning
                icon = MessageBoxIcon.Exclamation
            Case Else
                icon = MessageBoxIcon.Information
        End Select
        MessageBox.Show(e.Details, "Messenger Message", MessageBoxButtons.OK, icon,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    End Sub

End Class
