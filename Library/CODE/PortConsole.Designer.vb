﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class PortConsole
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                disposeManagedResources()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PortConsole))
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._PortNumberComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PortNamesCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._BaudRateComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BaudRateCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._DataBitsComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._DataBitsCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ParityComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ParityCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._StopBitsComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StopBitsCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ReceiveDelayComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReceiveDelayCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ReceiveThresholdComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReceiveThresholdCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._ConnectButton = New System.Windows.Forms.ToolStripButton()
        Me._ConnectStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._MainMenu = New System.Windows.Forms.MenuStrip()
        Me._FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LoadConfigMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveConfigMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OptionsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReceiveBoxFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LargeFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MediumFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SmallFontMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FontDialog = New System.Windows.Forms.FontDialog()
        Me._SplitContainer = New System.Windows.Forms.SplitContainer()
        Me._ReceiveTextBox = New System.Windows.Forms.RichTextBox()
        Me._ReceiveToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReceiveTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator2 = New System.Windows.Forms.ToolStripSeparator()
        Me._ClearReceiveBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator = New System.Windows.Forms.ToolStripSeparator()
        Me._SaveReceiveTextBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._Separator7 = New System.Windows.Forms.ToolStripSeparator()
        Me._receiveCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator8 = New System.Windows.Forms.ToolStripSeparator()
        Me._receiveStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator9 = New System.Windows.Forms.ToolStripSeparator()
        Me._ReceiveDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._ReceiveShowHEXToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReceiveShowASCIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitTextBox = New System.Windows.Forms.RichTextBox()
        Me._TransmitContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._TransmitCopyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitPasteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitCutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitSendMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitSendSelectionMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SendDataToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TransmitMessageCombo = New System.Windows.Forms.ToolStripComboBox()
        Me._TransmitToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TransmitTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TransmitSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._ClearTransmitBoxButton = New System.Windows.Forms.ToolStripButton()
        Me.__TransmitSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me._LoadTransmitFileButton = New System.Windows.Forms.ToolStripButton()
        Me._TransmitSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me._transmitCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TransmitSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me._TransmitStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TransmitSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me._TransmitToolStripDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._TransmitEnterHEXToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitShowHEXToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitShowASCIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me._TransmitDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._AppendLFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AppendCRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TransmitSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me._TransmitRepeatCountTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._TransmitDelayMillisecondsTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._TransmitPlayButton = New System.Windows.Forms.ToolStripButton()
        Me._TransmitSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStrip.SuspendLayout()
        Me._StatusStrip.SuspendLayout()
        Me._MainMenu.SuspendLayout()
        CType(Me._SplitContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._SplitContainer.Panel1.SuspendLayout()
        Me._SplitContainer.Panel2.SuspendLayout()
        Me._SplitContainer.SuspendLayout()
        Me._ReceiveToolStrip.SuspendLayout()
        Me._TransmitContextMenu.SuspendLayout()
        Me._SendDataToolStrip.SuspendLayout()
        Me._TransmitToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PortNumberComboLabel, Me._PortNamesCombo, Me._BaudRateComboLabel, Me._BaudRateCombo, Me._DataBitsComboLabel, Me._DataBitsCombo, Me._ParityComboLabel, Me._ParityCombo, Me._StopBitsComboLabel, Me._StopBitsCombo, Me._ReceiveDelayComboLabel, Me._ReceiveDelayCombo, Me._ReceiveThresholdComboLabel, Me._ReceiveThresholdCombo, Me._ConnectButton, Me._ConnectStatusLabel})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 24)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(762, 25)
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_PortNumberComboLabel
        '
        Me._PortNumberComboLabel.Name = "_PortNumberComboLabel"
        Me._PortNumberComboLabel.Size = New System.Drawing.Size(32, 22)
        Me._PortNumberComboLabel.Text = " Port"
        Me._PortNumberComboLabel.ToolTipText = "Port Number"
        '
        '_PortNamesCombo
        '
        Me._PortNamesCombo.AutoSize = False
        Me._PortNamesCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PortNamesCombo.MergeAction = System.Windows.Forms.MergeAction.MatchOnly
        Me._PortNamesCombo.Name = "_PortNamesCombo"
        Me._PortNamesCombo.Size = New System.Drawing.Size(60, 23)
        Me._PortNamesCombo.ToolTipText = "Port number"
        '
        '_BaudRateComboLabel
        '
        Me._BaudRateComboLabel.Name = "_BaudRateComboLabel"
        Me._BaudRateComboLabel.Size = New System.Drawing.Size(37, 22)
        Me._BaudRateComboLabel.Text = " Baud"
        Me._BaudRateComboLabel.ToolTipText = "Baud Rate"
        '
        '_BaudRateCombo
        '
        Me._BaudRateCombo.DropDownWidth = 50
        Me._BaudRateCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BaudRateCombo.Items.AddRange(New Object() {"2400", "4800", "9600", "19200", "38400", "115200"})
        Me._BaudRateCombo.Name = "_BaudRateCombo"
        Me._BaudRateCombo.Size = New System.Drawing.Size(75, 25)
        Me._BaudRateCombo.Text = "9600"
        Me._BaudRateCombo.ToolTipText = "Baud Rate"
        '
        '_DataBitsComboLabel
        '
        Me._DataBitsComboLabel.AutoToolTip = True
        Me._DataBitsComboLabel.Name = "_DataBitsComboLabel"
        Me._DataBitsComboLabel.Size = New System.Drawing.Size(34, 22)
        Me._DataBitsComboLabel.Text = " Data"
        Me._DataBitsComboLabel.ToolTipText = "Data bits"
        '
        '_DataBitsCombo
        '
        Me._DataBitsCombo.AutoSize = False
        Me._DataBitsCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DataBitsCombo.Items.AddRange(New Object() {"7", "8"})
        Me._DataBitsCombo.Name = "_DataBitsCombo"
        Me._DataBitsCombo.Size = New System.Drawing.Size(30, 23)
        Me._DataBitsCombo.Text = "8"
        Me._DataBitsCombo.ToolTipText = "Data bits"
        '
        '_ParityComboLabel
        '
        Me._ParityComboLabel.Name = "_ParityComboLabel"
        Me._ParityComboLabel.Size = New System.Drawing.Size(40, 22)
        Me._ParityComboLabel.Text = " Parity"
        Me._ParityComboLabel.ToolTipText = "Parity"
        '
        '_ParityCombo
        '
        Me._ParityCombo.AutoSize = False
        Me._ParityCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ParityCombo.Items.AddRange(New Object() {"None", "Even", "Mark", "Odd", "Space"})
        Me._ParityCombo.Name = "_ParityCombo"
        Me._ParityCombo.Size = New System.Drawing.Size(50, 23)
        Me._ParityCombo.Text = "None"
        Me._ParityCombo.ToolTipText = "Parity"
        '
        '_StopBitsComboLabel
        '
        Me._StopBitsComboLabel.Name = "_StopBitsComboLabel"
        Me._StopBitsComboLabel.Size = New System.Drawing.Size(34, 22)
        Me._StopBitsComboLabel.Text = " Stop"
        Me._StopBitsComboLabel.ToolTipText = "Stop bits"
        '
        '_StopBitsCombo
        '
        Me._StopBitsCombo.AutoSize = False
        Me._StopBitsCombo.DropDownWidth = 50
        Me._StopBitsCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StopBitsCombo.Items.AddRange(New Object() {"None", "One", "Two"})
        Me._StopBitsCombo.Name = "_StopBitsCombo"
        Me._StopBitsCombo.Size = New System.Drawing.Size(50, 23)
        Me._StopBitsCombo.Text = "One"
        Me._StopBitsCombo.ToolTipText = "Stop bits"
        '
        '_ReceiveDelayComboLabel
        '
        Me._ReceiveDelayComboLabel.Name = "_ReceiveDelayComboLabel"
        Me._ReceiveDelayComboLabel.Size = New System.Drawing.Size(36, 22)
        Me._ReceiveDelayComboLabel.Text = "Delay"
        Me._ReceiveDelayComboLabel.ToolTipText = "Delay in ms"
        '
        '_ReceiveDelayCombo
        '
        Me._ReceiveDelayCombo.AutoSize = False
        Me._ReceiveDelayCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveDelayCombo.Items.AddRange(New Object() {"1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"})
        Me._ReceiveDelayCombo.Name = "_ReceiveDelayCombo"
        Me._ReceiveDelayCombo.Size = New System.Drawing.Size(45, 23)
        Me._ReceiveDelayCombo.Text = "1"
        Me._ReceiveDelayCombo.ToolTipText = "Data received handle delay in ms"
        '
        '_ReceiveThresholdComboLabel
        '
        Me._ReceiveThresholdComboLabel.Name = "_ReceiveThresholdComboLabel"
        Me._ReceiveThresholdComboLabel.Size = New System.Drawing.Size(30, 22)
        Me._ReceiveThresholdComboLabel.Text = "Lim."
        '
        '_ReceiveThresholdCombo
        '
        Me._ReceiveThresholdCombo.AutoSize = False
        Me._ReceiveThresholdCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me._ReceiveThresholdCombo.Items.AddRange(New Object() {"1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"})
        Me._ReceiveThresholdCombo.Name = "_ReceiveThresholdCombo"
        Me._ReceiveThresholdCombo.Size = New System.Drawing.Size(45, 23)
        Me._ReceiveThresholdCombo.Text = "1"
        Me._ReceiveThresholdCombo.ToolTipText = "received bytes threshold property"
        '
        '_ConnectButton
        '
        Me._ConnectButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me._ConnectButton.CheckOnClick = True
        Me._ConnectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ConnectButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ConnectButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectButton.Name = "_ConnectButton"
        Me._ConnectButton.Size = New System.Drawing.Size(75, 22)
        Me._ConnectButton.Text = "*CONNECT*"
        Me._ConnectButton.ToolTipText = "Connect/Disconnect"
        '
        '_ConnectStatusLabel
        '
        Me._ConnectStatusLabel.Image = Global.isr.Ports.Serial.My.Resources.Resources.ledCornerGray
        Me._ConnectStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._ConnectStatusLabel.Name = "_ConnectStatusLabel"
        Me._ConnectStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._ConnectStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._ConnectStatusLabel.ToolTipText = "connection state"
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 576)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(762, 22)
        Me._StatusStrip.TabIndex = 1
        Me._StatusStrip.Text = "Status Strip"
        '
        '_StatusLabel
        '
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(16, 17)
        Me._StatusLabel.Text = "..."
        '
        '_MainMenu
        '
        Me._MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileMenuItem, Me._OptionsMenuItem})
        Me._MainMenu.Location = New System.Drawing.Point(0, 0)
        Me._MainMenu.Name = "_MainMenu"
        Me._MainMenu.Size = New System.Drawing.Size(762, 24)
        Me._MainMenu.TabIndex = 2
        Me._MainMenu.Text = "Main Menu"
        '
        '_FileMenuItem
        '
        Me._FileMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LoadConfigMenuItem, Me._SaveConfigMenuItem})
        Me._FileMenuItem.Name = "_FileMenuItem"
        Me._FileMenuItem.Size = New System.Drawing.Size(37, 20)
        Me._FileMenuItem.Text = "File"
        '
        '_LoadConfigMenuItem
        '
        Me._LoadConfigMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Disk
        Me._LoadConfigMenuItem.Name = "_LoadConfigMenuItem"
        Me._LoadConfigMenuItem.Size = New System.Drawing.Size(139, 22)
        Me._LoadConfigMenuItem.Text = "Load Config"
        '
        '_SaveConfigMenuItem
        '
        Me._SaveConfigMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Disk_download
        Me._SaveConfigMenuItem.Name = "_SaveConfigMenuItem"
        Me._SaveConfigMenuItem.Size = New System.Drawing.Size(139, 22)
        Me._SaveConfigMenuItem.Text = "Save Config"
        '
        '_OptionsMenuItem
        '
        Me._OptionsMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReceiveBoxFontMenuItem})
        Me._OptionsMenuItem.Name = "_OptionsMenuItem"
        Me._OptionsMenuItem.Size = New System.Drawing.Size(61, 20)
        Me._OptionsMenuItem.Text = "Options"
        '
        '_ReceiveBoxFontMenuItem
        '
        Me._ReceiveBoxFontMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LargeFontMenuItem, Me._MediumFontMenuItem, Me._SmallFontMenuItem})
        Me._ReceiveBoxFontMenuItem.Name = "_ReceiveBoxFontMenuItem"
        Me._ReceiveBoxFontMenuItem.Size = New System.Drawing.Size(98, 22)
        Me._ReceiveBoxFontMenuItem.Text = "Font"
        '
        '_LargeFontMenuItem
        '
        Me._LargeFontMenuItem.Name = "_LargeFontMenuItem"
        Me._LargeFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._LargeFontMenuItem.Text = "Large"
        '
        '_MediumFontMenuItem
        '
        Me._MediumFontMenuItem.Name = "_MediumFontMenuItem"
        Me._MediumFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._MediumFontMenuItem.Text = "Medium"
        '
        '_SmallFontMenuItem
        '
        Me._SmallFontMenuItem.Name = "_SmallFontMenuItem"
        Me._SmallFontMenuItem.Size = New System.Drawing.Size(119, 22)
        Me._SmallFontMenuItem.Text = "Small"
        '
        '_SplitContainer
        '
        Me._SplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SplitContainer.Location = New System.Drawing.Point(0, 49)
        Me._SplitContainer.Name = "_SplitContainer"
        Me._SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '_SplitContainer.Panel1
        '
        Me._SplitContainer.Panel1.Controls.Add(Me._ReceiveTextBox)
        Me._SplitContainer.Panel1.Controls.Add(Me._ReceiveToolStrip)
        '
        '_SplitContainer.Panel2
        '
        Me._SplitContainer.Panel2.Controls.Add(Me._TransmitTextBox)
        Me._SplitContainer.Panel2.Controls.Add(Me._SendDataToolStrip)
        Me._SplitContainer.Panel2.Controls.Add(Me._TransmitToolStrip)
        Me._SplitContainer.Size = New System.Drawing.Size(762, 527)
        Me._SplitContainer.SplitterDistance = 327
        Me._SplitContainer.TabIndex = 3
        '
        '_ReceiveTextBox
        '
        Me._ReceiveTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me._ReceiveTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ReceiveTextBox.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveTextBox.Location = New System.Drawing.Point(0, 25)
        Me._ReceiveTextBox.Name = "_ReceiveTextBox"
        Me._ReceiveTextBox.ReadOnly = True
        Me._ReceiveTextBox.Size = New System.Drawing.Size(760, 300)
        Me._ReceiveTextBox.TabIndex = 3
        Me._ReceiveTextBox.Text = "*"
        Me._ReceiveTextBox.WordWrap = False
        '
        '_ReceiveToolStrip
        '
        Me._ReceiveToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReceiveTextBoxLabel, Me._Separator2, Me._ClearReceiveBoxButton, Me._Separator, Me._SaveReceiveTextBoxButton, Me._Separator7, Me._receiveCountLabel, Me._Separator8, Me._receiveStatusLabel, Me._Separator9, Me._ReceiveDropDownButton})
        Me._ReceiveToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ReceiveToolStrip.Name = "_ReceiveToolStrip"
        Me._ReceiveToolStrip.Size = New System.Drawing.Size(760, 25)
        Me._ReceiveToolStrip.TabIndex = 0
        Me._ReceiveToolStrip.Text = "ToolStrip2"
        '
        '_ReceiveTextBoxLabel
        '
        Me._ReceiveTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveTextBoxLabel.Name = "_ReceiveTextBoxLabel"
        Me._ReceiveTextBoxLabel.Size = New System.Drawing.Size(51, 22)
        Me._ReceiveTextBoxLabel.Text = "RECEIVE"
        '
        '_Separator2
        '
        Me._Separator2.Name = "_Separator2"
        Me._Separator2.Size = New System.Drawing.Size(6, 25)
        '
        '_ClearReceiveBoxButton
        '
        Me._ClearReceiveBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearReceiveBoxButton.Image = Global.isr.Ports.Serial.My.Resources.Resources.Doc_Del
        Me._ClearReceiveBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearReceiveBoxButton.Name = "_ClearReceiveBoxButton"
        Me._ClearReceiveBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._ClearReceiveBoxButton.ToolTipText = "clear receive box"
        '
        '_Separator
        '
        Me._Separator.Name = "_Separator"
        Me._Separator.Size = New System.Drawing.Size(6, 25)
        '
        '_SaveReceiveTextBoxButton
        '
        Me._SaveReceiveTextBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SaveReceiveTextBoxButton.Image = Global.isr.Ports.Serial.My.Resources.Resources.Disk_download
        Me._SaveReceiveTextBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SaveReceiveTextBoxButton.Name = "_SaveReceiveTextBoxButton"
        Me._SaveReceiveTextBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._SaveReceiveTextBoxButton.ToolTipText = "save received data to text file"
        '
        '_Separator7
        '
        Me._Separator7.Name = "_Separator7"
        Me._Separator7.Size = New System.Drawing.Size(6, 25)
        '
        '_receiveCountLabel
        '
        Me._receiveCountLabel.Name = "_receiveCountLabel"
        Me._receiveCountLabel.Size = New System.Drawing.Size(37, 22)
        Me._receiveCountLabel.Text = "00000"
        Me._receiveCountLabel.ToolTipText = "number of bytes received"
        '
        '_Separator8
        '
        Me._Separator8.Name = "_Separator8"
        Me._Separator8.Size = New System.Drawing.Size(6, 25)
        '
        '_receiveStatusLabel
        '
        Me._receiveStatusLabel.Image = Global.isr.Ports.Serial.My.Resources.Resources.ledCornerGray
        Me._receiveStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._receiveStatusLabel.Name = "_receiveStatusLabel"
        Me._receiveStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._receiveStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._receiveStatusLabel.ToolTipText = "receive status"
        '
        '_Separator9
        '
        Me._Separator9.Name = "_Separator9"
        Me._Separator9.Size = New System.Drawing.Size(6, 25)
        '
        '_ReceiveDropDownButton
        '
        Me._ReceiveDropDownButton.AutoToolTip = False
        Me._ReceiveDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReceiveDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReceiveShowHEXToolStripMenuItem, Me._ReceiveShowASCIIToolStripMenuItem})
        Me._ReceiveDropDownButton.Image = CType(resources.GetObject("_ReceiveDropDownButton.Image"), System.Drawing.Image)
        Me._ReceiveDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReceiveDropDownButton.Name = "_ReceiveDropDownButton"
        Me._ReceiveDropDownButton.Size = New System.Drawing.Size(58, 22)
        Me._ReceiveDropDownButton.Text = "Show..."
        Me._ReceiveDropDownButton.ToolTipText = "Select displaying Hex and/or ASCII"
        '
        '_ReceiveShowHEXToolStripMenuItem
        '
        Me._ReceiveShowHEXToolStripMenuItem.Checked = True
        Me._ReceiveShowHEXToolStripMenuItem.CheckOnClick = True
        Me._ReceiveShowHEXToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ReceiveShowHEXToolStripMenuItem.Name = "_ReceiveShowHEXToolStripMenuItem"
        Me._ReceiveShowHEXToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._ReceiveShowHEXToolStripMenuItem.Text = "Show HEX"
        '
        '_ReceiveShowASCIIToolStripMenuItem
        '
        Me._ReceiveShowASCIIToolStripMenuItem.CheckOnClick = True
        Me._ReceiveShowASCIIToolStripMenuItem.Name = "_ReceiveShowASCIIToolStripMenuItem"
        Me._ReceiveShowASCIIToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._ReceiveShowASCIIToolStripMenuItem.Text = "Show ASCII"
        '
        '_TransmitTextBox
        '
        Me._TransmitTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._TransmitTextBox.ContextMenuStrip = Me._TransmitContextMenu
        Me._TransmitTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TransmitTextBox.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitTextBox.Location = New System.Drawing.Point(0, 50)
        Me._TransmitTextBox.Name = "_TransmitTextBox"
        Me._TransmitTextBox.ReadOnly = True
        Me._TransmitTextBox.Size = New System.Drawing.Size(760, 144)
        Me._TransmitTextBox.TabIndex = 4
        Me._TransmitTextBox.Text = "*"
        Me._ToolTip.SetToolTip(Me._TransmitTextBox, "press right mouse button")
        Me._TransmitTextBox.WordWrap = False
        '
        '_TransmitContextMenu
        '
        Me._TransmitContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TransmitCopyMenuItem, Me._TransmitPasteMenuItem, Me._TransmitCutMenuItem, Me._TransmitSendMenuItem, Me._TransmitSendSelectionMenuItem})
        Me._TransmitContextMenu.Name = "MenuTxBox"
        Me._TransmitContextMenu.Size = New System.Drawing.Size(150, 114)
        '
        '_TransmitCopyMenuItem
        '
        Me._TransmitCopyMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Copy
        Me._TransmitCopyMenuItem.Name = "_TransmitCopyMenuItem"
        Me._TransmitCopyMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._TransmitCopyMenuItem.Text = "Copy"
        '
        '_TransmitPasteMenuItem
        '
        Me._TransmitPasteMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Paste
        Me._TransmitPasteMenuItem.Name = "_TransmitPasteMenuItem"
        Me._TransmitPasteMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._TransmitPasteMenuItem.Text = "Paste"
        '
        '_TransmitCutMenuItem
        '
        Me._TransmitCutMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Clipboard_Cut
        Me._TransmitCutMenuItem.Name = "_TransmitCutMenuItem"
        Me._TransmitCutMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._TransmitCutMenuItem.Text = "Cut"
        '
        '_TransmitSendMenuItem
        '
        Me._TransmitSendMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Arrow1_Right
        Me._TransmitSendMenuItem.Name = "_TransmitSendMenuItem"
        Me._TransmitSendMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._TransmitSendMenuItem.Text = "send line"
        '
        '_TransmitSendSelectionMenuItem
        '
        Me._TransmitSendSelectionMenuItem.Image = Global.isr.Ports.Serial.My.Resources.Resources.Arrow2_Right
        Me._TransmitSendSelectionMenuItem.Name = "_TransmitSendSelectionMenuItem"
        Me._TransmitSendSelectionMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._TransmitSendSelectionMenuItem.Text = "send selection"
        '
        '_SendDataToolStrip
        '
        Me._SendDataToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TransmitMessageCombo})
        Me._SendDataToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._SendDataToolStrip.Name = "_SendDataToolStrip"
        Me._SendDataToolStrip.Size = New System.Drawing.Size(760, 25)
        Me._SendDataToolStrip.TabIndex = 3
        Me._SendDataToolStrip.Text = "ToolStrip1"
        '
        '_TransmitMessageCombo
        '
        Me._TransmitMessageCombo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitMessageCombo.Name = "_TransmitMessageCombo"
        Me._TransmitMessageCombo.Size = New System.Drawing.Size(500, 25)
        Me._TransmitMessageCombo.Text = "enter message to sent here and type<enter>"
        Me._TransmitMessageCombo.ToolTipText = "enter message here and type<enter>"
        '
        '_TransmitToolStrip
        '
        Me._TransmitToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TransmitTextBoxLabel, Me._TransmitSeparator1, Me._ClearTransmitBoxButton, Me.__TransmitSeparator2, Me._LoadTransmitFileButton, Me._TransmitSeparator3, Me._transmitCountLabel, Me._TransmitSeparator4, Me._TransmitStatusLabel, Me._TransmitSeparator5, Me._TransmitToolStripDropDownButton, Me._TransmitSeparator6, Me._TransmitDropDownButton, Me._TransmitSeparator7, Me._TransmitRepeatCountTextBox, Me._TransmitDelayMillisecondsTextBox, Me._TransmitPlayButton, Me._TransmitSeparator9})
        Me._TransmitToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._TransmitToolStrip.Name = "_TransmitToolStrip"
        Me._TransmitToolStrip.Size = New System.Drawing.Size(760, 25)
        Me._TransmitToolStrip.TabIndex = 0
        Me._TransmitToolStrip.Text = "ToolStrip3"
        '
        '_TransmitTextBoxLabel
        '
        Me._TransmitTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitTextBoxLabel.Name = "_TransmitTextBoxLabel"
        Me._TransmitTextBoxLabel.Size = New System.Drawing.Size(62, 22)
        Me._TransmitTextBoxLabel.Text = "TRANSMIT"
        '
        '_TransmitSeparator1
        '
        Me._TransmitSeparator1.Name = "_TransmitSeparator1"
        Me._TransmitSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        '_ClearTransmitBoxButton
        '
        Me._ClearTransmitBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearTransmitBoxButton.Image = Global.isr.Ports.Serial.My.Resources.Resources.Doc_Del
        Me._ClearTransmitBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearTransmitBoxButton.Name = "_ClearTransmitBoxButton"
        Me._ClearTransmitBoxButton.Size = New System.Drawing.Size(23, 22)
        Me._ClearTransmitBoxButton.ToolTipText = "Clear transmit box"
        '
        '__TransmitSeparator2
        '
        Me.__TransmitSeparator2.Name = "__TransmitSeparator2"
        Me.__TransmitSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        '_LoadTransmitFileButton
        '
        Me._LoadTransmitFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._LoadTransmitFileButton.Image = Global.isr.Ports.Serial.My.Resources.Resources.Disk
        Me._LoadTransmitFileButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LoadTransmitFileButton.Name = "_LoadTransmitFileButton"
        Me._LoadTransmitFileButton.Size = New System.Drawing.Size(23, 22)
        Me._LoadTransmitFileButton.ToolTipText = "load file into transmit text box"
        '
        '_TransmitSeparator3
        '
        Me._TransmitSeparator3.Name = "_TransmitSeparator3"
        Me._TransmitSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        '_transmitCountLabel
        '
        Me._transmitCountLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._transmitCountLabel.Name = "_transmitCountLabel"
        Me._transmitCountLabel.Size = New System.Drawing.Size(37, 22)
        Me._transmitCountLabel.Text = "00000"
        Me._transmitCountLabel.ToolTipText = "number of bytes sent"
        '
        '_TransmitSeparator4
        '
        Me._TransmitSeparator4.Name = "_TransmitSeparator4"
        Me._TransmitSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        '_TransmitStatusLabel
        '
        Me._TransmitStatusLabel.Image = Global.isr.Ports.Serial.My.Resources.Resources.ledCornerGray
        Me._TransmitStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._TransmitStatusLabel.Name = "_TransmitStatusLabel"
        Me._TransmitStatusLabel.Size = New System.Drawing.Size(16, 22)
        Me._TransmitStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._TransmitStatusLabel.ToolTipText = "send status"
        '
        '_TransmitSeparator5
        '
        Me._TransmitSeparator5.Name = "_TransmitSeparator5"
        Me._TransmitSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        '_TransmitToolStripDropDownButton
        '
        Me._TransmitToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TransmitToolStripDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TransmitEnterHEXToolStripMenuItem, Me._TransmitShowHEXToolStripMenuItem, Me._TransmitShowASCIIToolStripMenuItem})
        Me._TransmitToolStripDropDownButton.Image = CType(resources.GetObject("_TransmitToolStripDropDownButton.Image"), System.Drawing.Image)
        Me._TransmitToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TransmitToolStripDropDownButton.Name = "_TransmitToolStripDropDownButton"
        Me._TransmitToolStripDropDownButton.Size = New System.Drawing.Size(90, 22)
        Me._TransmitToolStripDropDownButton.Text = "Enter/Show..."
        '
        '_TransmitEnterHEXToolStripMenuItem
        '
        Me._TransmitEnterHEXToolStripMenuItem.Checked = True
        Me._TransmitEnterHEXToolStripMenuItem.CheckOnClick = True
        Me._TransmitEnterHEXToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._TransmitEnterHEXToolStripMenuItem.Name = "_TransmitEnterHEXToolStripMenuItem"
        Me._TransmitEnterHEXToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._TransmitEnterHEXToolStripMenuItem.Text = "Enter HEX"
        '
        '_TransmitShowHEXToolStripMenuItem
        '
        Me._TransmitShowHEXToolStripMenuItem.Checked = True
        Me._TransmitShowHEXToolStripMenuItem.CheckOnClick = True
        Me._TransmitShowHEXToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._TransmitShowHEXToolStripMenuItem.Name = "_TransmitShowHEXToolStripMenuItem"
        Me._TransmitShowHEXToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._TransmitShowHEXToolStripMenuItem.Text = "Show HEX"
        '
        '_TransmitShowASCIIToolStripMenuItem
        '
        Me._TransmitShowASCIIToolStripMenuItem.CheckOnClick = True
        Me._TransmitShowASCIIToolStripMenuItem.Name = "_TransmitShowASCIIToolStripMenuItem"
        Me._TransmitShowASCIIToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._TransmitShowASCIIToolStripMenuItem.Text = "Show ASCII"
        Me._TransmitShowASCIIToolStripMenuItem.ToolTipText = "Check to display ASCII"
        '
        '_TransmitSeparator6
        '
        Me._TransmitSeparator6.Name = "_TransmitSeparator6"
        Me._TransmitSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        '_TransmitDropDownButton
        '
        Me._TransmitDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TransmitDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AppendLFToolStripMenuItem, Me._AppendCRToolStripMenuItem})
        Me._TransmitDropDownButton.Image = CType(resources.GetObject("_TransmitDropDownButton.Image"), System.Drawing.Image)
        Me._TransmitDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TransmitDropDownButton.Name = "_TransmitDropDownButton"
        Me._TransmitDropDownButton.Size = New System.Drawing.Size(71, 22)
        Me._TransmitDropDownButton.Text = "Append..."
        '
        '_AppendLFToolStripMenuItem
        '
        Me._AppendLFToolStripMenuItem.CheckOnClick = True
        Me._AppendLFToolStripMenuItem.Name = "_AppendLFToolStripMenuItem"
        Me._AppendLFToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._AppendLFToolStripMenuItem.Text = "Append LF"
        '
        '_AppendCRToolStripMenuItem
        '
        Me._AppendCRToolStripMenuItem.CheckOnClick = True
        Me._AppendCRToolStripMenuItem.Name = "_AppendCRToolStripMenuItem"
        Me._AppendCRToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._AppendCRToolStripMenuItem.Text = "Append CR"
        '
        '_TransmitSeparator7
        '
        Me._TransmitSeparator7.Name = "_TransmitSeparator7"
        Me._TransmitSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        '_TransmitRepeatCountTextBox
        '
        Me._TransmitRepeatCountTextBox.AutoSize = False
        Me._TransmitRepeatCountTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitRepeatCountTextBox.Name = "_TransmitRepeatCountTextBox"
        Me._TransmitRepeatCountTextBox.Size = New System.Drawing.Size(50, 25)
        Me._TransmitRepeatCountTextBox.Text = "100"
        Me._TransmitRepeatCountTextBox.ToolTipText = "Repeat the transmitted message."
        Me._TransmitRepeatCountTextBox.Visible = False
        '
        '_TransmitDelayMillisecondsTextBox
        '
        Me._TransmitDelayMillisecondsTextBox.AutoSize = False
        Me._TransmitDelayMillisecondsTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitDelayMillisecondsTextBox.Name = "_TransmitDelayMillisecondsTextBox"
        Me._TransmitDelayMillisecondsTextBox.Size = New System.Drawing.Size(50, 25)
        Me._TransmitDelayMillisecondsTextBox.Text = "5000"
        Me._TransmitDelayMillisecondsTextBox.ToolTipText = "Transmit delay time milliseconds. Applies to repeat messages only. "
        Me._TransmitDelayMillisecondsTextBox.Visible = False
        '
        '_TransmitPlayButton
        '
        Me._TransmitPlayButton.AutoToolTip = False
        Me._TransmitPlayButton.CheckOnClick = True
        Me._TransmitPlayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TransmitPlayButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TransmitPlayButton.Image = CType(resources.GetObject("_TransmitPlayButton.Image"), System.Drawing.Image)
        Me._TransmitPlayButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TransmitPlayButton.Name = "_TransmitPlayButton"
        Me._TransmitPlayButton.Size = New System.Drawing.Size(39, 22)
        Me._TransmitPlayButton.Text = "PLAY"
        Me._TransmitPlayButton.ToolTipText = "Send the messages loaded in the transmit box, as many times as identified in the" &
    " Repeat box."
        Me._TransmitPlayButton.Visible = False
        '
        '_TransmitSeparator9
        '
        Me._TransmitSeparator9.Name = "_TransmitSeparator9"
        Me._TransmitSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'PortConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SplitContainer)
        Me.Controls.Add(Me._StatusStrip)
        Me.Controls.Add(Me._ToolStrip)
        Me.Controls.Add(Me._MainMenu)
        Me.MinimumSize = New System.Drawing.Size(716, 250)
        Me.Name = "PortConsole"
        Me.Size = New System.Drawing.Size(762, 598)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me._MainMenu.ResumeLayout(False)
        Me._MainMenu.PerformLayout()
        Me._SplitContainer.Panel1.ResumeLayout(False)
        Me._SplitContainer.Panel1.PerformLayout()
        Me._SplitContainer.Panel2.ResumeLayout(False)
        Me._SplitContainer.Panel2.PerformLayout()
        CType(Me._SplitContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me._SplitContainer.ResumeLayout(False)
        Me._ReceiveToolStrip.ResumeLayout(False)
        Me._ReceiveToolStrip.PerformLayout()
        Me._TransmitContextMenu.ResumeLayout(False)
        Me._SendDataToolStrip.ResumeLayout(False)
        Me._SendDataToolStrip.PerformLayout()
        Me._TransmitToolStrip.ResumeLayout(False)
        Me._TransmitToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _MainMenu As System.Windows.Forms.MenuStrip
    Private WithEvents _FileMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _LoadConfigMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SaveConfigMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _OptionsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReceiveBoxFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FontDialog As System.Windows.Forms.FontDialog
    Private WithEvents _SplitContainer As System.Windows.Forms.SplitContainer
    Private WithEvents _ReceiveToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _ReceiveTextBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _TransmitToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _TransmitTextBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ClearTransmitBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ClearReceiveBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _TransmitSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents __TransmitSeparator2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _LoadTransmitFileButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TransmitSeparator3 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _transmitCountLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _TransmitSeparator4 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _SaveReceiveTextBoxButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator7 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _receiveCountLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator8 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _receiveStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _Separator9 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _TransmitStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _TransmitSeparator5 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ReceiveTextBox As System.Windows.Forms.RichTextBox
    Private WithEvents _SendDataToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _TransmitMessageCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _TransmitTextBox As System.Windows.Forms.RichTextBox
    Private WithEvents _StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _PortNumberComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PortNamesCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _BaudRateComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _BaudRateCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ParityComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ParityCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _StopBitsComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _StopBitsCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ConnectButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ConnectStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _LargeFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _MediumFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SmallFontMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _TransmitContextMenu As System.Windows.Forms.ContextMenuStrip
    Private WithEvents _TransmitCopyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitPasteMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitCutMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _DataBitsComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _DataBitsCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _TransmitSendMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitSendSelectionMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReceiveDelayComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ReceiveDelayCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ReceiveThresholdComboLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _ReceiveThresholdCombo As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ReceiveDropDownButton As System.Windows.Forms.ToolStripDropDownButton
    Private WithEvents _ReceiveShowHEXToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReceiveShowASCIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitToolStripDropDownButton As System.Windows.Forms.ToolStripDropDownButton
    Private WithEvents _TransmitEnterHEXToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitShowHEXToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitShowASCIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitDropDownButton As System.Windows.Forms.ToolStripDropDownButton
    Private WithEvents _AppendLFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _AppendCRToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TransmitRepeatCountTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _TransmitSeparator6 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _TransmitSeparator7 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _TransmitPlayButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TransmitDelayMillisecondsTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _TransmitSeparator9 As System.Windows.Forms.ToolStripSeparator

End Class
