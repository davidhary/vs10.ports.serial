﻿''' <summary>
''' Defines an event arguments class for <see cref="Port">port connection messages</see>.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class ConnectionEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortEventArgs" /> class.
    ''' </summary>
    Public Sub New()
        Me.New(False)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ConnectionEventArgs" /> class.
    ''' </summary>
    ''' <param name="isConnected">if set to <c>True</c> [is connected].</param>
    Public Sub New(ByVal isConnected As Boolean)
        MyBase.new()
        Me._IsConnected = isConnected
    End Sub

#End Region

    Private _IsConnected As Boolean
    ''' <summary>
    ''' Gets a value indicating whether the port is connected, true; Otherwise, False.
    ''' </summary>
    Public ReadOnly Property IsConnected As Boolean
        Get
            Return Me._IsConnected
        End Get
    End Property

End Class
