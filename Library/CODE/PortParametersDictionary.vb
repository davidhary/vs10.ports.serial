﻿Imports System.IO
Imports System.Runtime.Serialization

''' <summary>
''' Implements a key,value pair for storing the Port Parameters. 
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<Serializable()>
Public Class PortParametersDictionary
    Inherits Collections.Generic.Dictionary(Of PortParameterKey, String)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PortParametersDictionary" /> class.
    ''' </summary>
    ''' <param name="info">The info.</param>
    ''' <param name="context">The context.</param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary>
    ''' Replaces the specified key.
    ''' </summary>
    ''' <param name="key">The key.</param>
    ''' <param name="value">The value.</param>
    Public Sub Replace(ByVal key As PortParameterKey, ByVal value As String)
        If Me.ContainsKey(key) Then
            If Not Me.Item(key).Equals(value, StringComparison.OrdinalIgnoreCase) Then
                Me.Remove(key)
                Me.Add(key, value)
            End If
        Else
            Me.Add(key, value)
        End If
    End Sub

    ''' <summary>
    ''' Restores the collection from the specified file name.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    Public Sub Restore(ByVal fileName As String)
        Using sr As New StreamReader(fileName)
            Do Until sr.EndOfStream
                Dim values() As String = sr.ReadLine().Split(","c)
                If values.Length = 2 Then
                    Dim key As Integer
                    If Integer.TryParse(values(0), key) Then
                        Me.Replace(key, values(1))
                    End If
                End If
            Loop
        End Using
    End Sub

    ''' <summary>
    ''' Store port parameters.
    ''' </summary>
    Public Sub Store(ByVal fileName As String)
        Using sw As New StreamWriter(fileName)
            For Each key As Integer In [Enum].GetValues(GetType(PortParameterKey))
                If Me.ContainsKey(key) Then
                    sw.WriteLine("{0},{1}", key, Me.Item(key))
                End If
            Next
        End Using
    End Sub

End Class

