﻿Imports System
Imports System.Reflection
Imports System.Resources

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyCopyright("(c) 2012 Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")> 
#If Not NonClsCompliant Then
<Assembly: CLSCompliant(True)> 
#End If

' Version information for an assembly consists of the following four values:
'
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 
' <Assembly: AssemblyFileVersion("1.0.0.0")> 
