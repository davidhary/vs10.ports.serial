﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Ports.Serial
Imports isr.Ports.Serial.Extensions

'''<summary>
'''This is a test class for ExtensionsTest and is intended
'''to contain all ExtensionsTest Unit Tests
'''</summary>
<TestClass()> 
Public Class ExtensionsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for InsertDelimiter
    '''</summary>
    <TestMethod()> 
    Public Sub InsertDelimiterTest()
        Dim value As String = "000003"
        Dim everyLength As Integer = 2
        Dim delimiter As String = " "
        Dim expected As String = "00 00 03"
        Dim actual As String
        actual = Extensions.InsertDelimiter(value, everyLength, delimiter)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expected, value.InsertDelimiter(everyLength, delimiter))
    End Sub

    '''<summary>
    '''A test for RemoveDelimiter
    '''</summary>
    <TestMethod()> 
    Public Sub RemoveDelimiterTest()
        Dim value As String = "00 00 03"
        Dim delimiter As String = " "
        Dim expected As String = "000003"
        Dim actual As String
        actual = Extensions.RemoveDelimiter(value, delimiter)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expected, value.RemoveDelimiter(delimiter))
    End Sub

    '''<summary>
    '''A test for  Remove and then insert Delimiter
    '''</summary>
    <TestMethod()> 
    Public Sub RemoveInsertDelimiterTest()
        Dim value As String = "00 00 03"
        Dim everyLength As Integer = 2
        Dim delimiter As String = " "
        Dim expected As String = "00 00 03"
        Dim actual As String
        actual = Extensions.RemoveDelimiter(value, delimiter)
        Assert.AreEqual(expected, value.RemoveDelimiter(delimiter).InsertDelimiter(everyLength, delimiter))
    End Sub

End Class
